using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Server.Dto;
using Server.Models;
using Server.Services;
using System.Collections.Generic;

namespace Server.Controllers
{
    [ApiController]
    [Authorize]
    [Route("schedules")]
    public class SchedulesController : ControllerBase
    {
        private readonly ISchedulesService _schedules;

        public SchedulesController(ISchedulesService schedules)
        {
            _schedules = schedules;
        }

        [HttpDelete("{id}")]
        public void Delete(ulong id)
        {
            _schedules.Delete(id);
        }

        [HttpGet]
        public List<ScheduleDto> Get()
        {
            return _schedules.GetAll();
        }

        [HttpGet("{id}")]
        public ScheduleDto Get(ulong id)
        {
            return _schedules.Get(id);
        }

        [HttpPatch("{id}")]
        public ScheduleDto Patch(ulong id, ScheduleUpdateDto scheduleDto)
        {
            return _schedules.Update(id, scheduleDto);
        }
    }
}