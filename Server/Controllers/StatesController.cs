using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Server.Dto;
using Server.Models;
using Server.Services;
using System.Collections.Generic;

namespace Server.Controllers
{
    [ApiController]
    [Authorize]
    [Route("states")]
    public class StatesController : ControllerBase
    {
        private readonly IStatesService _states;

        public StatesController(IStatesService states)
        {
            _states = states;
        }

        //[HttpPost]
        //public State Post(StateDto stateDto)
        //{
        //    var userIdValue = User.FindFirst(ClaimTypes.NameIdentifier).Value;
        //    var userId = UInt64.Parse(userIdValue);

        //    return _states.Create(userId, stateDto);
        //}

        [HttpDelete("{id}")]
        public void Delete(ulong id)
        {
            _states.Delete(id);
        }

        [HttpGet]
        public List<StateDto> Get()
        {
            return _states.GetAll();
        }

        [HttpGet("{id}")]
        public StateDto Get(ulong id)
        {
            return _states.Get(id);
        }

        [HttpPatch("{id}")]
        public StateDto Patch(ulong id, StateUpdateDto stateDto)
        {
            return _states.Update(id, stateDto);
        }
    }
}