using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Server.Dto;
using Server.Models;
using Server.Services;
using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace Server.Controllers
{
    [ApiController]
    [Authorize]
    [Route("users")]
    public class UsersController : ControllerBase
    {
        private readonly IActivitiesService _activities;

        private readonly IGroupsService _groups;

        private readonly ILogger<UsersController> _logger;

        private readonly IMappedActivitiesService _mappedActivities;

        private readonly IMappedStatesService _mappedStates;

        private readonly ISchedulesService _schedules;

        private readonly IStatesService _states;

        private readonly IUsersService _users;

        private readonly IActivityStatesService _activityStates;

        private readonly IScheduleGenerationService _scheduleGeneration;

        public UsersController(
            ILogger<UsersController> logger,
            IActivitiesService activities,
            IGroupsService groups,
            ISchedulesService schedules,
            IStatesService states,
            IUsersService users,
            IMappedStatesService mappedStates,
            IMappedActivitiesService mappedActivities,
            IActivityStatesService activityStates,
            IScheduleGenerationService scheduleGeneration
            )
        {
            _activities = activities;
            _groups = groups;
            _logger = logger;
            _schedules = schedules;
            _states = states;
            _users = users;
            _mappedStates = mappedStates;
            _mappedActivities = mappedActivities;
            _activityStates = activityStates;
            _scheduleGeneration = scheduleGeneration;
        }

        [HttpDelete("{id}/activities/{activityId}")]
        public void DeleteActivity(ulong id, ulong activityId)
        {
            _activities.DeleteForUser(id, activityId);
        }

        [HttpDelete("{id}/groups/{groupId}")]
        public void DeleteGroup(ulong id, ulong groupId)
        {
            _groups.DeleteForUser(id, groupId);
        }

        [HttpDelete("{id}/mappedactivities/{mappedActivityId}")]
        public void DeleteMappedActivity(ulong id, ulong mappedActivityId)
        {
            _mappedActivities.DeleteForUser(id, mappedActivityId);
        }

        [HttpDelete("{id}/mappedstates/{mappedStateId}")]
        public void DeleteMappedState(ulong id, ulong mappedStateId)
        {
            _mappedStates.DeleteForUser(id, mappedStateId);
        }

        [HttpDelete("{id}/schedule")]
        public void DeleteSchedule(ulong id)
        {
            _schedules.DeleteForUser(id);
        }

        [HttpDelete("{id}/states/{stateId}")]
        public void DeleteState(ulong id, ulong stateId)
        {
            _states.DeleteForUser(id, stateId);
        }

        /// <summary>
        /// Get user with <c>id</c>
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public UserDto Get(ulong id)
        {
            return _users.Get(id);
        }

        /// <summary>
        /// Returns all users.
        /// </summary>
        [HttpGet]
        public List<UserDto> Get()
        {
            return _users.GetAll();
        }

        [HttpGet("{id}/activities")]
        public List<ActivityDto> GetActivities(ulong id)
        {
            return _activities.GetAllForUser(id);
        }

        [HttpGet("{id}/activities/{activityId}")]
        public ActivityDto GetActivity(ulong id, ulong activityId)
        {
            return _activities.GetForUser(id, activityId);
        }

        [HttpGet("{id}/groups")]
        public List<GroupDto> GetGroups(ulong id)
        {
            return _groups.GetAllForUser(id);
        }

        [HttpGet("{id}/mappedactivities")]
        public List<MappedActivityDto> GetMappedActivities(ulong id)
        {
            return _mappedActivities.GetAllForUser(id);
        }

        [HttpGet("{id}/mappedactivities/{mappedActivityId}")]
        public MappedActivityDto GetMappedActivity(ulong id, ulong mappedActivityId)
        {
            return _mappedActivities.GetForUser(id, mappedActivityId);
        }

        [HttpGet("{id}/mappedstates/{mappedStateId}")]
        public MappedStateDto GetMappedState(ulong id, ulong mappedStateId)
        {
            return _mappedStates.GetForUser(id, mappedStateId);
        }

        [HttpGet("{id}/mappedstates")]
        public List<MappedStateDto> GetMappedStates(ulong id)
        {
            return _mappedStates.GetAllForUser(id);
        }

        [HttpGet("me")]
        public UserDto GetMe()
        {
            var userIdValue = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var userId = UInt64.Parse(userIdValue);

            return _users.Get(userId);
        }

        [HttpGet("{id}/schedule")]
        public ScheduleDto GetSchedule(ulong id)
        {
            return _schedules.GetForUser(id);
        }

        [HttpGet("{id}/states/{stateId}")]
        public StateDto GetState(ulong id, ulong stateId)
        {
            return _states.GetForUser(id, stateId);
        }

        [HttpGet("{id}/states")]
        public List<StateDto> GetStates(ulong id)
        {
            return _states.GetAllForUser(id);
        }

        /// <summary>
        /// Update user.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        [HttpPatch("{id}")]
        public UserDto Patch(ulong id, UserDto user)
        {
            return _users.Update(id, user);
        }

        [HttpPatch("{id}/activities/{activityId}")]
        public ActivityDto PatchActivity(ulong id, ulong activityId, ActivityUpdateDto activityDto)
        {
            return _activities.UpdateForUser(id, activityId, activityDto);
        }

        [HttpPatch("{id}/groups/{groupId}")]
        public GroupDto PatchGroup(ulong id, ulong groupId, GroupUpdateDto groupDto)
        {
            return _groups.UpdateGroupForUser(id, groupId, groupDto);
        }

        [HttpPatch("{id}/mappedactivities/{mappedActivityId}")]
        public MappedActivityDto PatchMappedActivity(ulong id, ulong mappedActivityId, MappedActivityUpdateDto mappedActivityDto)
        {
            return _mappedActivities.UpdateForUser(id, mappedActivityId, mappedActivityDto);
        }

        [HttpPatch("{id}/mappedstates/{mappedStateId}")]
        public MappedStateDto PatchMappedState(ulong id, ulong mappedStateId, MappedStateUpdateDto mappedStateDto)
        {
            return _mappedStates.UpdateForUser(id, mappedStateId, mappedStateDto);
        }

        [HttpPatch("{id}/schedule")]
        public ScheduleDto PatchSchedule(ulong id, ScheduleUpdateDto scheduleDto)
        {
            return _schedules.UpdateForUser(id, scheduleDto);
        }

        [HttpPatch("{id}/states/{stateId}")]
        public StateDto PatchState(ulong id, ulong stateId, StateUpdateDto stateDto)
        {
            return _states.UpdateForUser(id, stateId, stateDto);
        }

        [HttpPost("{id}/activities")]
        public ActivityDto PostActivities(ulong id, ActivityCreateDto activityDto)
        {
            return _activities.Create(id, activityDto);
        }

        [HttpPost("{id}/groups")]
        public GroupDto PostGroups(ulong id, GroupCreateDto groupDto)
        {
            return _groups.Create(id, groupDto);
        }

        [HttpPost("{id}/mappedactivities")]
        public MappedActivityDto PostMappedActivities(ulong id, MappedActivityCreateDto mappedActivityDto)
        {
            return _mappedActivities.Create(id, mappedActivityDto);
        }

        [HttpPost("{id}/mappedstates")]
        public MappedStateDto PostMappedStates(ulong id, MappedStateCreateDto mappedStateDto)
        {
            return _mappedStates.Create(id, mappedStateDto);
        }
        [HttpPost("{id}/states")]
        public StateDto PostStates(ulong id, StateCreateDto stateDto)
        {
            return _states.Create(id, stateDto);
        }

        [HttpPut("{id}/schedule")]
        public ScheduleDto PutSchedule(ulong id, ScheduleCreateDto scheduleDto)
        {
            return _schedules.Create(id, scheduleDto);
        }

        [HttpGet("{id}/activities/{activityId}/states")]
        public List<ActivityStateDto> GetActivityStates(ulong id, ulong activityId)
        {
            return _activityStates.GetForUser(id, activityId);
        }

        [HttpPut("{id}/activities/{activityId}/states/{stateId}")]
        public void PostActivityStates(ulong id, ulong activityId, ulong stateId, ActivityStateCreateDto activityStateDto)
        {
            _activityStates.CreateForUser(id, activityId, stateId, activityStateDto);
        }

        [HttpDelete("{id}/activities/{activityId}/states/{stateId}")]
        public void DeleteActivityState(ulong id, ulong activityId, ulong stateId)
        {
            _activityStates.DeleteForUser(id, activityId, stateId);
        }

        [HttpGet("{id}/schedule/generatedactivities")]
        public List<MappedActivityDto> GetGeneratedActivities(ulong id) {
            return _scheduleGeneration.Generate(id);
        }
    }
}