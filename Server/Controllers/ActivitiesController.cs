using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Server.Dto;
using Server.Services;
using System.Collections.Generic;

namespace Server.Controllers
{
    [ApiController]
    [Authorize]
    [Route("activities")]
    public class ActivitiesController : ControllerBase
    {
        private readonly IActivitiesService _activities;

        public ActivitiesController(IActivitiesService states)
        {
            _activities = states;
        }

        [HttpDelete("{id}")]
        public void Delete(ulong id)
        {
            _activities.Delete(id);
        }

        [HttpGet]
        public List<ActivityDto> Get()
        {
            return _activities.GetAll();
        }

        [HttpGet("{id}")]
        public ActivityDto Get(ulong id)
        {
            return _activities.Get(id);
        }

        [HttpPatch("{id}")]
        public ActivityDto Patch(ulong id, ActivityUpdateDto activityDto)
        {
            return _activities.Update(id, activityDto);
        }
    }
}