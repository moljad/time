using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Server.Dto;
using Server.Services;

namespace Server.Controllers
{
    [ApiController]
    [Authorize]
    [Route("mappedactivities")]
    public class MappedActivitiesController : ControllerBase
    {
        private IMappedActivitiesService _mappedActivities;

        public MappedActivitiesController(IMappedActivitiesService mappedActivities)
        {
            _mappedActivities = mappedActivities;
        }

        [HttpGet]
        public List<MappedActivityDto> GetMappedStates()
        {
            return _mappedActivities.GetAll();
        }
    }
}