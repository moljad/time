using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Server.Dto;
using Server.Services;
using System.Collections.Generic;

namespace Server.Controllers
{
    [ApiController]
    [Authorize]
    [Route("groups")]
    public class GroupsController : ControllerBase
    {
        private readonly IGroupsService _groups;

        public GroupsController(IGroupsService states)
        {
            _groups = states;
        }

        [HttpDelete("{id}")]
        public void Delete(ulong id)
        {
            _groups.Delete(id);
        }

        [HttpGet]
        public List<GroupDto> Get()
        {
            return _groups.GetAll();
        }

        [HttpGet("{id}")]
        public GroupDto Get(ulong id)
        {
            return _groups.Get(id);
        }

        [HttpPatch("{id}")]
        public GroupDto Patch(ulong id, GroupUpdateDto groupDto)
        {
            return _groups.Update(id, groupDto);
        }
    }
}