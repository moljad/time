using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Server.Dto;
using Server.Services;

namespace Server.Controllers
{
    [ApiController]
    [Authorize]
    [Route("mappedstates")]
    public class MappedStatesController : ControllerBase
    {
        private IMappedStatesService _mappedStates;

        public MappedStatesController(IMappedStatesService mappedStates) {
            _mappedStates = mappedStates;
        }

        [HttpGet]
        public List<MappedStateDto> GetMappedStates()
        {
           return _mappedStates.GetAll();
        }
        
    }
}