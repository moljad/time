﻿using System;

namespace Server.Models
{
    public interface ITrackable
    {
        DateTime CreatedDate { get; set; }

        DateTime UpdatedDate { get; set; }
    }
}