using System;
using System.Collections.Generic;

namespace Server.Models
{
    public class Schedule : ITrackable
    {
        public bool Active { get; set; }

        public int ActivityUsualLenght { get; set; }

        public bool Alternating { get; set; }

        public DateTime CreatedDate { get; set; }

        public ulong Id { get; set; }

        public virtual List<MappedActivity> MappedActivities { get; set; } = new List<MappedActivity>();

        public virtual List<MappedState> MappedStates { get; set; } = new List<MappedState>();

        public int RestLength { get; set; }

        public DateTime UpdatedDate { get; set; }

        public User User { get; set; } = null!;

        public ulong UserId { get; set; }
    }
}