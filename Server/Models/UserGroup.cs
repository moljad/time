namespace Server.Models
{
    public class UserGroup
    {
        public virtual Group Group { get; set; } = null!;

        public ulong GroupId { get; set; }

        public ulong Id { get; set; }

        public virtual User User { get; set; } = null!;

        public ulong UserId { get; set; }
    }
}