using System;

namespace Server.Models
{
    public class MappedActivity : ITrackable
    {
        public virtual ulong ActivityId { get; set; }

        public virtual Activity Activity { get; set; } = null!;

        public DateTime CreatedDate { get; set; }

        public ulong Id { get; set; }

        public virtual ulong ScheduleId { get; set; }

        public virtual Schedule Schedule { get; set; } = null!;

        public Span Span { get; set; } = new Span();

        public DateTime UpdatedDate { get; set; }
    }
}