using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace Server.Models
{
    public class User : IdentityUser<ulong>
    {
        public User() : base()
        {
        }

        public virtual List<Activity> Activities { get; set; } = new List<Activity>();

        public virtual List<UserGroup> Groups { get; set; } = new List<UserGroup>();

        public Schedule? Schedule { get; set; } = null!;

        public virtual List<State> States { get; set; } = new List<State>();
    }
}