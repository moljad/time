﻿using Microsoft.EntityFrameworkCore;

namespace Server.Models
{
    [Owned]
    public class Span
    {
        public Span() {}

        public Span(int begin, int end) {
            Begin = begin;
            End = end;
        }

        public int Begin { get; set; }

        public int End { get; set; }
    }
}