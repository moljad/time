using System;
using System.Collections.Generic;

namespace Server.Models
{
    public class State : ITrackable
    {
        public DateTime CreatedDate { get; set; }

        public string Description { get; set; } = string.Empty;

        public ulong Id { get; set; }

        public string Name { get; set; } = string.Empty;

        public string Color { get; set; } = string.Empty;

        public virtual ulong OwnerId { get; set; }

        public virtual User Owner { get; set; } = null!;

        public DateTime UpdatedDate { get; set; }

        public List<ActivityState> Activities { get; set; } = new List<ActivityState>();
    }
}