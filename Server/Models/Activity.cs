using System;
using System.Collections.Generic;

namespace Server.Models
{
    public class Activity : ITrackable
    {
        public DateTime CreatedDate { get; set; }

        public string? Description { get; set; }

        public virtual Group? Group { get; set; }

        public ulong? GroupId { get; set; }

        public ulong Id { get; set; }

        public bool IsRest { get; set; }

        public string? Link { get; set; }

        public int LengthMin { get; set; }

        public int LengthMax { get; set; }

        public int LengthRequired { get; set; }

        public string Color { get; set; } = string.Empty;

        public string Name { get; set; } = string.Empty;

        public int Priority { get; set; }

        public DateTime UpdatedDate { get; set; }

        public virtual User User { get; set; } = null!;

        public ulong UserId { get; set; }

        public List<ActivityState> States { get; set; } = new List<ActivityState>();
    }
}