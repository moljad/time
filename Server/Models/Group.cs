using System;
using System.Collections.Generic;

namespace Server.Models
{
    public class Group : ITrackable
    {
        public virtual List<Activity> Activities { get; set; } = new List<Activity>(); 

        public DateTime CreatedDate { get; set; }

        public ulong Id { get; set; }

        public List<UserGroup> Members { get; set; } = new List<UserGroup>();

        public string Name { get; set; } = string.Empty;

        public DateTime UpdatedDate { get; set; }
    }
}