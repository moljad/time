using System;

namespace Server.Models
{
    public class MappedState : ITrackable
    {
        public DateTime CreatedDate { get; set; }

        public ulong Id { get; set; }

        public ulong ScheduleId { get; set; }

        public virtual Schedule Schedule { get; set; } = null!;

        public Span Span { get; set; } = new Span();

        public ulong StateId { get; set; }

        public State State { get; set; } = null!;

        public DateTime UpdatedDate { get; set; }
    }
}