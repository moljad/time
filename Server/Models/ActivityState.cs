namespace Server.Models
{
    public class ActivityState
    {
        public virtual Activity Activity { get; set; } = null!;

        public virtual State State { get; set; } = null!;

        public ulong Id { get; set; }

        public ulong ActivityId { get; set; }

        public ulong StateId { get; set; }
    }
}