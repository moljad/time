using Server.Dto;
using System.Collections.Generic;

namespace Server.Services
{
    public interface ISchedulesService
    {
        ScheduleDto Create(ulong userId, ScheduleCreateDto scheduleDto);

        void Delete(ulong id);

        void DeleteForUser(ulong userId);

        ScheduleDto Get(ulong id);

        List<ScheduleDto> GetAll();

        ScheduleDto GetForUser(ulong userId);

        ScheduleDto Update(ulong id, ScheduleUpdateDto schedule);

        ScheduleDto UpdateForUser(ulong userId, ScheduleUpdateDto scheduleDto);
    }
}