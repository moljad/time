using Server.Data;
using Server.Dto;
using System.Collections.Generic;
using System.Linq;

namespace Server.Services
{
    public class UsersService : IUsersService
    {
        private readonly TaskContext _context;

        public UsersService(TaskContext context)
        {
            _context = context;
        }

        public void Delete(ulong id)
        {
            var user = _context.Users.Find(id);

            _context.Users.Remove(user);
            _context.SaveChanges();
        }

        public UserDto Get(ulong id)
        {
            return UserDto.FromUser(_context.Users.FirstOrDefault(u => u.Id == id));
        }

        public List<UserDto> GetAll()
        {
            return _context.Users.Select(UserDto.FromUser).ToList();
        }

        public UserDto Update(ulong id, UserDto userDto)
        {
            var user = _context.Users.Find(id);

            if (userDto.UserName != null)
            {
                //user.Name = userDto.Name;
            }

            if (userDto.Email != null)
            {
                //user.Mail = userDto.Mail;
            }

            if (userDto.Password != null)
            {
                //user.Password = userDto.Password;
            }

            var updatedUser = _context.Users.Update(user).Entity;
            _context.SaveChanges();

            return UserDto.FromUser(updatedUser);
        }
    }
}