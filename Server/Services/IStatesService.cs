using Server.Dto;
using System.Collections.Generic;

namespace Server.Services
{
    public interface IStatesService
    {
        StateDto Create(ulong userId, StateCreateDto stateDto);

        void Delete(ulong id);

        void DeleteForUser(ulong userId, ulong id);

        StateDto Get(ulong id);

        List<StateDto> GetAll();

        List<StateDto> GetAllForUser(ulong userId);

        StateDto GetForUser(ulong userId, ulong id);

        StateDto Update(ulong id, StateUpdateDto stateDto);

        StateDto UpdateForUser(ulong userId, ulong id, StateUpdateDto stateDto);
    }
}