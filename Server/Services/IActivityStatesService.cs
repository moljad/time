using System.Collections.Generic;
using Server.Dto;

namespace Server.Services
{
    public interface IActivityStatesService
    {
        List<ActivityStateDto> GetForUser(ulong userId, ulong activityId);

        ActivityStateDto CreateForUser(ulong id, ulong activityId, ulong stateId, ActivityStateCreateDto activityStateDto);

        void DeleteForUser(ulong id, ulong activityId, ulong stateId);
    }
}