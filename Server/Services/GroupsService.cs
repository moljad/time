﻿using Server.Data;
using Server.Dto;
using Server.Models;
using System.Collections.Generic;
using System.Linq;

namespace Server.Services
{
    public class GroupsService : IGroupsService
    {
        private readonly TaskContext _context;

        public GroupsService(TaskContext context)
        {
            _context = context;
        }

        public GroupDto Create(ulong userId, GroupCreateDto groupDto)
        {
            var user = _context.Users.Find(userId);

            var group = new Group()
            {
                Name = groupDto.Name ?? "",
            };

            var userGroup = new UserGroup()
            {
                Group = group,
                User = user,
            };

            group.Members = new List<UserGroup>() { userGroup };

            var createdGroup = _context.Groups.Add(group).Entity;
            _context.SaveChanges();

            return GroupDto.FromGroup(createdGroup);
        }

        public void Delete(ulong id)
        {
            var group = _context.Groups.Find(id);

            _context.Groups.Remove(group);
            _context.SaveChanges();
        }

        public void DeleteForUser(ulong userId, ulong id)
        {
            // TODO
        }

        public GroupDto Get(ulong id)
        {
            return GroupDto.FromGroup(_context.Groups.Find(id));
        }

        public List<GroupDto> GetAll()
        {
            return _context.Groups.Select(GroupDto.FromGroup).ToList();
        }

        public List<GroupDto> GetAllForUser(ulong userId)
        {
            var user = _context.Users.Find(userId);

            return _context.Groups
                .Where(g => g.Members.First(m => m.User == user) != null)
                .Select(GroupDto.FromGroup)
                .ToList();
        }

        public GroupDto Update(ulong id, GroupUpdateDto groupDto)
        {
            var group = _context.Groups.Find(id);

            if (groupDto.Name != null)
            {
                group.Name = groupDto.Name;
            }

           var updatedGroup = _context.Groups.Update(group).Entity;
            _context.SaveChanges();

            return GroupDto.FromGroup(updatedGroup);
        }

        public GroupDto UpdateGroupForUser(ulong userId, ulong id, GroupUpdateDto groupDto)
        {
            var user = _context.Users.Find(userId);

            var group = user.Groups.First(g => g.Group.Id == id).Group;

            if (groupDto.Name != null)
            {
                group.Name = groupDto.Name;
            }

           var updatedGroup = _context.Groups.Update(group).Entity;
            _context.SaveChanges();

            return GroupDto.FromGroup(updatedGroup);
        }
    }
}