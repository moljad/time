using Server.Dto;
using System.Collections.Generic;

namespace Server.Services
{
    public interface IUsersService
    {
        void Delete(ulong id);

        UserDto Get(ulong id);

        List<UserDto> GetAll();

        UserDto Update(ulong id, UserDto user);
    }
}