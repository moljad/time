using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Server.Data;
using Server.Dto;
using Server.Models;
using Server.Services.ScheduleGeneration;

namespace Server.Services
{
    public class ScheduleGenerationService : IScheduleGenerationService
    {
        private readonly TaskContext _context;

        public ScheduleGenerationService(TaskContext context)
        {
            _context = context;
        }

        public List<MappedActivityDto> Generate(ulong userId)
        {
            var user = _context.Users
                .Include(u => u.Groups)
                .Include(u => u.Schedule)
                .Include(u => u.Activities)
                .Include(u => u.States)
                .First(u => u.Id == userId);

            var activities = _context.Activities.Include(a => a.States).Where(a=>a.UserId == userId).ToList();

            var mappedStates = _context.MappedStates.Where(ms => ms.ScheduleId == user.Schedule.Id).ToList();

            var mappedActivities = GenerateLinear(user.Schedule, activities, user.States, user.Groups);
            var validation = Validator.Validate(mappedActivities, user.Schedule, user.Activities, user.States, user.Groups);

            return mappedActivities.Select(MappedActivityDto.FromMappedActivity).ToList();
        }

        private List<MappedActivity> GenerateLinear(Schedule schedule, List<Activity> activities, List<State> states, List<UserGroup> groups)
        {
            IAlgorithm algorithm = new WalkingAlgorithm();

            return algorithm.Generate(schedule, activities, states, groups);
        }
    }
}
