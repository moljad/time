﻿using Server.Dto;
using System.Collections.Generic;

namespace Server.Services
{
    public interface IGroupsService
    {
        GroupDto Create(ulong userId, GroupCreateDto groupDto);

        void Delete(ulong id);

        void DeleteForUser(ulong userId, ulong id);

        GroupDto Get(ulong id);

        List<GroupDto> GetAll();

        List<GroupDto> GetAllForUser(ulong userId);

        GroupDto Update(ulong id, GroupUpdateDto groupDto);

        GroupDto UpdateGroupForUser(ulong userId, ulong id, GroupUpdateDto groupDto);
    }
}