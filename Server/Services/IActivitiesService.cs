﻿using Server.Dto;
using System.Collections.Generic;

namespace Server.Services
{
    public interface IActivitiesService
    {
        ActivityDto Create(ulong userId, ActivityCreateDto activityDto);

        void Delete(ulong id);

        void DeleteForUser(ulong id, ulong activityId);

        ActivityDto Get(ulong id);

        List<ActivityDto> GetAll();

        List<ActivityDto> GetAllForUser(ulong id);

        ActivityDto GetForUser(ulong id, ulong activityId);

        ActivityDto Update(ulong id, ActivityUpdateDto activityDto);

        ActivityDto UpdateForUser(ulong id, ulong activityId, ActivityUpdateDto activityDto);
    }
}