using Server.Dto;
using System.Collections.Generic;

namespace Server.Services
{
    public interface IMappedActivitiesService
    {
        MappedActivityDto Create(ulong userId, MappedActivityCreateDto mappedActivityDto);

        void DeleteForUser(ulong userId, ulong id);

        List<MappedActivityDto> GetAll();

        List<MappedActivityDto> GetAllForUser(ulong userId);

        MappedActivityDto GetForUser(ulong userId, ulong id);

        MappedActivityDto UpdateForUser(ulong userId, ulong id, MappedActivityUpdateDto mappedActivityDto);
    }
}