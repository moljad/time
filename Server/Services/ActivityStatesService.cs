using System.Collections.Generic;
using System.Linq;
using Server.Data;
using Server.Dto;
using Server.Models;

namespace Server.Services
{
    public class ActivityStatesService : IActivityStatesService
    {
        private readonly TaskContext _context;

        public ActivityStatesService(TaskContext context)
        {
            _context = context;
        }

        public ActivityStateDto CreateForUser(ulong id, ulong activityId, ulong stateId, ActivityStateCreateDto activityStateDto)
        {
            var activityState = new ActivityState()
            {
                ActivityId = activityId,
                StateId = stateId,
            };

            var createdActivityState = _context.ActivityStates.Add(activityState).Entity;
            _context.SaveChanges();

            return ActivityStateDto.FromActivityState(createdActivityState);
        }

        public void DeleteForUser(ulong id, ulong activityId, ulong stateId)
        {
            var activityState = _context.ActivityStates.First(a => a.ActivityId == activityId && a.StateId == stateId);

            _context.ActivityStates.Remove(activityState);
            _context.SaveChanges();
        }

        public List<ActivityStateDto> GetForUser(ulong userId, ulong activityId)
        {
            return _context.ActivityStates
                .Where(a => a.Activity.UserId == userId && a.ActivityId == activityId)
                .Select(ActivityStateDto.FromActivityState).ToList();
        }
    }
}