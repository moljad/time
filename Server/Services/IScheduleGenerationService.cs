using System.Collections.Generic;
using Server.Dto;

namespace Server.Services
{
    public interface IScheduleGenerationService
    {
         List<MappedActivityDto> Generate(ulong userId);
    }
}