using Microsoft.EntityFrameworkCore;
using Server.Data;
using Server.Dto;
using Server.Models;
using System.Collections.Generic;
using System.Linq;

namespace Server.Services
{
    public class StatesService : IStatesService
    {
        private readonly TaskContext _context;

        public StatesService(TaskContext context)
        {
            _context = context;
        }

        public StateDto Create(ulong userId, StateCreateDto stateDto)
        {
            var creator = _context.Users.Find(userId);

            var state = new State()
            {
                Name = stateDto.Name ?? string.Empty,
                Color = stateDto.Color ?? string.Empty,
                Description = stateDto.Description ?? string.Empty,
                Owner = creator,
            };

            var createdState = _context.States.Add(state).Entity;
            _context.SaveChanges();

            return StateDto.FromState(createdState);
        }

        public void Delete(ulong id)
        {
            var state = _context.States.Find(id);

            _context.States.Remove(state);
            _context.SaveChanges();
        }

        public void DeleteForUser(ulong userId, ulong id)
        {
            var state = _context.States.FirstOrDefault(s => s.Id == id && s.Owner.Id == userId);

            _context.States.Remove(state);
            _context.SaveChanges();
        }

        public StateDto Get(ulong id)
        {
            return StateDto.FromState(_context.States.Find(id));
        }

        public List<StateDto> GetAll()
        {
            return _context.States.Select(StateDto.FromState).ToList();
        }

        public List<StateDto> GetAllForUser(ulong userId)
        {
            var states= _context.States
                .Include(s => s.Owner)
                .Where(state => state.Owner.Id == userId)
                .Select(StateDto.FromState)
                .ToList();

                System.Console.WriteLine("ASDASDASDASD" + states.Count);
            return states;
        }

        public StateDto GetForUser(ulong userId, ulong id)
        {
            var state = _context.States
                .Include(s => s.Owner)
                .FirstOrDefault(s => s.Id == id && s.Owner.Id == userId);

            return StateDto.FromState(state);
        }

        public StateDto Update(ulong id, StateUpdateDto stateDto)
        {
            var state = _context.States.Find(id);

            if (stateDto.Name != null)
            {
                state.Name = stateDto.Name;
            }

            if (stateDto.Description != null)
            {
                state.Description = stateDto.Description;
            }

            if (stateDto.Color != null)
            {
                state.Color = stateDto.Color;
            }

            var updatedState = _context.States.Update(state).Entity;
            _context.SaveChanges();

            return StateDto.FromState(updatedState);
        }

        public StateDto UpdateForUser(ulong userId, ulong id, StateUpdateDto stateDto)
        {
            var state = _context.States.First(s => s.Owner.Id == userId && s.Id == id);

            if (stateDto.Name != null)
            {
                state.Name = stateDto.Name;
            }

            if (stateDto.Description != null)
            {
                state.Description = stateDto.Description;
            }

            if (stateDto.Color != null)
            {
                state.Color = stateDto.Color;
            }

            var updatedState = _context.States.Update(state).Entity;
            _context.SaveChanges();

            return StateDto.FromState(updatedState);
        }
    }
}