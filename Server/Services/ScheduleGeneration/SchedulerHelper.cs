using System.Collections.Generic;
using System.Linq;
using Server.Models;

namespace Server.Services.ScheduleGeneration
{
    public static class SchedulerHelper
    {
        public static int SpansInWeek => 1008;

        public static int CalculateFreeTime(Schedule schedule)
        {
            return schedule.MappedStates.Sum(ms => ms.Span.GetSize());
        }

        public static Dictionary<Activity, double> CalculateTargetTime(int freeTime, List<Activity> activities)
        {
            var minLengthTime = activities.Sum(a => a.LengthRequired);
            var sumPriority = activities.Sum(a => a.Priority);

            var target = new Dictionary<Activity, double>();

            foreach (var activity in activities)
            {
                var time = (double)activity.Priority / sumPriority * (freeTime - minLengthTime) + activity.LengthRequired;
                target.Add(activity, time);
            }

            return target;
        }

        public static Dictionary<Activity, double> CalculateActualTime(List<MappedActivity> mappedActivities)
        {
            var actual = new Dictionary<Activity, double>();

            foreach (var mappedActivity in mappedActivities)
            {
                if (actual.ContainsKey(mappedActivity.Activity))
                {
                    actual[mappedActivity.Activity] = actual[mappedActivity.Activity] + mappedActivity.Span.GetSize();
                }
                else
                {
                    actual.Add(mappedActivity.Activity, mappedActivity.Span.GetSize());
                }
            }

            return actual;
        }

        public static int GetSpansCount(Schedule schedule)
        {
            return schedule.Alternating ? SpansInWeek * 2 : SpansInWeek;
        }
    }
}