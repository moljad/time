using System.Collections.Generic;
using Server.Models;

namespace Server.Services.ScheduleGeneration
{
    public interface IAlgorithm
    {
        List<MappedActivity> Generate(Schedule schedule, List<Activity> activities, List<State> states, List<UserGroup> groups);
    }
}