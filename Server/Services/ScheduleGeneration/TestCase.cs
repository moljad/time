using System.Collections.Generic;
using Server.Models;

namespace Server.Services.ScheduleGeneration
{
    public class TestCase
    {
        public List<Activity> Activities { get; } = new List<Activity>();

        public List<State> States { get; } = new List<State>();

        public List<UserGroup> UserGroups { get; } = new List<UserGroup>();

        public List<MappedState> MappedStates { get; } = new List<MappedState>();

        public User User { get; } = new User();

        public Schedule Schedule { get; } = new Schedule();

        /*
                public static TestCase DefaultCase()
                {
                    var testCase = new TestCase();


                    var states = new List<State>()
                    {
                        new State()
                        {
                            Name = "Home",
                        },
                        new State()
                        {
                            Name = "Commute",
                        },
                    };

                    var activities = new List<Activity>()
                    {
                        new Activity()
                        {
                            Name = "Programming",
                            IsRest = false,
                            LengthMin = 6,
                            LengthMax = 6,
                            LengthRequired = 0,
                            Priority = 2.0,
                            States = new List<State>() {
                                states[0],
                            }
                        },
                        new Activity()
                        {
                            Name = "Drawing",
                            IsRest = false,
                            LengthMin = 3,
                            LengthMax = 6,
                            LengthRequired = 0,
                            Priority = 1.0,
                            States = new List<State>() {
                                states[0],
                            }
                        },
                        new Activity()
                        {
                            Name = "Reading",
                            IsRest = true,
                            LengthMin = 2,
                            LengthMax = 6,
                            LengthRequired = 0,
                            Priority = 0.1,
                            States = new List<State>() {
                                states[0],
                                states[1],
                            }
                        },
                        new Activity()
                        {
                            Name = "Planing",
                            IsRest = false,
                            LengthMin = 6,
                            LengthMax = 6,
                            LengthRequired = 6,
                            Priority = 0.0,
                            States = new List<State>() {
                                states[0],
                                states[1],
                            }
                        }
                    };

                    var userGroups = new List<UserGroup>()
                    {
                    };

                    var mappedStates = new List<MappedState>()
                    {
                        new MappedState() {
                            State = states[0],
                            Span = new Span(0, 300),
                        },
                        new MappedState() {
                            State = states[1],
                            Span = new Span(300, 360),
                        }
                    };

                    testCase.User.UserName = "Test User";

                    testCase.Schedule.Active = true;
                    testCase.Schedule.Alternating = false;
                    testCase.Schedule.RestLength = 3;

                    testCase.Activities.AddRange(activities);
                    testCase.MappedStates.AddRange(mappedStates);
                    testCase.States.AddRange(states);
                    testCase.UserGroups.AddRange(userGroups);

                    testCase.UpdateConnections();

                    return testCase;
                }

                private TestCase()
                {
                }

                private void UpdateConnections()
                {
                    User.Schedule = Schedule;
                    User.Activities = Activities;
                    User.States = States;
                    User.Groups = UserGroups;

                    Schedule.User = User;
                    Schedule.MappedStates = MappedStates;

                    foreach (var activity in Activities)
                    {
                        var states = activity.States;

                        if (states != null)
                        {
                            foreach (var state in states)
                            {
                                if (state.Activities == null)
                                {
                                    state.Activities = new List<Activity>();
                                }

                                if (!state.Activities.Contains(activity))
                                {
                                    state.Activities.Add(activity);
                                }
                            }
                        }
                    }

                }*/
    }
}