using Microsoft.EntityFrameworkCore;
using Server.Data;
using Server.Dto;
using Server.Models;
using System.Collections.Generic;
using System.Linq;

namespace Server.Services
{
    public class MappedStatesService : IMappedStatesService
    {
        private readonly TaskContext _context;

        public MappedStatesService(TaskContext context)
        {
            _context = context;
        }

        public MappedStateDto Create(ulong userId, MappedStateCreateDto mappedStateDto)
        {
            var schedule = _context.Schedules.FirstOrDefault(s => s.UserId == userId);
            var state = _context.States.Find(mappedStateDto.StateId);

            var mappedState = new MappedState()
            {
                Schedule = schedule,
                Span = mappedStateDto.Span,
                State = state,
            };

            var createdMappedState = _context.Add(mappedState).Entity;
            _context.SaveChanges();

            return MappedStateDto.FromMappedState(createdMappedState);
        }

        public void DeleteForUser(ulong userId, ulong id)
        {
            var mappedState = _context.MappedStates.FirstOrDefault(ms => ms.Schedule.UserId == userId && ms.Id == id);

            _context.MappedStates.Remove(mappedState);
            _context.SaveChanges();
        }

        public List<MappedStateDto> GetAll()
        {
            return _context.MappedStates.Select(MappedStateDto.FromMappedState).ToList();
        }

        public List<MappedStateDto> GetAllForUser(ulong userId)
        {
            return _context.MappedStates
                .Where(mappedstate => mappedstate.Schedule.UserId == userId)
                .Select(MappedStateDto.FromMappedState)
                .ToList();
        }

        public MappedStateDto GetForUser(ulong userId, ulong id)
        {
            var mappedState = _context.MappedStates.FirstOrDefault(ms => ms.Schedule.UserId == userId && ms.Id == id);

            return MappedStateDto.FromMappedState(mappedState);
        }

        public MappedStateDto UpdateForUser(ulong userId, ulong id, MappedStateUpdateDto mappedStateDto)
        {
            var mappedState = _context.MappedStates
                .FirstOrDefault(ms => ms.Id == id && ms.Schedule.UserId == userId);

            if (mappedStateDto.Span != null)
            {
                mappedState.Span = mappedStateDto.Span;
            }

            if (mappedStateDto.StateId != null)
            {
                mappedState.StateId = (ulong)mappedStateDto.StateId;
            }

            var updatedMappedState = _context.MappedStates.Update(mappedState).Entity;
            _context.SaveChanges();

            return MappedStateDto.FromMappedState(updatedMappedState);
        }
    }
}