using Server.Data;
using Server.Dto;
using Server.Models;
using System.Collections.Generic;
using System.Linq;

namespace Server.Services
{
    public class SchedulesService : ISchedulesService
    {
        private readonly TaskContext _context;

        public SchedulesService(TaskContext context)
        {
            _context = context;
        }

        public ScheduleDto Create(ulong userId, ScheduleCreateDto scheduleDto)
        {
            var user = _context.Users.Find(userId);

            var schedule = new Schedule()
            {
                Active = scheduleDto.Active ?? false,
                Alternating = scheduleDto.Alternating ?? false,
                RestLength = scheduleDto.RestLength ?? 0,
                User = user,
            };

            var createdSchedule = _context.Schedules.Add(schedule).Entity;
            _context.SaveChanges();

            return ScheduleDto.FromSchedule(createdSchedule);
        }

        public void Delete(ulong id)
        {
            var schedule = _context.Schedules.Find(id);

            _context.Schedules.Remove(schedule);
        }

        public void DeleteForUser(ulong userId)
        {
            var schedule = _context.Schedules.FirstOrDefault(s => s.UserId == userId);

            _context.Schedules.Remove(schedule);
            _context.SaveChanges();
        }

        public ScheduleDto Get(ulong id)
        {
            return ScheduleDto.FromSchedule(_context.Schedules.Find(id));
        }

        public List<ScheduleDto> GetAll()
        {
            return _context.Schedules.Select(ScheduleDto.FromSchedule).ToList();
        }

        public ScheduleDto GetForUser(ulong userId)
        {
            var schedule = _context.Schedules.FirstOrDefault(s => s.UserId == userId);

            return ScheduleDto.FromSchedule(schedule);
        }

        public ScheduleDto Update(ulong id, ScheduleUpdateDto scheduleDto)
        {
            var schedule = _context.Schedules.Find(id);

            if (scheduleDto.Active != null)
            {
                schedule.Active = (bool)scheduleDto.Active;
            }

            if (scheduleDto.ActivityUsualLength != null)
            {
                schedule.ActivityUsualLenght = (int)scheduleDto.ActivityUsualLength;
            }

            if (scheduleDto.Alternating != null)
            {
                schedule.Alternating = (bool)scheduleDto.Alternating;
            }

            if (scheduleDto.RestLength != null)
            {
                schedule.RestLength = (int)scheduleDto.RestLength;
            }

            var updatedSchedule = _context.Schedules.Update(schedule).Entity;
            _context.SaveChanges();

            return ScheduleDto.FromSchedule(updatedSchedule);
        }

        public ScheduleDto UpdateForUser(ulong userId, ScheduleUpdateDto scheduleDto)
        {
            var schedule = _context.Schedules.FirstOrDefault(s => s.UserId == userId);

            if (scheduleDto.Active != null)
            {
                schedule.Active = (bool)scheduleDto.Active;
            }

            if (scheduleDto.ActivityUsualLength != null)
            {
                schedule.ActivityUsualLenght = (int)scheduleDto.ActivityUsualLength;
            }

            if (scheduleDto.Alternating != null)
            {
                schedule.Alternating = (bool)scheduleDto.Alternating;
            }

            if (scheduleDto.RestLength != null)
            {
                schedule.RestLength = (int)scheduleDto.RestLength;
            }

            var updatedSchedule = _context.Schedules.Update(schedule).Entity;
            _context.SaveChanges();

            return ScheduleDto.FromSchedule(updatedSchedule);
        }
    }
}