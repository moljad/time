using Server.Data;
using Server.Dto;
using Server.Models;
using System.Collections.Generic;
using System.Linq;

namespace Server.Services
{
    public class MappedActivitiesService : IMappedActivitiesService
    {
        private readonly TaskContext _context;

        public MappedActivitiesService(TaskContext context)
        {
            _context = context;
        }

        public MappedActivityDto Create(ulong userId, MappedActivityCreateDto mappedActivityDto)
        {
            var schedule = _context.Schedules
                .FirstOrDefault(s => s.UserId == userId);

            var activity = _context.Activities
                .FirstOrDefault(a => a.Id == mappedActivityDto.ActivityId);

            var mappedActivity = new MappedActivity()
            {
                Activity = activity,
                Schedule = schedule,
                Span = mappedActivityDto.Span,
            };

            var createdMappedActivity = _context.Add(mappedActivity).Entity;
            _context.SaveChanges();

            return MappedActivityDto.FromMappedActivity(createdMappedActivity);
        }

        public void DeleteForUser(ulong userId, ulong id)
        {
            var mappedActivity = _context.MappedActivities
                .FirstOrDefault(ma => ma.Id == id && ma.Schedule.UserId == userId);

            _context.MappedActivities.Remove(mappedActivity);
            _context.SaveChanges();
        }

        public List<MappedActivityDto> GetAll()
        {
            return _context.MappedActivities.Select(MappedActivityDto.FromMappedActivity).ToList();
        }

        public List<MappedActivityDto> GetAllForUser(ulong userId)
        {
            return _context.MappedActivities
                .Where(ma => ma.Schedule.UserId == userId)
                .Select(MappedActivityDto.FromMappedActivity)
                .ToList();
        }

        public MappedActivityDto GetForUser(ulong userId, ulong id)
        {
            var mappedActivity = _context.MappedActivities
                .FirstOrDefault(ma => ma.Id == id && ma.Schedule.UserId == userId);

            return MappedActivityDto.FromMappedActivity(mappedActivity);
        }

        public MappedActivityDto UpdateForUser(ulong userId, ulong id, MappedActivityUpdateDto mappedActivityDto)
        {
            var mappedActivity = _context.MappedActivities
                            .FirstOrDefault(ma => ma.Id == id && ma.Schedule.UserId == userId);

            if (mappedActivityDto.Span != null)
            {
                mappedActivity.Span = mappedActivityDto.Span;
            }

            if (mappedActivityDto.ActivityId != null)
            {
                mappedActivity.ActivityId = (ulong)mappedActivityDto.ActivityId;
            }

            var updatedMappedActivity = _context.MappedActivities.Update(mappedActivity).Entity;
            _context.SaveChanges();

            return MappedActivityDto.FromMappedActivity(updatedMappedActivity);
        }
    }
}