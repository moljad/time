using Server.Dto;
using System.Collections.Generic;

namespace Server.Services
{
    public interface IMappedStatesService
    {
        MappedStateDto Create(ulong userId, MappedStateCreateDto mappedStateDto);

        void DeleteForUser(ulong userId, ulong id);

        List<MappedStateDto> GetAll();

        List<MappedStateDto> GetAllForUser(ulong userId);

        MappedStateDto GetForUser(ulong userId, ulong id);

        MappedStateDto UpdateForUser(ulong userId, ulong id, MappedStateUpdateDto mappedStateDto);
    }
}