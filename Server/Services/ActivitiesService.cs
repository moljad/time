﻿using Server.Data;
using Server.Dto;
using Server.Models;
using System.Collections.Generic;
using System.Linq;

namespace Server.Services
{
    public class ActivitiesService : IActivitiesService
    {
        private readonly TaskContext _context;

        public ActivitiesService(TaskContext context)
        {
            _context = context;
        }

        public ActivityDto Create(ulong userId, ActivityCreateDto activityDto)
        {
            var user = _context.Users.Find(userId);

            var activity = new Activity()
            {
                Description = activityDto.Description,
                IsRest = activityDto.IsRest ?? false,
                Link = activityDto.Link,
                LengthMin = activityDto.LengthMin ?? 0,
                LengthMax = activityDto.LengthMax ?? 6,
                LengthRequired = activityDto.LengthRequired ?? 0,
                Color = activityDto.Color ?? string.Empty,
                Name = activityDto.Name ?? string.Empty,
                Priority = activityDto.Priority ?? 0,
                User = user,
            };

            var createdActivity = _context.Activities.Add(activity).Entity;
            _context.SaveChanges();

            return ActivityDto.FromActivity(createdActivity);
        }

        public void Delete(ulong id)
        {
            var activity = _context.Activities.Find(id);

            _context.Activities.Remove(activity);
            _context.SaveChanges();
        }

        public void DeleteForUser(ulong id, ulong activityId)
        {
            var activity = _context.Activities.First(a => a.UserId == id && a.Id == activityId);

            _context.Activities.Remove(activity);
            _context.SaveChanges();
        }

        public ActivityDto Get(ulong id)
        {
            var activity = _context.Activities.Find(id);

            return ActivityDto.FromActivity(activity);
        }

        public List<ActivityDto> GetAll()
        {
            return _context.Activities.Select(ActivityDto.FromActivity).ToList();
        }

        public List<ActivityDto> GetAllForUser(ulong id)
        {
            return _context.Activities
                .Where(a => a.UserId == id)
                .Select(ActivityDto.FromActivity)
                .ToList();
        }

        public ActivityDto GetForUser(ulong id, ulong activityId)
        {
            var activity = _context.Activities
                .FirstOrDefault(a => a.UserId == id && a.Id == activityId);

            return ActivityDto.FromActivity(activity);
        }

        public ActivityDto Update(ulong id, ActivityUpdateDto activityDto)
        {
            var activity = _context.Activities.Find(id);

            if (activityDto.Description != null)
            {
                activity.Description = activityDto.Description;
            }

            if (activityDto.IsRest != null)
            {
                activity.IsRest = (bool)activityDto.IsRest;
            }

            if (activityDto.Link != null)
            {
                activity.Link = activityDto.Link;
            }

            if (activityDto.LengthMin != null)
            {
                activity.LengthMin = (int)activityDto.LengthMin;
            }

            if (activityDto.LengthMax != null)
            {
                activity.LengthMax = (int)activityDto.LengthMax;
            }

            if (activityDto.LengthRequired != null)
            {
                activity.LengthRequired = (int)activityDto.LengthRequired;
            }

            if (activityDto.Color != null)
            {
                activity.Color = activityDto.Color;
            }

            if (activityDto.Name != null)
            {
                activity.Name = activityDto.Name;
            }

            if (activityDto.Priority != null)
            {
                activity.Priority = (int)activityDto.Priority;
            }

            var updatedActivity = _context.Activities.Update(activity).Entity;
            _context.SaveChanges();

            return ActivityDto.FromActivity(updatedActivity);
        }

        public ActivityDto UpdateForUser(ulong id, ulong activityId, ActivityUpdateDto activityDto)
        {
            var activity = _context.Activities.FirstOrDefault(a => a.UserId == id && a.Id == activityId);

            if (activityDto.Description != null)
            {
                activity.Description = activityDto.Description;
            }

            if (activityDto.IsRest != null)
            {
                activity.IsRest = (bool)activityDto.IsRest;
            }

            if (activityDto.Link != null)
            {
                activity.Link = activityDto.Link;
            }

            if (activityDto.LengthMin != null)
            {
                activity.LengthMin = (int)activityDto.LengthMin;
            }

            if (activityDto.LengthMax != null)
            {
                activity.LengthMax = (int)activityDto.LengthMax;
            }

            if (activityDto.LengthRequired != null)
            {
                activity.LengthRequired = (int)activityDto.LengthRequired;
            }

            if (activityDto.Color != null)
            {
                activity.Color = activityDto.Color;
            }

            if (activityDto.Name != null)
            {
                activity.Name = activityDto.Name;
            }

            if (activityDto.Priority != null)
            {
                activity.Priority = (int)activityDto.Priority;
            }

            var updatedActivity = _context.Activities.Update(activity).Entity;
            _context.SaveChanges();

            return ActivityDto.FromActivity(updatedActivity);
        }
    }
}