# Time

## Server Setup

* Install [.NET Core](https://dotnet.microsoft.com/download).

* ``dotnet tool install --global dotnet-ef``

* ``cd server``

Create migration:

* ``dotnet ef migrations add InitialCreate``

Update DB:

* ``dotnet ef database update``

Server start:

* ``dotnet watch run``

## Swagger

* URL: https://localhost:5001/swagger/index.html
* API Info: https://localhost:5001/.well-known/openid-configuration
