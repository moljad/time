using Server.Models;

namespace Server.Dto
{
    public class MappedStateCreateDto
    {
        public Span Span { get; set; } = new Span();

        public ulong StateId { get; set; }
    }
}