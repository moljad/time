using Server.Models;

namespace Server.Dto
{
    public class ActivityStateDto
    {
        public ulong Id { get; set; }

        public ulong ActivityId { get; set; }

        public ulong StateId { get; set; }

        public static ActivityStateDto FromActivityState(ActivityState activityState)
        {
            return new ActivityStateDto()
            {
                ActivityId = activityState.ActivityId,
                Id = activityState.Id,
                StateId = activityState.StateId,
            };
        }
    }
}