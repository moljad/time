using System;
using Server.Models;

namespace Server.Dto
{
    public class StateDto
    {
        public DateTime? CreatedDate { get; set; }

        public string Color { get; set; } = string.Empty;

        public string? Description { get; set; }

        public ulong? Id { get; set; }

        public string? Name { get; set; }

        public ulong? UserId { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public static StateDto FromState(State state)
        {
            return new StateDto()
            {
                CreatedDate = state.CreatedDate,
                Color = state.Color,
                Description = state.Description,
                Id = state.Id,
                Name = state.Name,
                UpdatedDate = state.UpdatedDate,
                UserId = state.OwnerId,
            };
        }
    }
}