namespace Server.Dto
{
    public class StateUpdateDto
    {
        public string? Description { get; set; }

        public string? Name { get; set; }

        public string? Color { get; set; }
    }
}