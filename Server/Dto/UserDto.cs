using Server.Models;

namespace Server.Dto
{
    public class UserDto
    {
        public string? Email { get; set; }

        public ulong? Id { get; set; }

        public string? Password { get; set; }

        public string? UserName { get; set; }

        public static UserDto FromUser(User user)
        {
            return new UserDto()
            {
                Id = user.Id,
                Email = user.Email,
                UserName = user.UserName,
            };
        }
    }
}