﻿using System;
using Server.Models;

namespace Server.Dto
{
    public class ActivityDto
    {
        public DateTime? CreatedDate { get; set; }

        public string? Description { get; set; }

        public ulong? Id { get; set; }

        public bool? IsRest { get; set; }

        public string? Link { get; set; }

        public int? LengthMin { get; set; }

        public int? LengthMax { get; set; }

        public int? LengthRequired { get; set; }

        public string? Color { get; set; }

        public string? Name { get; set; }

        public int? Priority { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public ulong? UserId { get; set; }

        public static ActivityDto FromActivity(Activity activity)
        {
            return new ActivityDto()
            {
                Description = activity.Description,
                Id = activity.Id,
                IsRest = activity.IsRest,
                Link = activity.Link,
                LengthMin = activity.LengthMin,
                LengthMax = activity.LengthMax,
                LengthRequired = activity.LengthRequired,
                Color = activity.Color,
                Name = activity.Name,
                Priority = activity.Priority,
                CreatedDate = activity.CreatedDate,
                UpdatedDate = activity.UpdatedDate,
                UserId = activity.UserId,
            };
        }
    }
}