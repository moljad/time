namespace Server.Dto
{
    public class StateCreateDto
    {
        public string? Description { get; set; }

        public string Name { get; set; } = string.Empty;

        public string? Color { get; set; }
    }
}