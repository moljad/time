﻿using System;
using Server.Models;

namespace Server.Dto
{
    public class ScheduleDto
    {
        public bool? Active { get; set; }

        public bool? Alternating { get; set; }

        public int? ActivityUsualLength { get; set; }

        public DateTime? CreatedDate { get; set; }

        public int? RestLength { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public ulong? UserId { get; set; }

        public ulong? Id {get;set;}

        public static ScheduleDto FromSchedule(Schedule schedule)
        {
            return new ScheduleDto()
            {
                Active = schedule.Active,
                ActivityUsualLength = schedule.ActivityUsualLenght,
                Alternating = schedule.Alternating,
                RestLength = schedule.RestLength,
                CreatedDate = schedule.CreatedDate,
                UpdatedDate = schedule.UpdatedDate,
                UserId = schedule.UserId,
                Id = schedule.Id,
            };
        }
    }
}