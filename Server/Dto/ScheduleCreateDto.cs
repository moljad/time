﻿namespace Server.Dto
{
    public class ScheduleCreateDto
    {
        public bool? Active { get; set; }

        public int? ActivityUsualLength { get; set; }

        public bool? Alternating { get; set; }

        public int? RestLength { get; set; }
    }
}