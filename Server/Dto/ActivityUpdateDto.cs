﻿namespace Server.Dto
{
    public class ActivityUpdateDto
    {
        public string? Description { get; set; }

        public bool? IsRest { get; set; }

        public string? Link { get; set; }

        public int? LengthMin { get; set; }

        public int? LengthMax { get; set; }

        public int? LengthRequired { get; set; }

        public string? Color { get; set; }

        public string? Name { get; set; }

        public int? Priority { get; set; }
    }
}