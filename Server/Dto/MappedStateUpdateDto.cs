using Server.Models;

namespace Server.Dto
{
    public class MappedStateUpdateDto
    {
        public Span? Span { get; set; } = new Span();

        public ulong? StateId { get; set; }
    }
}