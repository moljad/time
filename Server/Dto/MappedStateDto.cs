using System;
using Server.Models;

namespace Server.Dto
{
    public class MappedStateDto
    {
        public DateTime? CreatedDate { get; set; }

        public ulong? Id { get; set; }

        public ulong? ScheduleId { get; set; }

        public Span? Span { get; set; } = new Span();

        public ulong? StateId { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public static MappedStateDto FromMappedState(MappedState mappedState)
        {
            return new MappedStateDto()
            {
                Id = mappedState.Id,
                ScheduleId = mappedState.ScheduleId,
                Span = mappedState.Span,
                StateId = mappedState.StateId,
                CreatedDate = mappedState.CreatedDate,
                UpdatedDate = mappedState.UpdatedDate,
            };
        }
    }
}