using Server.Models;

namespace Server.Dto
{
    public class MappedActivityCreateDto
    {
        public ulong ActivityId { get; set; }

        public Span Span { get; set; } = new Span();
    }
}