using System;
using Server.Models;

namespace Server.Dto
{
    public class MappedActivityDto
    {
        public ulong? ActivityId { get; set; }

        public DateTime? CreatedDate { get; set; }

        public ulong? Id { get; set; }

        public Span? Span { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public ulong? ScheduleId {get;set;}

        public static MappedActivityDto FromMappedActivity(MappedActivity mappedActivity)
        {
            return new MappedActivityDto()
            {
                CreatedDate = mappedActivity.CreatedDate,
                Span = mappedActivity.Span,
                Id = mappedActivity.Id,
                UpdatedDate = mappedActivity.UpdatedDate,
                ActivityId = mappedActivity.ActivityId,
                ScheduleId = mappedActivity.ScheduleId,
            };
        }
    }
}