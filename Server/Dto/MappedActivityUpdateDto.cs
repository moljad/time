using Server.Models;

namespace Server.Dto
{
    public class MappedActivityUpdateDto
    {
        public ulong? ActivityId { get; set; }

        public Span? Span { get; set; }
    }
}