﻿using System;
using Server.Models;

namespace Server.Dto
{
    public class GroupDto
    {
        public DateTime? CreatedDate { get; set; }

        public ulong? Id { get; set; }

        public string? Name { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public static GroupDto FromGroup(Group group)
        {
            return new GroupDto()
            {
                Id = group.Id,
                Name = group.Name,
                CreatedDate = group.CreatedDate,
                UpdatedDate = group.UpdatedDate,
            };
        }
    }
}