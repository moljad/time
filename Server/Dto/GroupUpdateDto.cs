﻿namespace Server.Dto
{
    public class GroupUpdateDto
    {
        public string? Name { get; set; }
    }
}