﻿namespace Server.Dto
{
    public class ActivityCreateDto
    {
        public string? Description { get; set; }

        public bool? IsRest { get; set; }

        public string? Link { get; set; }

        public int? LengthMin { get; set; }

        public int? LengthMax { get; set; }

        public int? LengthRequired { get; set; }

        public string? Color { get; set; }

        public string Name { get; set; } = string.Empty;

        public int? Priority { get; set; }
    }
}