# Api Plan

* [ ] `/mappedstates/{mappedstateId} (DELETE)`
* [ ] `/users/{id} (PATCH)`
* [ ] `/users/{id}/groups/{id} (GET)`
* [ ] `/users/{id}/schedule/resultingstates (GET)`
* [?] `/users/{id}/groups/{id} (DELETE)`
* [x] `/activities (GET)`
* [x] `/activities/{id} (DELETE)`
* [x] `/activities/{id} (GET)`
* [x] `/activities/{id} (PATCH)`
* [x] `/groups (GET)`
* [x] `/groups/{id} (DELETE)`
* [x] `/groups/{id} (GET)`
* [x] `/groups/{id} (PATCH)`
* [x] `/mappedactivities (GET)`
* [x] `/mappedstates (GET)`
* [x] `/schedules (GET)`
* [x] `/schedules/{id} (DELETE)`
* [x] `/schedules/{id} (GET)`
* [x] `/schedules/{id} (PATCH)`
* [x] `/states (GET)`
* [x] `/states/{id} (DELETE)`
* [x] `/states/{id} (GET)`
* [x] `/states/{id} (PATCH)`
* [x] `/users (GET)`
* [x] `/users/{id} (GET)`
* [x] `/users/{id}/activities (GET)`
* [x] `/users/{id}/activities (POST)`
* [x] `/users/{id}/activities/{activityId} (DELETE)`
* [x] `/users/{id}/activities/{activityId} (GET)`
* [x] `/users/{id}/activities/{activityId} (PATCH)`
* [ ] `/users/{id}/activities/{activityId}/states (GET)`
* [ ] `/users/{id}/activities/{activityId}/states/{stateId} (PUT)`
* [ ] `/users/{id}/activities/{activityId}/states/{stateId} (DELETE)`
* [x] `/users/{id}/groups (GET)`
* [x] `/users/{id}/groups (POST)`
* [x] `/users/{id}/groups/{id} (PATCH)`
* [x] `/users/{id}/schedule (DELETE)`
* [x] `/users/{id}/schedule (GET)`
* [x] `/users/{id}/schedule (PATCH)`
* [x] `/users/{id}/schedule (PUT)`
* [x] `/users/{id}/schedule/mappedactivities (GET)`
* [x] `/users/{id}/schedule/mappedactivities (POST)`
* [x] `/users/{id}/schedule/mappedactivities/{mappedactivitiesId} (DELETE)`
* [x] `/users/{id}/schedule/mappedactivities/{mappedactivitiesId} (GET)`
* [x] `/users/{id}/schedule/mappedactivities/{mappedactivitiesId} (PATCH)`
* [x] `/users/{id}/schedule/mappedstates (GET)`
* [x] `/users/{id}/schedule/mappedstates (POST)`???
* [x] `/users/{id}/schedule/mappedstates/{mappedstateId} (DELETE)`???
* [x] `/users/{id}/schedule/mappedstates/{mappedstateId} (GET)`
* [x] `/users/{id}/schedule/mappedstates/{mappedstateId} (PATCH)`???
* [x] `/users/{id}/states (GET)`
* [x] `/users/{id}/states (POST)`
* [x] `/users/{id}/states/{stateId} (DELETE)`
* [x] `/users/{id}/states/{stateId} (GET)`
* [x] `/users/{id}/states/{stateId} (PATCH)`
