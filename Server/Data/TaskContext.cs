using IdentityServer4.EntityFramework.Entities;
using IdentityServer4.EntityFramework.Extensions;
using IdentityServer4.EntityFramework.Interfaces;
using IdentityServer4.EntityFramework.Options;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Server.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Data
{
    public class TaskContext : IdentityDbContext<User, IdentityRole<ulong>, ulong>, IPersistedGrantDbContext //DbContext
    {
        private readonly IOptions<OperationalStoreOptions> _operationalStoreOptions;

        public TaskContext(DbContextOptions options, IOptions<OperationalStoreOptions> operationalStoreOptions) : base(options)
        {
            _operationalStoreOptions = operationalStoreOptions;
        }

        public DbSet<Activity> Activities { get; set; } = null!;

        public DbSet<DeviceFlowCodes> DeviceFlowCodes { get; set; } = null!;

        public DbSet<Group> Groups { get; set; } = null!;

        public DbSet<MappedActivity> MappedActivities { get; set; } = null!;

        public DbSet<MappedState> MappedStates { get; set; } = null!;

        public DbSet<PersistedGrant> PersistedGrants { get; set; } = null!;

        public DbSet<Schedule> Schedules { get; set; } = null!;

        public DbSet<State> States { get; set; } = null!;

        public DbSet<ActivityState> ActivityStates { get; set; } = null!;

        public DbSet<UserGroup> UserGroups { get; set; } = null!;
        public override int SaveChanges()
        {
            var now = DateTime.Now;
            var entries = ChangeTracker.Entries().Where(e => e.Entity is ITrackable && (e.State == EntityState.Added || e.State == EntityState.Modified));

            foreach (var entry in entries)
            {
                var entity = entry.Entity as ITrackable;

                if (entity is ITrackable trackable)
                {
                    if (entry.State == EntityState.Added)
                    {
                        trackable.CreatedDate = now;
                    }

                    trackable.UpdatedDate = now;
                }
            }

            return base.SaveChanges();
        }

        public Task<int> SaveChangesAsync() => base.SaveChangesAsync();

        protected override void OnConfiguring(DbContextOptionsBuilder options) => options.UseSqlite("Data Source=time.db");

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.ConfigurePersistedGrantContext(_operationalStoreOptions.Value);

            builder.Entity<Activity>()
                .HasOne(a => a.User)
                .WithMany(u => u.Activities)
                .HasForeignKey(a => a.UserId);

            builder.Entity<Activity>()
                .HasOne(a => a.Group)
                .WithMany(g => g!.Activities)
                .HasForeignKey(a => a.GroupId);

            builder.Entity<UserGroup>()
                .HasOne(ug => ug.User)
                .WithMany(u => u.Groups)
                .HasForeignKey(ug => ug.UserId);

            builder.Entity<UserGroup>()
                .HasOne(ug => ug.Group)
                .WithMany(g => g.Members)
                .HasForeignKey(ug => ug.GroupId);
        }
    }
}