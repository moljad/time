# Entities

```mermaid
classDiagram

class User {
string Name
string Password
string Mail
}

class Activity {
string Name
string Description
string Link
bool IsRest
int MinLength
int Priority
}

class MappedActivity {
int Start
int End
}

class Group {
string Name
}

class MappedState {
int Start
int End
}

class State {
string Name
string Description
}

class Schedule {
bool Active
bool Alternating
int RestLength
}

```