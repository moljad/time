﻿using Scheduler.Algorithm;
using Scheduler.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Scheduler
{
    public class SchedulerResult
    {
        public List<MappedActivity> MappedActivities { get; set; }

        public double Quality { get; set; }

        public List<Tuple<string, string>> Properties { get; set; }
    }

    public class Scheduler
    {
        public static SchedulerResult Generate(IAlgorithm algoritm, Schedule schedule, List<Activity> activities, List<State> states, List<UserGroup> groups)
        {
            var result = algoritm.Generate(schedule, activities, states, groups);

            CheckCorrectness(schedule, result);

            var quality = CalculateQuality(schedule, activities, states, groups, result);
            var breaks = CheckBreaks(schedule, result);

            return new SchedulerResult()
            {
                MappedActivities = result,
                Quality = quality,
                Properties = new List<Tuple<string, string>>()
                {
                    new Tuple<string, string>("Breaks check", breaks.ToString()),
                    new Tuple<string, string>("Activities count", result.Count.ToString()),
                }
            };
        }

        private static double CalculateQuality(Schedule schedule, List<Activity> activities, List<State> states, List<UserGroup> groups, List<MappedActivity> mappedActivities)
        {
            var freeTime = SchedulerHelper.CalculateFreeTime(schedule);
            var target = SchedulerHelper.CalculateTargetTime(freeTime, activities);
            var actual = SchedulerHelper.CalculateActualTime(mappedActivities);

            var score = 0.0;
            foreach (var activity in activities)
            {
                var max = target[activity];
                actual.TryGetValue(activity, out var local);

                score += Math.Min(max, local);
            }

            return freeTime > 0 ? score / freeTime : 0.0;
        }

        private static bool CheckBreaks(Schedule schedule, List<MappedActivity> mappedActivities)
        {
            if (mappedActivities.Count == 0)
            {
                return true;
            }

            var sortedSpans = mappedActivities
                .Select(ma => ma.Span)
                .OrderBy(s => s.Begin);

            var minRestTime = sortedSpans
                .Zip(sortedSpans.Skip(1), (a, b) => Span.GetDistance(a, b))
                .Min();

            return minRestTime >= schedule.RestLength;
        }

        private static void CheckCorrectness(Schedule schedule, List<MappedActivity> mappedActivities)
        {
            var spansCount = SchedulerHelper.GetSpansCount(schedule);

            var frees = new bool[spansCount];

            foreach (var mappedStates in schedule.MappedStates)
            {
                var span = mappedStates.Span;

                for (int i = span.Begin; i < span.End; i++)
                {
                    frees[i] = true;
                }
            }

            for (int i = 0; i < mappedActivities.Count; i++)
            {
                for (int j = i + 1; j < mappedActivities.Count; j++)
                {
                    if (Span.Intersects(mappedActivities[i].Span, mappedActivities[j].Span))
                    {
                        throw new Exception("Intersection");
                    }
                }

            }

            foreach (var mappedActivity in mappedActivities)
            {
                var span = mappedActivity.Span;

                if (span.Begin < 0 || span.End > spansCount)
                {
                    throw new Exception("Out of week range");
                }
            }

            foreach (var mappedACtivity in mappedActivities)
            {
                var span = mappedACtivity.Span;

                var spanSize = Span.GetSize(span);

                if (Span.GetSize(span) <= 0)
                {
                    throw new Exception($"Wrong span size {spanSize} for {span.Begin}-{span.End}");
                }
            }

            foreach (var mappedActivity in mappedActivities)
            {
                var span = mappedActivity.Span;

                for (int i = span.Begin; i < span.End; i++)
                {
                    if (!frees[i])
                    {
                        throw new Exception("Activity on busy part");
                    }
                }
            }
        }

        
    }
}