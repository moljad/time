using System.Collections.Generic;
using System.Linq;
using Scheduler.Model;

namespace Scheduler
{
    public static class SchedulerHelper
    {
        public static int SpansInWeek => 1008;

        public static int CalculateFreeTime(Schedule schedule)
        {
            return schedule.MappedStates.Sum(ms => Span.GetSize(ms.Span));
        }

        public static Dictionary<Activity, double> CalculateTargetTime(int freeTime, List<Activity> activities)
        {
            var minLengthTime = activities.Sum(a => a.LengthRequired);
            var sumPriority = activities.Sum(a => a.Priority);

            var target = new Dictionary<Activity, double>();

            foreach (var activity in activities)
            {
                var time = activity.Priority / sumPriority * (freeTime - minLengthTime) + activity.LengthRequired;
                target.Add(activity, time);
            }

            return target;
        }

        public static Dictionary<Activity, double> CalculateActualTime(List<MappedActivity> mappedActivities)
        {
            var actual = new Dictionary<Activity, double>();

            foreach (var mappedActivity in mappedActivities)
            {
                if (actual.ContainsKey(mappedActivity.Activity))
                {
                    actual[mappedActivity.Activity] = actual[mappedActivity.Activity] + Span.GetSize(mappedActivity.Span);
                }
                else
                {
                    actual.Add(mappedActivity.Activity, Span.GetSize(mappedActivity.Span));
                }
            }

            return actual;
        }

        public static int GetSpansCount(Schedule schedule)
        {
            return schedule.Alternating ? SpansInWeek * 2 : SpansInWeek;
        }
    }
}