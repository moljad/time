using System.Collections.Generic;
using Scheduler.Model;

namespace Scheduler.Algorithm
{
    public class DummyAlgorithm : IAlgorithm
    {
        public List<MappedActivity> Generate(Schedule schedule, List<Activity> activities, List<State> states, List<UserGroup> groups)
        {
            var firstActivity = activities[0];
            return new List<MappedActivity>
            {
                new MappedActivity() { Activity = firstActivity, Span = new Span(0, 5) },
                new MappedActivity() { Activity = firstActivity, Span = new Span(10, 20)}
            };
        }
    }
}