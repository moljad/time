using System;
using System.Collections.Generic;
using System.Linq;
using Scheduler.Model;

namespace Scheduler.Algorithm
{
    public class WalkingAlgorithm : IAlgorithm
    {
        public List<MappedActivity> Generate(Schedule schedule, List<Activity> activities, List<State> states, List<UserGroup> groups)
        {
            var spansInWeek = SchedulerHelper.GetSpansCount(schedule);

            var freeTime = SchedulerHelper.CalculateFreeTime(schedule);

            var targetTimes = SchedulerHelper.CalculateTargetTime(freeTime, activities);

            int current = 0;

            var spanStates = new State[spansInWeek];

            foreach (var mappedState in schedule.MappedStates)
            {
                var span = mappedState.Span;

                for (int i = span.Begin; i < span.End; i++)
                {
                    spanStates[i] = mappedState.State;
                }
            }

            const int bestTime = 6;

            var mappedActivities = new List<MappedActivity>();

            while (current < spansInWeek)
            {
                var state = spanStates[current];

                if (state == null)
                {
                    current++;
                }
                else
                {
                    var act = SchedulerHelper.CalculateActualTime(mappedActivities);

                    var appropriteActivities = FindAppropriteForState(activities, state)
                                                .Where(a =>
                                                {
                                                    if (act.ContainsKey(a))
                                                    {
                                                        return a.Priority != 0 || act[a] < targetTimes[a];
                                                    }

                                                    return true;
                                                }).ToList();

                    if (appropriteActivities.Count == 0)
                    {
                        current++;
                        continue;
                    }

                    var sorted = appropriteActivities.OrderBy(aa =>
                            act.ContainsKey(aa) ? (act[aa] / targetTimes[aa]) : 0
                        );

                    var ss = appropriteActivities.Select(aa =>
                            act.ContainsKey(aa) ? act[aa] / targetTimes[aa] : 0
                        ).ToArray();

                    foreach (var activity in sorted)
                    {
                        var maxlen = FindMaxLenghtForActivity(activity, spanStates.Skip(current).ToArray());

                        if (maxlen >= activity.LengthMin)
                        {
                            var resultLen = Math.Min(bestTime, maxlen);

                            var mappedActivity = new MappedActivity()
                            {
                                Activity = activity,
                                Schedule = schedule,
                                Span = new Span(current, current + resultLen)
                            };

                            mappedActivities.Add(mappedActivity);

                            current += resultLen + schedule.RestLength;
                            break;
                        }

                    }
                }
            }

            return mappedActivities;
        }

        private List<Activity> FindAppropriteForState(List<Activity> activities, State state)
        {
            return activities.Where(a => a.States.Contains(state)).ToList();
        }

        private int FindMaxLenghtForActivity(Activity activity, State[] spanStates)
        {
            var len = 0;

            for (int i = 0; i < spanStates.Length; i++)
            {
                if (spanStates[i] == null || !spanStates[i].Activities.Contains(activity))
                {
                    break;
                }

                len++;
            }

            return Math.Min(activity.LengthMax, len);
        }
    }
}