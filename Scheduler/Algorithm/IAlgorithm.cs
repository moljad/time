using System.Collections.Generic;
using Scheduler.Model;

namespace Scheduler.Algorithm
{
    public interface IAlgorithm
    {
        List<MappedActivity> Generate(Schedule schedule, List<Activity> activities, List<State> states, List<UserGroup> groups);
    }
}