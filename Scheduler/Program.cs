﻿using Scheduler.Algorithm;
using Scheduler.Model;
using System;
using System.Collections.Generic;

namespace Scheduler
{
    class Program
    {
        static void Main(string[] args)
        {
            var testCases = new List<TestCase>()
            {
                TestCase.DefaultCase(),
            };

            var algorithms = new List<IAlgorithm>() {
                new DummyAlgorithm(),
                new WalkingAlgorithm(),
            };

            foreach (var testCase in testCases)
            {
                foreach (var algorithm in algorithms)
                {
                    var result = Scheduler.Generate(algorithm, testCase.Schedule, testCase.Activities, testCase.States, testCase.UserGroups);

                    Console.WriteLine($"+++ {algorithm.GetType().Name}");
                    Console.WriteLine($"  Quality: {result.Quality}");

                    foreach (var property in result.Properties)
                    {
                        Console.WriteLine($"  {property.Item1}: {property.Item2}");
                    }

                    Console.WriteLine("  @ Schedule:");
                    foreach (var mappedActivity in result.MappedActivities)
                    {
                        Console.WriteLine($"  {Span.ToDateString(mappedActivity.Span)} {mappedActivity.Activity.Name}");
                    }
                }
            }
        }
    }
}
