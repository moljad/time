using System.Collections.Generic;

namespace Scheduler.Model
{
    public class User
    {
        public string UserName { get; set; }

        public Schedule Schedule { get; set; }

        public List<State> States { get; set; }

        public List<UserGroup> Groups { get; set; }

        public List<Activity> Activities { get; set; }
    }
}