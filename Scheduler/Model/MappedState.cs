namespace Scheduler.Model
{
    public class MappedState
    {
        public Schedule Schedule { get; set; }

        public Span Span { get; set; }

        public State State { get; set; }
    }
}