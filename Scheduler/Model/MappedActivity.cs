namespace Scheduler.Model
{
    public class MappedActivity
    {
        public virtual Activity Activity { get; set; }

        public virtual Schedule Schedule { get; set; }

        public Span Span { get; set; }
    }
}