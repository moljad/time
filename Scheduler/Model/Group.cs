using System.Collections.Generic;

namespace Scheduler.Model
{
    public class Group
    {
        public string Name { get; set; }

        public List<UserGroup> Members { get; set; }

        public virtual List<Activity> Activities { get; set; }
    }
}