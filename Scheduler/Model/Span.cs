﻿using System;

namespace Scheduler.Model
{
    public class Span
    {
        public Span()
        {
            Begin = 0;
            End = 0;
        }

        public Span(int begin, int end)
        {
            Begin = begin;
            End = end;

            if (begin > end)
            {
                throw new Exception($"Wrong span values: ({begin}, {end})");
            }
        }

        public int Begin { get; private set; }

        public int End { get; private set; }

        public static bool Contains(Span a, Span b)
        {
            return a.Begin <= b.Begin && a.End <= b.End;
        }

        public static Span GetIntersection(Span a, Span b)
        {
            var begin = Math.Max(a.Begin, b.Begin);
            var end = Math.Min(a.End, b.End);

            end = Math.Max(begin, end);

            return new Span(begin, end);
        }

        public static int GetSize(Span a)
        {
            return Math.Max(0, a.End - a.Begin);
        }

        public static bool Intersects(Span a, Span b)
        {
            if (a.Begin <= b.Begin && b.Begin < a.End)
            {
                return true;
            }

            if (a.End < b.End && b.End <= a.End)
            {
                return true;
            }

            return false;
        }

        public static int GetDistance(Span a, Span b)
        {
            return Math.Max(a.Begin - b.End, b.Begin - a.End);
        }

        public static string ToDateString(Span a)
        {
            var dateS = TimeSpan.FromMinutes(a.Begin * 10);
            var dateE = TimeSpan.FromMinutes(a.End * 10);

            var hS = dateS.Hours < 10 ? "0" + dateS.Hours.ToString() : dateS.Hours.ToString();
            var hE = dateE.Hours < 10 ? "0" + dateE.Hours.ToString() : dateE.Hours.ToString();

            var mS = dateS.Minutes == 0 ? "00" : dateS.Minutes.ToString();
            var mE = dateE.Minutes == 0 ? "00" : dateE.Minutes.ToString();

            return $"Day {dateS.Days + 1} _ {hS}:{mS}-{hE}:{mE}";
        }
    }
}