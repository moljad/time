using System.Collections.Generic;

namespace Scheduler.Model
{
    public class Activity
    {
        public bool IsRest { get; set; }

        public int LengthMin { get; set; }

        public int LengthMax { get; set; }

        public int LengthRequired { get; set; }

        public string Name { get; set; }

        public double Priority { get; set; }

        public Group Group { get; set; }

        public virtual User User { get; set; }

        public List<State> States { get; set; }
    }
}