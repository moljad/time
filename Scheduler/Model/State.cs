using System.Collections.Generic;

namespace Scheduler.Model
{
    public class State
    {
        public string Name { get; set; }

        public virtual User Owner { get; set; }

        public List<Activity> Activities { get; set; }
    }
}