using System.Collections.Generic;

namespace Scheduler.Model
{
    public class Schedule
    {
        public bool Active { get; set; }

        public bool Alternating { get; set; }

        public int RestLength { get; set; }

        public User User { get; set; }

        public List<MappedActivity> MappedActivities { get; set; }

        public List<MappedState> MappedStates { get; set; }
    }
}