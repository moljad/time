package com.example.personalTimeManager.repository

import com.example.personalTimeManager.repository.database.entity.*
import com.example.personalTimeManager.repository.server.dto.*
import com.example.personalTimeManager.repository.service.*
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class AppRepository @Inject constructor(
    private val userRepositoryService: UserRepositoryService,
    private val scheduleRepositoryService: ScheduleRepositoryService,
    private val stateRepositoryService: StateRepositoryService,
    private val activityRepositoryService: ActivityRepositoryService,
    private val mappedStateRepositoryService: MappedStateRepositoryService,
    private val activityStateRepositoryService: ActivityStateRepositoryService,
    private val mappedActivityRepositoryService: MappedActivityRepositoryService
) {
    companion object {
        private const val DEFAULT_TIMEOUT = 10
        private const val DEFAULT_RETRY_ATTEMPTS = 0L
    }

    fun getCurrentUser(): Single<UserEntity> {
        return userRepositoryService.getCurrentUser().compose(singleTransformer())
    }

    fun getScheduleByUserId(userId: Int?): Single<Schedule> {
        if (userId == null) {
            return userRepositoryService.guessCurrentUser()
                .flatMap { scheduleRepositoryService.getScheduleByUserId(it.id!!) }
                .compose(singleTransformer())
        }
        return scheduleRepositoryService.getScheduleByUserId(userId).compose(singleTransformer())
    }

    fun getStatesByUserId(userId: Int?): Single<List<State>> {
        if (userId == null) {
            return userRepositoryService.guessCurrentUser()
                .flatMap { stateRepositoryService.getStatesByUserId(it.id!!) }
                .compose(singleTransformer())
        }

        return stateRepositoryService.getStatesByUserId(userId).compose(singleTransformer())
    }

    fun getStateByIdAndUserId(id: Int, userId: Int? = null): Maybe<State> {
        if (userId == null) {
            return userRepositoryService.guessCurrentUser().flatMapMaybe {
                stateRepositoryService.getStateByUserIdAndId(it.id!!, id)
                    .compose(maybeTransformer())
            }
        }

        return stateRepositoryService.getStateByUserIdAndId(userId, id).compose(maybeTransformer())
    }

    fun createState(createDto: StateCreateDto, userId: Int? = null): Single<State> {
        if (userId == null) {
            return userRepositoryService.guessCurrentUser().flatMap {
                stateRepositoryService.createStateAndGetItById(
                    it.id!!,
                    createDto
                )
            }.compose(singleTransformer())
        }

        return stateRepositoryService.createStateAndGetItById(userId, createDto)
    }


    fun updateState(id: Int, updateDto: StateUpdateDto): Single<State> {
        return stateRepositoryService.updateStateAndGetItById(id, updateDto)
            .compose(singleTransformer())
    }

    fun deleteState(id: Int): Completable {
        return stateRepositoryService.deleteState(id).compose(completableTransformer())
    }

    fun getActivitiesByUserId(userId: Int? = null): Single<List<Activity>> {
        if (userId == null) {
            return userRepositoryService.guessCurrentUser()
                .flatMap { activityRepositoryService.getActivitiesByUserId(it.id!!) }
                .compose(singleTransformer())
        }

        return activityRepositoryService.getActivitiesByUserId(userId).compose(singleTransformer())
    }

    fun getActivityByUserIdAndId(id: Int, userId: Int? = null): Maybe<Activity> {
        if (userId == null) {
            return userRepositoryService.guessCurrentUser()
                .flatMapMaybe { activityRepositoryService.getActivityByUserIdAndId(it.id!!, id) }
                .compose(maybeTransformer())
        }

        return activityRepositoryService.getActivityByUserIdAndId(userId, id)
            .compose(maybeTransformer())
    }

    fun createActivity(createDto: ActivityCreateDto, userId: Int? = null): Single<Activity> {
        if (userId == null) {
            return userRepositoryService.guessCurrentUser()
                .flatMap {
                    activityRepositoryService.createActivityAndGetItById(
                        it.id!!,
                        createDto
                    )
                }
                .compose(singleTransformer())
        }

        return activityRepositoryService.createActivityAndGetItById(userId, createDto)
            .compose(singleTransformer())
    }

    fun updateActivity(id: Int, updateDto: ActivityUpdateDto): Single<Activity> {
        return activityRepositoryService.updateActivityAndGetItById(id, updateDto)
            .compose(singleTransformer())
    }

    fun deleteActivity(id: Int): Completable {
        return activityRepositoryService.deleteActivity(id).compose(completableTransformer())
    }

    fun getMappedStatesByUserId(userId: Int? = null): Single<List<MappedState>> {
        if (userId == null) {
            return userRepositoryService.guessCurrentUser().flatMap {
                mappedStateRepositoryService.getMappedStatesByUserId(it.id!!)
                    .compose(singleTransformer())
            }
        }

        return mappedStateRepositoryService.getMappedStatesByUserId(userId)
            .compose(singleTransformer())
    }

    /**if it persists in local, than gets userId of connected state.
     * if it is not - will take parameter. If parameter is null - guesses currentUser**/
    fun getMappedStateByIdAndUserId(id: Int, userId: Int? = null): Maybe<MappedState> {
        if (userId == null) {
            return mappedStateRepositoryService.guessUserIdByMappedStateId(id)
                .flatMap { guessedUserId ->
                    mappedStateRepositoryService.getMappedStateByUserIdAndId(guessedUserId, id)
                }
        }

        return mappedStateRepositoryService.getMappedStateByUserIdAndId(userId, id)
    }

    fun createMappedState(
        createDto: MappedStateCreateDto,
        userId: Int? = null
    ): Single<MappedState> {
        if (userId == null) {
            return stateRepositoryService.guessUserIdByStateId(createDto.stateId)
                .switchIfEmpty(userRepositoryService.guessCurrentUser().map { it.id })
                .flatMap {
                    mappedStateRepositoryService.createMappedStateAndGetItById(
                        it,
                        createDto
                    )
                }.compose(singleTransformer())
        }

        return mappedStateRepositoryService.createMappedStateAndGetItById(userId, createDto).compose(singleTransformer())
    }

    fun updateMappedState(id: Int, updateDto: MappedStateUpdateDto): Single<MappedState> {
        return mappedStateRepositoryService.updateMappedStateAndGetItById(id, updateDto)
            .compose(singleTransformer())
    }

    fun deleteMappedState(id: Int): Completable {
        return mappedStateRepositoryService.deleteMappedState(id).compose(completableTransformer())
    }

    fun getMappedActivities(userId: Int? = null): Single<List<MappedActivity>> {
        if (userId == null) {
            return userRepositoryService.guessCurrentUser().flatMap {
                mappedActivityRepositoryService.getMappedActivitiesByUserId(it.id!!)
                    .compose(singleTransformer())
            }
        }
        return mappedActivityRepositoryService.getMappedActivitiesByUserId(userId)
            .compose(singleTransformer())
    }

    fun getMappedActivity(id: Int, userId: Int? = null): Maybe<MappedActivity> {
        if (userId == null) {
            return userRepositoryService.guessCurrentUser().flatMapMaybe {
                mappedActivityRepositoryService.getMappedActivityByUserIdAndId(it.id!!, id)
                    .compose(maybeTransformer())
            }
        }

        return mappedActivityRepositoryService.getMappedActivityByUserIdAndId(userId, id)
            .compose(maybeTransformer())
    }

    fun generateAndGetMappedActivities(userId: Int? = null): Single<List<MappedActivity>> {
        if (userId == null) {
            return userRepositoryService.guessCurrentUser().flatMap {
                mappedActivityRepositoryService.generateAndGetMappedActivities(it.id!!)
                    .compose(singleTransformer())
            }
        }

        return mappedActivityRepositoryService.generateAndGetMappedActivities(userId)
            .compose(singleTransformer())
    }

    fun createMappedActivity(
        createDto: MappedActivityCreateDto,
        userId: Int? = null
    ): Single<MappedActivity> {
        if (userId == null) {
            return activityRepositoryService.guessUserIdByActivityId(createDto.activityId)
                .switchIfEmpty(userRepositoryService.guessCurrentUser().map { it.id })
                .flatMap {
                    mappedActivityRepositoryService.createMappedActivityAndGetItById(
                        it,
                        createDto
                    )
                }.compose(singleTransformer())
        }

        return mappedActivityRepositoryService.createMappedActivityAndGetItById(userId, createDto).compose(singleTransformer())
    }

    fun updateMappedActivity(id: Int, updateDto: MappedActivityUpdateDto): Single<MappedActivity> {
        return mappedActivityRepositoryService.updateMappedActivityAndGetItById(id, updateDto)
            .compose(singleTransformer())
    }

    fun deleteMappedActivity(id: Int): Completable {
        return mappedActivityRepositoryService.deleteMappedActivity(id)
            .compose(completableTransformer())
    }

    /**If userId is null, then tries to get it with local activity.
     * If local activity is not found, then tries to synchronize it and get it again.
     * If it is still not found, then WTF? ok, i'll try to get it again with local state.
     * If local state is not found, then tries to synchronize it and get it again.
     * If it is still not found, then FUCK YOU! i've already tried 2 synchronizations.
     * In this case it will guess current user (by the way, it may be another synchronization)
     *
     * BY THE WAY YOU CAN JUST PASS VALID USER ID AND PASS THIS CHAIN OF SEARCHING USER ID
     */
    fun connectActivityAndStateByIds(
        activityId: Int,
        stateId: Int,
        userId: Int? = null
    ): Single<ActivityState> {
        if (userId == null) {
            return activityRepositoryService.guessUserIdByActivityId(activityId)
                .switchIfEmpty(stateRepositoryService.guessUserIdByStateId(stateId))
                .switchIfEmpty(userRepositoryService.guessCurrentUser().map { it.id })
                .flatMap {
                    activityStateRepositoryService.connectActivityAndStateByIds(
                        it,
                        activityId,
                        stateId
                    )
                }.compose(singleTransformer())
        }
        return activityStateRepositoryService.connectActivityAndStateByIds(
            userId,
            activityId,
            stateId
        ).compose(singleTransformer())
    }

    fun getActivityStatesByStateId(stateId: Int) {
        //TODO implement API
    }

    fun getActivityStatesByActivityId(
        activityId: Int,
        userId: Int? = null
    ): Single<List<ActivityState>> {
        if (userId == null) {
            return activityRepositoryService.guessUserIdByActivityId(activityId)
                .switchIfEmpty(userRepositoryService.guessCurrentUser().map { it.id })
                .flatMap {
                    activityStateRepositoryService.getActivityStatesByActivityId(
                        it,
                        activityId
                    )
                }.compose(singleTransformer())
        }
        return activityStateRepositoryService.getActivityStatesByActivityId(userId, activityId)
            .compose(singleTransformer())
    }

    fun disconnectActivityAndStateByIds(
        activityId: Int,
        stateId: Int,
        userId: Int? = null
    ): Completable {
        if (userId == null) {
            return activityRepositoryService.guessUserIdByActivityId(activityId)
                .switchIfEmpty(stateRepositoryService.guessUserIdByStateId(stateId))
                .switchIfEmpty(userRepositoryService.guessCurrentUser().map { it.id })
                .flatMapCompletable {
                    activityStateRepositoryService.disconnectActivityAndStateByIds(
                        it,
                        activityId,
                        stateId
                    )
                }.compose(completableTransformer())
        }
        return activityStateRepositoryService.disconnectActivityAndStateByIds(
            userId,
            activityId,
            stateId
        )
            .compose(completableTransformer())
    }

    private fun completableTransformer(): CompletableTransformer = CompletableTransformer {
        it.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .retry(DEFAULT_RETRY_ATTEMPTS)
            .timeout(DEFAULT_TIMEOUT.toLong(), TimeUnit.SECONDS)
    }

    private fun <T> singleTransformer(): SingleTransformer<T, T> = SingleTransformer {
        it.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .retry(DEFAULT_RETRY_ATTEMPTS)
            .timeout(DEFAULT_TIMEOUT.toLong(), TimeUnit.SECONDS)
    }

    private fun <T> maybeTransformer(): MaybeTransformer<T, T> = MaybeTransformer {
        it.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .retry(DEFAULT_RETRY_ATTEMPTS)
            .timeout(DEFAULT_TIMEOUT.toLong(), TimeUnit.SECONDS)
    }
}

