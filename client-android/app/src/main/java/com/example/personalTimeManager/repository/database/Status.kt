package com.example.personalTimeManager.repository.database

enum class Status {
    UNKNOWN,
    LOCAL,
    LOCAL_TO_UPDATE,
    LOCAL_TO_CREATE,
    LOCAL_TO_DELETE,
    SYNCHRONIZED
}