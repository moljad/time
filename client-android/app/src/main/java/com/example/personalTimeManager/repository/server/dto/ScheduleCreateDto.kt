package com.example.personalTimeManager.repository.server.dto

import com.google.gson.annotations.SerializedName

data class ScheduleCreateDto(
    @SerializedName("active")
    val active: Boolean?,
    @SerializedName("alternating")
    val alternating: Boolean?,
    @SerializedName("restLength")
    val restLength: Int?,
    @SerializedName("activityUsualLenght")
    val activityUsualLenght: Int?
)