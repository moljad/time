package com.example.personalTimeManager.repository.server

import android.util.Log
import com.example.personalTimeManager.repository.database.entity.*
import com.example.personalTimeManager.repository.server.dto.*
import io.reactivex.Completable
import io.reactivex.CompletableTransformer
import io.reactivex.Single
import io.reactivex.SingleTransformer
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

open class ServerCommunicator(private val mService: ApiService) {

    companion object {
        private const val DEFAULT_TIMEOUT = 10
        private const val DEFAULT_RETRY_ATTEMPTS = 3L
    }

    open fun getCurrentUser(): Single<UserEntity> {
        return mService.getCurrentUser()
            .compose(singleTransformer())
            .doOnError { t: Throwable -> Log.e("ServerCommunicator", t.message!!) }
    }

    open fun getSchedule(userId: Int): Single<Schedule> {
        return mService.getSchedule(userId.toString())
            .compose(singleTransformer())
            .doOnError { t: Throwable -> Log.e("ServerCommunicator", t.message!!) }
    }

    open fun createSchedule(userId: Int, scheduleCreateDto: ScheduleCreateDto): Single<Schedule> {
        return mService.createSchedule(userId.toString(), scheduleCreateDto)
            .compose(singleTransformer())
            .doOnError { t: Throwable -> Log.e("ServerCommunicator", t.message!!) }
    }

    open fun getStates(userId: Int): Single<List<State>> {
        return mService.getStates(userId.toString())
            .compose(singleTransformer())
            .doOnError { t: Throwable -> Log.e("ServerCommunicator", t.message!!) }
    }

    open fun getState(userId: Int, id: Int): Single<State> {
        return mService.getState(userId.toString(), id.toString())
            .compose(singleTransformer())
            .doOnError { t: Throwable -> Log.e("ServerCommunicator", t.message!!) }
    }

    open fun createState(userId: Int, createDto: StateCreateDto): Single<State> {
        return mService.createState(userId.toString(), createDto)
            .compose(singleTransformer())
            .doOnError { t: Throwable -> Log.e("ServerCommunicator", t.message!!) }
    }

    open fun updateState(userId: Int, stateId: Int, updateDto: StateUpdateDto): Single<State> {
        return mService.updateState(userId.toString(), stateId.toString(), updateDto)
            .compose(singleTransformer())
            .doOnError { t: Throwable -> Log.e("ServerCommunicator", t.message!!) }
    }

    open fun deleteState(userId: Int, stateId: Int): Completable {
        return mService.deleteState(userId.toString(), stateId.toString())
            .compose(completableTransformer())
            .doOnError { t: Throwable -> Log.e("ServerCommunicator", t.message!!) }
    }

    open fun getActivities(userId: Int): Single<List<Activity>> {
        return mService.getActivities(userId.toString())
            .compose(singleTransformer())
            .doOnError { t: Throwable -> Log.e("ServerCommunicator", t.message!!) }
    }

    open fun getActivity(userId: Int, id: Int): Single<Activity> {
        return mService.getActivity(userId.toString(), id.toString())
            .compose(singleTransformer())
            .doOnError { t: Throwable -> Log.e("ServerCommunicator", t.message!!) }
    }

    open fun createActivity(userId: Int, createDto: ActivityCreateDto): Single<Activity> {
        return mService.createActivity(userId.toString(), createDto)
            .compose(singleTransformer())
            .doOnError { t: Throwable -> Log.e("ServerCommunicator", t.message!!) }
    }

    open fun updateActivity(
        userId: Int,
        stateId: Int,
        updateDto: ActivityUpdateDto
    ): Single<Activity> {
        return mService.updateActivity(userId.toString(), stateId.toString(), updateDto)
            .compose(singleTransformer())
            .doOnError { t: Throwable -> Log.e("ServerCommunicator", t.message!!) }
    }

    open fun deleteActivity(userId: Int, stateId: Int): Completable {
        return mService.deleteActivity(userId.toString(), stateId.toString())
            .compose(completableTransformer())
            .doOnError { t: Throwable -> Log.e("ServerCommunicator", t.message!!) }
    }

    open fun getActivityStates(userId: Int, activityId: Int): Single<List<ActivityStateDto>> {
        return mService.getActivityState(userId.toString(), activityId.toString())
            .compose(singleTransformer())
            .doOnError { t: Throwable -> Log.e("ServerCommunicator", t.message!!) }
    }

    open fun createActivityState(userId: Int, activityId: Int, stateId: Int): Completable {
        return mService.createActivityState(
            userId.toString(),
            activityId.toString(),
            stateId.toString()
        )
            .compose(completableTransformer())
            .doOnError { t: Throwable -> Log.e("ServerCommunicator", t.message!!) }
    }

    open fun deleteActivityState(userId: Int, activityId: Int, stateId: Int): Completable {
        return mService.deleteActivityState(
            userId.toString(),
            activityId.toString(),
            stateId.toString()
        )
            .compose(completableTransformer())
            .doOnError { t: Throwable -> Log.e("ServerCommunicator", t.message!!) }
    }

    open fun getMappedStates(userId: Int): Single<List<MappedState>> {
        return mService.getMappedStates(userId.toString())
            .compose(singleTransformer())
            .doOnError { t: Throwable -> Log.e("ServerCommunicator", t.message!!) }
    }

    open fun getMappedState(userId: Int, id: Int): Single<MappedState> {
        return mService.getMappedState(userId.toString(), id.toString())
            .compose(singleTransformer())
            .doOnError { t: Throwable -> Log.e("ServerCommunicator", t.message!!) }
    }

    open fun createMappedState(userId: Int, createDto: MappedStateCreateDto): Single<MappedState> {
        return mService.createMappedState(userId.toString(), createDto)
            .compose(singleTransformer())
            .doOnError { t: Throwable -> Log.e("ServerCommunicator", t.message!!) }
    }

    open fun updateMappedState(
        userId: Int,
        mappedStateId: Int,
        updateDto: MappedStateUpdateDto
    ): Single<MappedState> {
        return mService.updateMappedState(userId.toString(), mappedStateId.toString(), updateDto)
            .compose(singleTransformer())
            .doOnError { t: Throwable -> Log.e("ServerCommunicator", t.message!!) }
    }

    open fun deleteMappedState(userId: Int, mappedStateId: Int): Completable {
        return mService.deleteMappedState(userId.toString(), mappedStateId.toString())
            .compose(completableTransformer())
            .doOnError { t: Throwable -> Log.e("ServerCommunicator", t.message!!) }
    }

    open fun getMappedActivities(userId: Int): Single<List<MappedActivity>> {
        return mService.getMappedActivities(userId.toString())
            .compose(singleTransformer())
            .doOnError { t: Throwable -> Log.e("ServerCommunicator", t.message!!) }
    }

    open fun getMappedActivity(userId: Int, id: Int): Single<MappedActivity> {
        return mService.getMappedActivity(userId.toString(), id.toString())
            .compose(singleTransformer())
            .doOnError { t: Throwable -> Log.e("ServerCommunicator", t.message!!) }
    }

    open fun generateAndGetMappedActivities(userId: Int): Single<List<MappedActivity>> {
        return mService.generateAndGetMappedActivities(userId.toString())
            .compose(singleTransformer())
            .doOnError { t: Throwable -> Log.e("ServerCommunicator", t.message!!) }
    }

    open fun createMappedActivity(
        userId: Int,
        createDto: MappedActivityCreateDto
    ): Single<MappedActivity> {
        return mService.createMappedActivity(userId.toString(), createDto)
            .compose(singleTransformer())
            .doOnError { t: Throwable -> Log.e("ServerCommunicator", t.message!!) }
    }

    open fun updateMappedActivity(
        userId: Int,
        mappedActivityId: Int,
        updateDto: MappedActivityUpdateDto
    ): Single<MappedActivity> {
        return mService.updateMappedActivity(
            userId.toString(),
            mappedActivityId.toString(),
            updateDto
        )
            .compose(singleTransformer())
            .doOnError { t: Throwable -> Log.e("ServerCommunicator", t.message!!) }
    }

    open fun deleteMappedActivity(userId: Int, mappedActivityId: Int): Completable {
        return mService.deleteMappedActivity(userId.toString(), mappedActivityId.toString())
            .compose(completableTransformer())
            .doOnError { t: Throwable -> Log.e("ServerCommunicator", t.message!!) }
    }

    private fun <T> singleTransformer(): SingleTransformer<T, T> = SingleTransformer {
        it.subscribeOn(Schedulers.io())
            .retry(DEFAULT_RETRY_ATTEMPTS)
            .timeout(DEFAULT_TIMEOUT.toLong(), TimeUnit.SECONDS)
    }

    private fun completableTransformer(): CompletableTransformer = CompletableTransformer {
        it.subscribeOn(Schedulers.io())
            .retry(DEFAULT_RETRY_ATTEMPTS)
            .timeout(DEFAULT_TIMEOUT.toLong(), TimeUnit.SECONDS)
    }
}