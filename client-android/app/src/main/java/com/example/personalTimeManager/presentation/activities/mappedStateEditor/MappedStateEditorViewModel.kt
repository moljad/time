package com.example.personalTimeManager.presentation.activities.mappedStateEditor

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.example.personalTimeManager.presentation.base.BaseViewModel
import com.example.personalTimeManager.repository.AppRepository
import com.example.personalTimeManager.repository.database.entity.MappedState
import com.example.personalTimeManager.repository.database.entity.State
import com.example.personalTimeManager.repository.server.dto.MappedStateCreateDto
import com.example.personalTimeManager.repository.server.dto.MappedStateUpdateDto
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

class MappedStateEditorViewModel(application: Application, private val repository: AppRepository) : BaseViewModel(application) {
    val mappedState: MutableLiveData<MappedStateModel> = MutableLiveData()
    var states: List<State> = listOf()

    fun getMappedState(id: Int): Maybe<MappedState> {
        return repository.getMappedStateByIdAndUserId(id)
    }

    fun updateMappedState(id: Int, mappedStateUpdateDto: MappedStateUpdateDto): Single<MappedState> {
        return repository.updateMappedState(id, mappedStateUpdateDto)
    }

    fun deleteMappedState(id: Int): Completable {
        return repository.deleteMappedState(id)
    }

    fun createMappedState(mappedStateCreateDto: MappedStateCreateDto): Single<MappedState> {
        return repository.createMappedState(mappedStateCreateDto)
    }

    fun getStates(): Single<List<State>> {
        return repository.getStatesByUserId(null)
    }

    data class MappedStateModel(
        var id: Int,
        var begin: Int,
        var end: Int,
        var stateId: Int
    )
}

