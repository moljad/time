package com.example.personalTimeManager.repository.database.entity

import androidx.room.*
import com.example.personalTimeManager.R
import com.example.personalTimeManager.repository.CustomColor
import com.example.personalTimeManager.repository.database.Status
import com.example.personalTimeManager.repository.database.converter.ColorConverters
import com.example.personalTimeManager.repository.database.converter.DateConverters
import com.example.personalTimeManager.repository.server.dto.ActivityCreateDto
import com.example.personalTimeManager.repository.server.dto.ActivityUpdateDto
import com.example.personalTimeManager.repository.server.gson.Exclude
import com.google.gson.annotations.SerializedName
import java.time.LocalDateTime

@Entity(
    tableName = "Activities",
    foreignKeys = [ForeignKey(
        entity = UserEntity::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("userId"),
        onUpdate = ForeignKey.CASCADE,
        onDelete = ForeignKey.CASCADE
    )]
)
data class Activity(
    @PrimaryKey
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("priority")
    val priority: Int,
    @SerializedName("isRest")
    val isRest: Boolean,
    @SerializedName("link")
    val link: String,
    @SerializedName("lengthMin")
    val lengthMin: Int,
    @SerializedName("lengthMax")
    val lengthMax: Int,
    @SerializedName("lengthRequired")
    val lengthRequired: Int,
    @TypeConverters(ColorConverters::class)
    @SerializedName("color")
    val color: CustomColor,
    @ColumnInfo(name = "userId", index = true)
    @SerializedName("userId")
    val userId: Int,
    @TypeConverters(DateConverters::class)
    @SerializedName("createdDate")
    val createdDate: LocalDateTime,
    @TypeConverters(DateConverters::class)
    @SerializedName("updatedDate")
    val updatedDate: LocalDateTime,
    @Exclude
    val status: Status = Status.UNKNOWN
) {

    @Suppress("unused")
    private constructor() : this(
        -1073741824, "", "", 0, false, "",
        0, 6, 0, CustomColor(R.color.colorAccent),
        -1073741824, LocalDateTime.now(), LocalDateTime.now()
    )

    @Exclude
    @delegate:Exclude
    @delegate:Ignore
    val activityCreateDto by lazy {
        ActivityCreateDto(
            this.name, this.description, this.priority, this.isRest, this.link,
            this.lengthMin, this.lengthMax, this.lengthRequired, this.color
        )
    }

    @Exclude
    @delegate:Exclude
    @delegate:Ignore
    val activityUpdateDto by lazy {
        ActivityUpdateDto(
            this.name, this.description, this.priority, this.isRest, this.link,
            this.lengthMin, this.lengthMax, this.lengthRequired, this.color
        )
    }
}