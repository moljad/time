package com.example.personalTimeManager.repository.database.entity

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.example.personalTimeManager.repository.database.Status
import com.example.personalTimeManager.repository.server.gson.Exclude
import com.google.gson.annotations.SerializedName

@Entity(
    tableName = "AspNetUsers",
    indices = [Index(
        value = ["id"],
        unique = true
    )]
)
data class UserEntity(
    @SerializedName("id")
    val id: Int?,
    @SerializedName("userName")
    val userName: String?,
    @PrimaryKey
    @SerializedName("email")
    val email: String,
    @SerializedName("password")
    val password: String, //sorry
    @SerializedName("isCurrentUser")
    val isCurrentUser: Boolean = false,
    @Exclude
    val status: Status = Status.UNKNOWN
) {
    @Suppress("unused")
    private constructor() : this(null, null, "", "")
}