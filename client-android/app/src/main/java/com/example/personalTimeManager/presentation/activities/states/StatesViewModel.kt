package com.example.personalTimeManager.presentation.activities.states

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.example.personalTimeManager.presentation.base.BaseViewModel
import com.example.personalTimeManager.repository.AppRepository
import com.example.personalTimeManager.repository.database.entity.State
import io.reactivex.Single

class StatesViewModel(application: Application, private val repository: AppRepository) :
    BaseViewModel(application) {
    val states: MutableLiveData<List<State>> = MutableLiveData(listOf())

    fun getStates(): Single<List<State>> {
        return repository.getCurrentUser().flatMap {
            repository.getStatesByUserId(it.id!!)
        }
    }
}