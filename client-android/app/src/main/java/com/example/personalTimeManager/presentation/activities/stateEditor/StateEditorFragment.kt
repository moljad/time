package com.example.personalTimeManager.presentation.activities.stateEditor

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.example.personalTimeManager.R
import com.example.personalTimeManager.databinding.FragmentStateEditorBinding
import com.example.personalTimeManager.di.component.ViewModelComponent
import com.example.personalTimeManager.presentation.base.BaseFragment
import com.example.personalTimeManager.repository.CustomColor
import com.example.personalTimeManager.repository.server.dto.StateCreateDto
import com.example.personalTimeManager.repository.server.dto.StateUpdateDto
import com.google.android.material.floatingactionbutton.FloatingActionButton
import dev.sasikanth.colorsheet.ColorSheet
import io.reactivex.functions.Consumer
import javax.inject.Inject


class StateEditorFragment : BaseFragment() {
    lateinit var viewModel: StateEditorViewModel
        @Inject set

    var defaultColor: Int = 0

    override fun injectDependency(component: ViewModelComponent) {
        component.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentStateEditorBinding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_state_editor, container, false
            )
        binding.fragment = this
        binding.lifecycleOwner = this

        val fab: FloatingActionButton = container!!.rootView.findViewById(R.id.fab)
        fab.hide()

        val view = binding.root

        val updateButtonView: Button = view.findViewById(R.id.update)
        updateButtonView.setOnClickListener(this::onUpdateButtonPressed)

        val deleteButtonView: Button = view.findViewById(R.id.delete)
        deleteButtonView.setOnClickListener(this::onDeleteButtonPressed)

        val createButtonView: Button = view.findViewById(R.id.create)
        createButtonView.setOnClickListener(this::onCreateButtonPressed)

        val colors = resources.getIntArray(R.array.entityColors)

        defaultColor = colors[0]

        val changeColor: View = view.findViewById(R.id.changeColor)
        changeColor.setOnClickListener {
            ColorSheet().colorPicker(colors,
                listener = {
                    viewModel.state.value!!.color = it
                    viewModel.state.postValue(viewModel.state.value)
                    Log.e(
                        it.toString(),
                        CustomColor(it).stringColor + " " + CustomColor(it).intColor
                    )
                }
            ).show(activity!!.supportFragmentManager)
        }

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val stateId = StateEditorFragmentArgs.fromBundle(arguments!!).stateId

        Log.e("s", stateId.toString())

        if (stateId != 0) {
            viewModel.getState(stateId).subscribe {
                viewModel.state.postValue(
                    StateEditorViewModel.StateModel(
                        stateId,
                        it.name,
                        it.description,
                        it.color.intColor
                    )
                )
            }
        } else {
            viewModel.state.postValue(
                StateEditorViewModel.StateModel(
                    0,
                    "",
                    "",
                    defaultColor
                )
            )
        }
    }

    fun onUpdateButtonPressed(v: View) {
        //Toast.makeText(this.context, "Updated: " + viewModel.getModel(), Toast.LENGTH_LONG).show()
        val stateUpdateDto = StateUpdateDto(
            viewModel.state.value?.name,
            viewModel.state.value?.description,
            CustomColor(viewModel.state.value?.color!!)
        )

        viewModel.updateState(viewModel.state.value?.id!!, stateUpdateDto).subscribe(Consumer {
            findNavController().popBackStack()
        })
    }

    fun onDeleteButtonPressed(v: View) {
        viewModel.deleteState(viewModel.state.value?.id!!).subscribe {
            findNavController().popBackStack()
        }
    }

    fun onCreateButtonPressed(v: View) {
        val stateCreateDto = StateCreateDto(
            viewModel.state.value?.name,
            viewModel.state.value?.description,
            CustomColor(viewModel.state.value?.color!!)
        )

        viewModel.createState(stateCreateDto).subscribe(Consumer {
            findNavController().popBackStack()
        })
    }
}
