package com.example.personalTimeManager.repository

import com.google.gson.annotations.SerializedName

data class Span(
    @SerializedName("begin")
    val begin: Int,
    @SerializedName("end")
    val end: Int
)