package com.example.personalTimeManager.presentation.activities.login

import android.app.Application
import android.content.res.Resources
import androidx.lifecycle.MutableLiveData
import com.example.personalTimeManager.presentation.base.BaseViewModel
import com.example.personalTimeManager.repository.database.entity.UserEntity
import com.example.personalTimeManager.repository.user.UserManager
import io.reactivex.Completable
import io.reactivex.internal.observers.CallbackCompletableObserver
import io.reactivex.internal.observers.ConsumerSingleObserver
import io.reactivex.internal.operators.maybe.MaybeCallbackObserver

class LoginRegistrationViewModel(
    application: Application,
    private val userManager: UserManager
) : BaseViewModel(application) {
    var loginModel: MutableLiveData<LoginModel> = MutableLiveData()
    var registrationModel: MutableLiveData<RegistrationModel> = MutableLiveData()

    val liveDataCurrentUser = MutableLiveData<UserEntity?>()
    val liveDataAreUsersExist = MutableLiveData<Boolean>()

    init {
        loginModel.value =
            LoginModel(
                "",
                ""
            )
        registrationModel.value =
            RegistrationModel(
                "",
                "",
                ""
            )
    }


    fun refreshCurrentUser() {
        val currentUserObserver: MaybeCallbackObserver<UserEntity> = MaybeCallbackObserver(
            { user -> liveDataCurrentUser.setValue(user) },
            { t -> throw t },
            { liveDataCurrentUser.setValue(null) })
        userManager.getCurrentUser()
            .subscribe(currentUserObserver)
    }

    fun refreshAreUsersExist() {
        val consumerSingleObserver = ConsumerSingleObserver<Boolean>(
            { areUserExist -> liveDataAreUsersExist.postValue(areUserExist) },
            { t -> throw t })
        userManager.areUsersExists().subscribe(consumerSingleObserver)
    }

    fun registerUser(registrationModel: RegistrationModel) {
        val email = registrationModel.email
        val password = registrationModel.password
        val repeatPassword = registrationModel.repeatPassword
        if (password != repeatPassword) throw RuntimeException("please repeat password")

        val createLocalNewUserAndMakeItActual =
            userManager.createLocalActualUser(email, password)

        // to update livedata
        val callbackCompletableObserver = CallbackCompletableObserver(
            { t -> throw t },
            { refreshCurrentUser() }
        )

        createLocalNewUserAndMakeItActual.subscribe(callbackCompletableObserver)

        refreshCurrentUser() // to update livedata
    }

    fun loginUser(loginModel: LoginModel) {
        val email = loginModel.email
        val password = loginModel.password

        val loginOrRegisterAndChangeCurrentUser = userManager.makeExistingUserActual(email)
            .onErrorResumeNext { t: Throwable ->
                if (t is Resources.NotFoundException) {
                    return@onErrorResumeNext userManager.createLocalActualUser(
                        email,
                        password
                    )
                }
                Completable.error(t)
            }.andThen(userManager.getCurrentUser())

        // to update livedata
        val liveDataCurrentUserObserver: MaybeCallbackObserver<UserEntity> = MaybeCallbackObserver(
            { user -> liveDataCurrentUser.setValue(user) },
            { t -> throw t },
            { liveDataCurrentUser.setValue(null) })
        loginOrRegisterAndChangeCurrentUser.subscribe(liveDataCurrentUserObserver)
    }

    data class LoginModel(var email: String, var password: String)

    data class RegistrationModel(
        var email: String,
        var password: String,
        var repeatPassword: String
    )
}