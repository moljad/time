package com.example.personalTimeManager.repository.service

import android.content.res.Resources
import android.util.Log
import com.example.personalTimeManager.repository.database.entity.UserEntity
import com.example.personalTimeManager.repository.server.ServerCommunicator
import com.example.personalTimeManager.repository.user.UserManager
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.rxkotlin.zipWith
import java.io.IOException
import java.util.*
import java.util.concurrent.TimeoutException
import javax.inject.Inject

class UserRepositoryService @Inject constructor(
    private val userManager: UserManager,
    private val serverCommunicator: ServerCommunicator
) {

    fun guessCurrentUser(): Single<UserEntity> {
        return getLocalUser().flatMap {
            //trying to get it from local first
            if (it.id == null) {
                return@flatMap Maybe.empty<UserEntity>()
            }
            return@flatMap Maybe.just(it)
        }
            .switchIfEmpty(getCurrentUser())        //this will synchronize with remote and try local again
    }

    fun getCurrentUser(): Single<UserEntity> {
        val communicatorUser = getRemoteUser()
        val localUser = getLocalUser()
            .map { return@map Optional.of(it) }.toSingle(Optional.empty())

        return communicatorUser.zipWith(localUser).flatMap { (communicatorUser, localUser) ->
            if (communicatorUser.isPresent && localUser.isPresent) {
                val synchronizedUser = UserEntity(
                    communicatorUser.get().id,
                    communicatorUser.get().userName,
                    localUser.get().email,
                    localUser.get().password,
                    localUser.get().isCurrentUser
                )
                return@flatMap refreshLocalUserAndGetIt(synchronizedUser)

            }
            if (!communicatorUser.isPresent && localUser.isPresent) {
                Log.i("AppRepository", "returning current user from local database instead")
                return@flatMap Single.just(localUser.get())
            }
            return@flatMap Single.error<UserEntity>(Resources.NotFoundException("can not synchronize with local user. Local user does not exist"))
        }
    }

    private fun refreshLocalUserAndGetIt(synchronizedUser: UserEntity): Single<UserEntity> {
        return userManager.updateUser(synchronizedUser).andThen(getLocalUser()).toSingle()
    }

    private fun getRemoteUser(): Single<Optional<UserEntity>> {
        return serverCommunicator.getCurrentUser()
            .map { Optional.of(it) }
            .onErrorResumeNext { t: Throwable ->
                if (t is IOException || t is TimeoutException) { //probably connection issue
                    Log.e("AppRepository", t.message!!)
                    return@onErrorResumeNext Single.just(Optional.empty<UserEntity>())
                }
                Single.error(t)
            }
    }

    fun getLocalUser(): Maybe<UserEntity> {
        return userManager.getCurrentUser()
    }
}