package com.example.personalTimeManager.repository.database.viewentities

import androidx.room.Embedded
import androidx.room.Relation
import com.example.personalTimeManager.repository.database.entity.ActivityState
import com.example.personalTimeManager.repository.database.entity.State


data class StateWithActivityIds(
    @Embedded val state: State,
    @Relation(
        parentColumn = "id",
        entityColumn = "stateId",
        entity = ActivityState::class,
        projection = ["activityId"]
    ) val activityIdList: List<Int>
)