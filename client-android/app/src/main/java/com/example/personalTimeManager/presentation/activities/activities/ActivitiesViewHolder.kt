package com.example.personalTimeManager.presentation.activities.activities

import android.graphics.Color
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.personalTimeManager.R

class ActivitiesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val textView: TextView = itemView.findViewById(R.id.activity_name)
    private val colorView: View = itemView.findViewById(R.id.activity_color) as View

    fun setName(string: String) {
        textView.text = string
    }

    fun setColor(color: String) {
        colorView.setBackgroundColor(Color.parseColor(color))
    }
}