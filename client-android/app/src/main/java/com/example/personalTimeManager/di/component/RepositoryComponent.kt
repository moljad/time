package com.example.personalTimeManager.di.component

import com.example.personalTimeManager.di.module.RepositoryModule
import com.example.personalTimeManager.di.scope.RepositoryScope
import com.example.personalTimeManager.repository.AppRepository
import com.example.personalTimeManager.repository.service.*
import dagger.Component

@RepositoryScope
@Component(
    modules = [RepositoryModule::class],
    dependencies = [ApiComponent::class, DatabaseComponent::class, UserComponent::class]
)
interface RepositoryComponent {
    val repository: AppRepository
    val userRepositoryService: UserRepositoryService
    val scheduleRepositoryService: ScheduleRepositoryService
    val stateRepositoryService: StateRepositoryService
    val activityRepositoryService: ActivityRepositoryService
    val mappedStateRepositoryService: MappedStateRepositoryService
    val activityStateRepositoryService: ActivityStateRepositoryService
}