package com.example.personalTimeManager.di.module

import com.example.personalTimeManager.di.scope.RepositoryScope
import com.example.personalTimeManager.repository.AppRepository
import com.example.personalTimeManager.repository.database.AppDatabase
import com.example.personalTimeManager.repository.server.ServerCommunicator
import com.example.personalTimeManager.repository.service.*
import com.example.personalTimeManager.repository.user.UserManager
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule {

    @Provides
    @RepositoryScope
    internal fun providesRepository(
        userRepositoryService: UserRepositoryService,
        scheduleRepositoryService: ScheduleRepositoryService,
        stateRepositoryService: StateRepositoryService,
        activityRepositoryService: ActivityRepositoryService,
        mappedStateRepositoryService: MappedStateRepositoryService,
        activityStateRepositoryService: ActivityStateRepositoryService,
        mappedActivityRepositoryService: MappedActivityRepositoryService
    ): AppRepository {
        return AppRepository(
            userRepositoryService,
            scheduleRepositoryService,
            stateRepositoryService,
            activityRepositoryService,
            mappedStateRepositoryService,
            activityStateRepositoryService,
            mappedActivityRepositoryService
        )
    }

    @Provides
    @RepositoryScope
    internal fun providesUserRepositoryService(
        userManager: UserManager,
        serverCommunicator: ServerCommunicator
    ): UserRepositoryService {
        return UserRepositoryService(userManager, serverCommunicator)
    }

    @Provides
    @RepositoryScope
    internal fun providesScheduleRepositoryService(
        serverCommunicator: ServerCommunicator,
        mainDatabase: AppDatabase
    ): ScheduleRepositoryService {
        return ScheduleRepositoryService(serverCommunicator, mainDatabase)
    }

    @Provides
    @RepositoryScope
    internal fun providesStateRepositoryService(
        userRepositoryService: UserRepositoryService,
        serverCommunicator: ServerCommunicator,
        mainDatabase: AppDatabase
    ): StateRepositoryService {
        return StateRepositoryService(userRepositoryService, serverCommunicator, mainDatabase)
    }

    @Provides
    @RepositoryScope
    internal fun providesActivityRepositoryService(
        userRepositoryService: UserRepositoryService,
        serverCommunicator: ServerCommunicator,
        mainDatabase: AppDatabase
    ): ActivityRepositoryService {
        return ActivityRepositoryService(userRepositoryService, serverCommunicator, mainDatabase)
    }

    @Provides
    @RepositoryScope
    internal fun providesMappedStateRepositoryService(
        userRepositoryService: UserRepositoryService,
        stateRepositoryService: StateRepositoryService,
        scheduleRepositoryService: ScheduleRepositoryService,
        serverCommunicator: ServerCommunicator,
        mainDatabase: AppDatabase
    ): MappedStateRepositoryService {
        return MappedStateRepositoryService(
            userRepositoryService,
            stateRepositoryService,
            scheduleRepositoryService,
            serverCommunicator,
            mainDatabase
        )
    }

    @Provides
    @RepositoryScope
    internal fun providesMappedActivityRepositoryService(
        userRepositoryService: UserRepositoryService,
        activityRepositoryService: ActivityRepositoryService,
        scheduleRepositoryService: ScheduleRepositoryService,
        serverCommunicator: ServerCommunicator,
        mainDatabase: AppDatabase
    ): MappedActivityRepositoryService {
        return MappedActivityRepositoryService(
            userRepositoryService,
            activityRepositoryService,
            scheduleRepositoryService,
            serverCommunicator,
            mainDatabase
        )
    }

    @Provides
    @RepositoryScope
    internal fun providesActivityStateRepositoryService(
        serverCommunicator: ServerCommunicator,
        activityRepositoryService: ActivityRepositoryService,
        stateRepositoryService: StateRepositoryService,
        mainDatabase: AppDatabase
    ): ActivityStateRepositoryService {
        return ActivityStateRepositoryService(
            serverCommunicator,
            activityRepositoryService,
            stateRepositoryService,
            mainDatabase
        )
    }
}