package com.example.personalTimeManager.presentation.activities.activityEditor

class ActivityStateElement(var id: Int, var name: String, var isEnabled: Boolean)