package com.example.personalTimeManager.di.module

import android.app.Application
import com.example.personalTimeManager.App
import com.example.personalTimeManager.di.scope.ViewModelScope
import com.example.personalTimeManager.presentation.activities.activities.ActivitiesViewModel
import com.example.personalTimeManager.presentation.activities.activityEditor.ActivityEditorViewModel
import com.example.personalTimeManager.presentation.activities.activitySchedule.ActivityScheduleViewModel
import com.example.personalTimeManager.presentation.activities.groupEditor.GroupEditorViewModel
import com.example.personalTimeManager.presentation.activities.groups.GroupsViewModel
import com.example.personalTimeManager.presentation.activities.login.LoginRegistrationViewModel
import com.example.personalTimeManager.presentation.activities.main.MainActivityViewModel
import com.example.personalTimeManager.presentation.activities.mappedActivityEditor.MappedActivityEditorViewModel
import com.example.personalTimeManager.presentation.activities.mappedStateEditor.MappedStateEditorViewModel
import com.example.personalTimeManager.presentation.activities.scheduleEditor.ScheduleEditorViewModel
import com.example.personalTimeManager.presentation.activities.settings.SettingsViewModel
import com.example.personalTimeManager.presentation.activities.stateEditor.StateEditorViewModel
import com.example.personalTimeManager.presentation.activities.stateSchedule.StateScheduleViewModel
import com.example.personalTimeManager.presentation.activities.states.StatesViewModel
import com.example.personalTimeManager.repository.AppRepository
import com.example.personalTimeManager.repository.user.UserManager
import dagger.Module
import dagger.Provides

@Module
class ViewModelModule(app: App) {
    internal var app: Application = app

    @ViewModelScope
    @Provides
    internal fun providesMainActivityViewModel(repository: AppRepository): MainActivityViewModel {
        return MainActivityViewModel(
            app,
            repository
        )
    }

    @ViewModelScope
    @Provides
    internal fun providesLoginRegistrationViewModel(userManager: UserManager): LoginRegistrationViewModel {
        return LoginRegistrationViewModel(
            app,
            userManager
        )
    }

    @ViewModelScope
    @Provides
    internal fun providesStatesViewModel(repository: AppRepository): StatesViewModel {
        return StatesViewModel(app, repository)
    }

    @ViewModelScope
    @Provides
    internal fun providesStateEditorViewModel(repository: AppRepository): StateEditorViewModel {
        return StateEditorViewModel(app, repository)
    }

    @ViewModelScope
    @Provides
    internal fun providesActivitiesViewModel(repository: AppRepository): ActivitiesViewModel {
        return ActivitiesViewModel(app, repository)
    }

    @ViewModelScope
    @Provides
    internal fun providesActivityEditorViewModel(repository: AppRepository): ActivityEditorViewModel {
        return ActivityEditorViewModel(app, repository)
    }

    @ViewModelScope
    @Provides
    internal fun providesScheduleViewModel(repository: AppRepository): ActivityScheduleViewModel {
        return ActivityScheduleViewModel(app, repository)
    }

    @ViewModelScope
    @Provides
    internal fun providesGroupsViewModel(repository: AppRepository): GroupsViewModel {
        return GroupsViewModel(app, repository)
    }

    @ViewModelScope
    @Provides
    internal fun providesGroupEditorViewModel(repository: AppRepository): GroupEditorViewModel {
        return GroupEditorViewModel(app, repository)
    }

    @ViewModelScope
    @Provides
    internal fun providesSettingsViewModel(repository: AppRepository): SettingsViewModel {
        return SettingsViewModel(app, repository)
    }

    @ViewModelScope
    @Provides
    internal fun providesMappedStateEditorViewModel(repository: AppRepository): MappedStateEditorViewModel {
        return MappedStateEditorViewModel(app, repository)
    }

    @ViewModelScope
    @Provides
    internal fun providesMappedActivityEditorViewModel(repository: AppRepository): MappedActivityEditorViewModel {
        return MappedActivityEditorViewModel(app, repository)
    }

    @ViewModelScope
    @Provides
    internal fun providesScheduleEditorViewModel(repository: AppRepository): ScheduleEditorViewModel {
        return ScheduleEditorViewModel(app, repository)
    }

    @ViewModelScope
    @Provides
    internal fun providesStateScheduleViewModel(repository: AppRepository): StateScheduleViewModel {
        return StateScheduleViewModel(app, repository)
    }
}