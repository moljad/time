package com.example.personalTimeManager.presentation.activities.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.RelativeLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.personalTimeManager.R
import com.example.personalTimeManager.databinding.ActivityLoginRegistrationBinding
import com.example.personalTimeManager.di.component.ViewModelComponent
import com.example.personalTimeManager.presentation.activities.main.MainActivity
import com.example.personalTimeManager.presentation.base.BaseActivity
import com.example.personalTimeManager.util.dpToPx
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent.registerEventListener
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener
import net.yslibrary.android.keyboardvisibilityevent.Unregistrar
import javax.inject.Inject


class LoginRegistrationActivity : BaseActivity(), LoginFragment.OnFragmentInteractionListener,
    RegistrationFragment.OnFragmentInteractionListener {
    var viewModel: LoginRegistrationViewModel? = null
        @Inject set

    private var login: LoginFragment =
        LoginFragment()
    private var registration: RegistrationFragment =
        RegistrationFragment()

    var unregistrar: Unregistrar? = null

    companion object {
        @JvmStatic
        fun newInstance(context: Context): Intent {
            val intent = Intent(context, LoginRegistrationActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            return intent
        }
    }

    private fun redirectToMain() {
        startActivity(MainActivity.newInstance(this))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Objects.requireNonNull(supportActionBar)?.setDisplayHomeAsUpEnabled(true)
        //setTheme(R.style.AppTheme)

        initViewModel()
        val binding: ActivityLoginRegistrationBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_login_registration)
        binding.lifecycleOwner = this
        binding.viewModel = this.viewModel


        val layoutToMove = findViewById<RelativeLayout>(R.id.loginRegistrationRelativeLayout)
        val displayMetrics = resources.displayMetrics
        val listener = LayoutMover(layoutToMove, displayMetrics)
        unregistrar = registerEventListener(
            this,
            listener
        )
    }


    override fun onStop() {
        super.onStop()

        // call this method when you don't need the event listener anymore
        unregistrar?.unregister()
    }

    override fun injectDependency(component: ViewModelComponent) {
        component.inject(this)
    }

    private fun initViewModel() {
        viewModel?.liveDataAreUsersExist
            ?.observe(
                this,
                Observer { it?.let { if (!it) initFragment(registration) } })

        viewModel?.liveDataCurrentUser
            ?.observe(
                this,
                Observer {
                    if (it == null) {
                        if (viewModel?.liveDataAreUsersExist?.value == false) {
                            initFragment(registration)
                        } else {
                            initFragment(login)
                        }
                    } else {
                        redirectToMain()
                    }
                })

        viewModel?.refreshCurrentUser()
        viewModel?.refreshAreUsersExist()
    }

    override fun onLoginButtonPressed(view: View) {
        login.onLoginButtonPressed()
    }

    override fun onRegistrationRedirectTextPressed(view: View) {
        login.onRegistrationRedirectTextPressed(registration)
    }

    override fun onRegistrationButtonPressed(view: View) {
        registration.onRegistrationButtonPressed()
    }

    override fun onLoginRedirectTextPressed(view: View) {
        registration.onLoginRedirectTextPressed(login)
    }

    private fun initFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        //transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onBackPressed() {
        val fm = supportFragmentManager
        if (fm.backStackEntryCount > 0) {
            Log.i("LoginRegActivity", "popping backstack")
            fm.popBackStack()
        } else {
            Log.i("LoginRegActivity", "nothing on backstack, calling super")
            super.onBackPressed()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    class LayoutMover(
        private val relativeLayout: RelativeLayout,
        private val displayMetrics: DisplayMetrics
    ) : KeyboardVisibilityEventListener {
        override fun onVisibilityChanged(isOpen: Boolean) { // some code depending on keyboard visiblity status

            if (isOpen) {
                val layoutParams = FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    500.dpToPx(displayMetrics)
                )
                relativeLayout.layoutParams = layoutParams
            } else {
                val layoutParams = FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
                )
                relativeLayout.layoutParams = layoutParams
            }
        }
    }
}