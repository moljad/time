package com.example.personalTimeManager.presentation.activities.stateSchedule

import android.graphics.RectF
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.alamkanak.weekview.WeekView
import com.alamkanak.weekview.WeekViewDisplayable
import com.example.personalTimeManager.R
import com.example.personalTimeManager.di.component.ViewModelComponent
import com.example.personalTimeManager.presentation.base.BaseFragment
import com.example.personalTimeManager.repository.CustomColor
import com.example.personalTimeManager.repository.database.entity.MappedState
import com.example.personalTimeManager.repository.database.entity.State
import com.example.personalTimeManager.util.CustomDateTimeInterpreter
import com.example.personalTimeManager.util.getStartOfTheWeek
import com.google.android.material.floatingactionbutton.FloatingActionButton
import io.reactivex.functions.BiFunction
import java.util.*
import javax.inject.Inject

class StateScheduleFragment : BaseFragment() {

    lateinit var viewModel: StateScheduleViewModel
        @Inject set

    override fun injectDependency(component: ViewModelComponent) {
        component.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_state_schedule, container, false)
        val fab: FloatingActionButton = container!!.rootView.findViewById(R.id.fab)
        fab.show()

        val schedule: WeekView<StateScheduleEvent> = view.findViewById(R.id.weekView)
        schedule.dateTimeInterpreter = CustomDateTimeInterpreter()

        viewModel.getStates()
            .zipWith(
                viewModel.getMappedStates(),
                BiFunction { a: List<State>, b: List<MappedState> -> a to b })
            .subscribe { (a, b) ->
                val events = convertToEvents(a, b)
                schedule.submit(events)
            }

        schedule.setOnEventClickListener(this::onWeekEventClick)

        fab.setOnClickListener(this::onFabClick)

        return view

    }

    private fun onWeekEventClick(event: StateScheduleEvent, rect: RectF) {
        Log.e("Navigation", """Open Mapped State Editor with id ${event.mappedStateId}""")

        val action =
            StateScheduleFragmentDirections.actionNavStateScheduleToNavMappedStateEditor(
                event.mappedStateId
            )

        findNavController().navigate(action)
    }

    private fun convertToEvents(
        activities: List<State>,
        mappedStates: List<MappedState>
    ): List<WeekViewDisplayable<StateScheduleEvent>> {
        val names = activities.map { it.id to it.name }.toMap()
        val colors = activities.map { it.id to it.color }.toMap()
        val startOfWeek = Calendar.getInstance().getStartOfTheWeek()

        return mappedStates.mapIndexed { id: Int, a: MappedState ->
            val name = if (names[a.stateId] != null) names[a.stateId]!! else "EMPTY NAME"
            val color = if (colors[a.stateId] != null) colors[a.stateId]!! else CustomColor(0)

            StateScheduleEvent(
                id.toLong(),
                a.id,
                name,
                startOfWeek,
                a.span,
                color.intColor
            )
        }
    }

    private fun onFabClick(v: View) {
        Log.e("Navigation", "Open Mapped State Editor with id 0 (NEW)")

        val action =
            StateScheduleFragmentDirections.actionNavStateScheduleToNavMappedStateEditor(
                0
            )

        findNavController().navigate(action)
    }
}
