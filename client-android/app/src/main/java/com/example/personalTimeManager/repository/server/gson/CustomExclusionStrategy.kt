package com.example.personalTimeManager.repository.server.gson

import com.google.gson.ExclusionStrategy
import com.google.gson.FieldAttributes

object CustomExclusionStrategy : ExclusionStrategy {
    override fun shouldSkipClass(clazz: Class<*>?): Boolean {
        return false
    }

    override fun shouldSkipField(f: FieldAttributes?): Boolean {
        return f?.getAnnotation(Exclude::class.java) != null
    }
}