package com.example.personalTimeManager.presentation.activities.activityEditor

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.example.personalTimeManager.presentation.base.BaseViewModel
import com.example.personalTimeManager.repository.AppRepository
import com.example.personalTimeManager.repository.database.entity.Activity
import com.example.personalTimeManager.repository.database.entity.ActivityState
import com.example.personalTimeManager.repository.database.entity.State
import com.example.personalTimeManager.repository.server.dto.ActivityCreateDto
import com.example.personalTimeManager.repository.server.dto.ActivityUpdateDto
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

class ActivityEditorViewModel(application: Application, private val repository: AppRepository) :
    BaseViewModel(application) {
    val activity: MutableLiveData<ActivityModel> = MutableLiveData()
    var oldActivityStates: List<ActivityState> = listOf()

    fun getActivity(id: Int): Maybe<Activity> {
        return repository.getActivityByUserIdAndId(id)
    }

    fun updateActivity(id: Int, activityUpdateDto: ActivityUpdateDto): Single<Activity> {
        return repository.updateActivity(id, activityUpdateDto)
    }

    fun deleteActivity(id: Int): Completable {
        return repository.deleteActivity(id)
    }

    fun createActivity(activityCreateDto: ActivityCreateDto): Single<Activity> {
        return repository.createActivity(activityCreateDto)
    }

    fun getStates(): Single<List<State>> {
        return repository.getStatesByUserId(null)
    }

    fun getActivityStates(id: Int): Single<List<ActivityState>> {
        return repository.getActivityStatesByActivityId(id)
    }

    fun connectActivityState(activityId: Int, stateId: Int): Single<ActivityState> {
        return repository.connectActivityAndStateByIds(activityId, stateId)
    }

    fun disconnectActivityState(activityId: Int, stateId: Int): Completable {
        return repository.disconnectActivityAndStateByIds(activityId, stateId)
    }

    data class ActivityModel(
        var id: Int,
        var name: String,
        var description: String,
        var priority: Int,
        var isRest: Boolean,
        var link: String,
        var lengthMin: Int,
        var lengthMax: Int,
        var lengthRequired: Int,
        var color: Int
    )
}
