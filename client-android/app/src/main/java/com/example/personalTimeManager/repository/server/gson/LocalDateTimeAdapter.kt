package com.example.personalTimeManager.repository.server.gson

import com.example.personalTimeManager.repository.database.converter.DateConverters
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type
import java.time.LocalDateTime

object LocalDateTimeAdapter : JsonDeserializer<LocalDateTime> {
    override fun deserialize(
        json: JsonElement,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): LocalDateTime {
        return LocalDateTime.parse(
            json.asJsonPrimitive.asString.substring(0, 22),
            DateConverters.dateTimeFormatter
        )
    }
}