package com.example.personalTimeManager.repository.server.okhttpclient

import java.io.IOException

class NoConnectivityException(message: String = "No Internet Connection") : IOException(message)