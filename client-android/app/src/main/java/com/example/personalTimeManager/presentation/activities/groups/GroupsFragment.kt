package com.example.personalTimeManager.presentation.activities.groups

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.personalTimeManager.R
import com.example.personalTimeManager.di.component.ViewModelComponent
import com.example.personalTimeManager.presentation.base.BaseFragment
import javax.inject.Inject

class GroupsFragment : BaseFragment() {

    lateinit var groupsViewModel: GroupsViewModel
        @Inject set

    override fun injectDependency(component: ViewModelComponent) {
        component.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_groups, container, false)
    }
}