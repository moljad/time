package com.example.personalTimeManager.repository.server.okhttpclient

import com.example.personalTimeManager.repository.server.security.AccessTokenRepository
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class AuthorizationInterceptor(private val accessTokenRepository: AccessTokenRepository) :
    Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val accessToken = accessTokenRepository.accessToken
        if (accessToken != null) {
            return chain.proceed(
                newRequestWithAccessToken(
                    chain.request(),
                    accessToken
                )
            )
        }
        synchronized(this) {
            val repeatCheck = accessTokenRepository.accessToken
            if (repeatCheck != null) {
                return chain.proceed(
                    newRequestWithAccessToken(
                        chain.request(),
                        repeatCheck
                    )
                )
            }

            accessTokenRepository.refreshAccessToken()
            val refreshedAccessToken = accessTokenRepository.accessToken
            if (refreshedAccessToken != null) {
                return chain.proceed(
                    newRequestWithAccessToken(
                        chain.request(),
                        refreshedAccessToken
                    )
                )
            }
        }

        return chain.proceed(chain.request())
    }

    private fun newRequestWithAccessToken(request: Request, accessToken: String): Request {
        return request.newBuilder()
            .header("Authorization", "Bearer $accessToken")
            .build()
    }
}