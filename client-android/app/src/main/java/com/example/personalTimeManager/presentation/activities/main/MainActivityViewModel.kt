package com.example.personalTimeManager.presentation.activities.main

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.example.personalTimeManager.presentation.base.BaseViewModel
import com.example.personalTimeManager.repository.AppRepository
import com.example.personalTimeManager.repository.database.entity.Schedule
import com.example.personalTimeManager.repository.database.entity.UserEntity
import io.reactivex.SingleObserver
import io.reactivex.internal.observers.ConsumerSingleObserver

class MainActivityViewModel(
    application: Application,
    private val mRepository: AppRepository
) : BaseViewModel(application) {
    var currentUser = MutableLiveData<UserEntity?>()
    var schedule = MutableLiveData<Schedule?>()

    fun refreshCurrentUser() {
        val consumerSingleObserver: SingleObserver<UserEntity> =
            ConsumerSingleObserver(
                { newUserValue ->
                    currentUser.postValue(newUserValue)
                    newUserValue.id?.let { refreshSchedule(it) }
                },
                { t -> throw t })
        mRepository.getCurrentUser().subscribe(consumerSingleObserver)
    }

    fun refreshSchedule(userId: Int) {
        val consumerSingleObserver: SingleObserver<Schedule> =
            ConsumerSingleObserver(
                {
                    schedule.postValue(it)
                },
                { t -> throw t })
        mRepository.getScheduleByUserId(userId).subscribe(consumerSingleObserver)
    }

}