package com.example.personalTimeManager.presentation.activities.activities

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.observe
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.personalTimeManager.R
import com.example.personalTimeManager.di.component.ViewModelComponent
import com.example.personalTimeManager.presentation.base.BaseAdapter
import com.example.personalTimeManager.presentation.base.BaseFragment
import com.example.personalTimeManager.repository.database.entity.Activity
import com.google.android.material.floatingactionbutton.FloatingActionButton
import javax.inject.Inject


class ActivitiesFragment : BaseFragment() {
    lateinit var viewModel: ActivitiesViewModel
        @Inject set

    override fun injectDependency(component: ViewModelComponent) {
        component.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_activities, container, false)

        val fab: FloatingActionButton = container!!.rootView.findViewById(R.id.fab)
        fab.show()
        fab.setOnClickListener(this::onFabClick)

        val recyclerView: RecyclerView = view.findViewById(R.id.activities_list)

        val adapter =
            object : BaseAdapter<ActivitiesViewHolder, Activity>(context!!, mutableListOf()) {
                override fun onBindViewHolder(holder: ActivitiesViewHolder, position: Int) {
                    val activity = list[position]

                    holder.setName(activity.name)
                    holder.setColor(activity.color.stringColor)
                }

                override fun onCreateViewHolder(
                    parent: ViewGroup,
                    viewType: Int
                ): ActivitiesViewHolder {
                    val recycleView =
                        inflater.inflate(R.layout.recycle_activity_item, parent, false)

                    recycleView.setOnClickListener {
                        val position = recyclerView.getChildAdapterPosition(it)
                        val activity: Activity = getItem(position)
                        val action =
                            ActivitiesFragmentDirections.actionNavActivitiesToNavActivityEditor(
                                activity.id
                            )

                        recycleView.findNavController().navigate(action)
                    }

                    return ActivitiesViewHolder(recycleView)
                }
            }

        recyclerView.adapter = adapter

        viewModel.activities.observe(this) {
            Log.e("Activity list set", it.size.toString())
            adapter.clear()
            adapter.addAll(it)
        }

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.getActivities().subscribe(
            viewModel.activities::postValue
        ) {
            Log.e("StatesViewModel", "", it)
        }
    }

    fun onFabClick(v: View) {
        val action = ActivitiesFragmentDirections.actionNavActivitiesToNavActivityEditor(0)

        findNavController().navigate(action)
    }
}