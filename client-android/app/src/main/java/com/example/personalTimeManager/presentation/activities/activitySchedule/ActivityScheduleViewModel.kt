package com.example.personalTimeManager.presentation.activities.activitySchedule

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.example.personalTimeManager.presentation.base.BaseViewModel
import com.example.personalTimeManager.repository.AppRepository
import com.example.personalTimeManager.repository.database.entity.Activity
import com.example.personalTimeManager.repository.database.entity.MappedActivity
import com.example.personalTimeManager.repository.server.dto.MappedActivityCreateDto
import io.reactivex.Completable
import io.reactivex.Single

class ActivityScheduleViewModel(application: Application, private val repository: AppRepository) :
    BaseViewModel(application) {
    val activities: MutableLiveData<List<Activity>> = MutableLiveData(listOf())
    val mappedActivities: MutableLiveData<List<MappedActivity>> = MutableLiveData(listOf())

    fun getActivities(): Single<List<Activity>> {
        return repository.getActivitiesByUserId()
    }

    fun getGeneratedMappedActivities(): Single<List<MappedActivity>> {
        return repository.generateAndGetMappedActivities()
    }

    fun deleteMappedActivity(id: Int): Completable {
        return repository.deleteMappedActivity(id)
    }

    fun getMappedActivities(): Single<List<MappedActivity>> {
        return repository.getMappedActivities()
    }

    fun createMappedActivity(mappedActivityCreateDto: MappedActivityCreateDto): Single<MappedActivity> {
        return repository.createMappedActivity(mappedActivityCreateDto)
    }
}