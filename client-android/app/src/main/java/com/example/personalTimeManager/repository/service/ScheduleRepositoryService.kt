package com.example.personalTimeManager.repository.service

import android.content.res.Resources
import android.util.Log
import com.example.personalTimeManager.repository.database.AppDatabase
import com.example.personalTimeManager.repository.database.entity.Schedule
import com.example.personalTimeManager.repository.server.ServerCommunicator
import com.example.personalTimeManager.repository.server.dto.ScheduleCreateDto
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.rxkotlin.zipWith
import retrofit2.HttpException
import java.io.IOException
import java.util.*
import java.util.concurrent.TimeoutException
import javax.inject.Inject

class ScheduleRepositoryService @Inject constructor(
    private val serverCommunicator: ServerCommunicator,
    private val mainDatabase: AppDatabase
) {

    fun getLocalScheduleById(id: Int): Maybe<Schedule> {
        return mainDatabase.scheduleDao().getById(id)
    }

    fun getScheduleByUserId(userId: Int): Single<Schedule> {
        val communicatorSchedule = getRemoteScheduleOrCreateAndGetIfNotExist(userId)

        val localSchedule =
            getLocalScheduleByUserId(userId).map { Optional.of(it) }.toSingle(Optional.empty())
        val zipWith = communicatorSchedule.zipWith(localSchedule)
        return zipWith.flatMap { (communicatorSchedule, localSchedule) ->
            if (communicatorSchedule.isPresent) {
                return@flatMap refreshLocalScheduleAndGetIt(communicatorSchedule.get(), userId)
            }
            if (!communicatorSchedule.isPresent && localSchedule.isPresent) {
                Log.i("AppRepository", "returning local schedule instead")
                return@flatMap Single.just(localSchedule.get())
            }
            return@flatMap Single.error<Schedule>(Resources.NotFoundException("No schedules were ever fetched from server and server is unavailable"))
        }
    }

    private fun getLocalScheduleByUserId(userId: Int): Maybe<Schedule> {
        return mainDatabase.scheduleDao().getSchedulesByUserId(userId)
            .flatMapMaybe { list ->
                if (list.isEmpty()) {
                    return@flatMapMaybe Maybe.empty<Schedule>()
                }
                return@flatMapMaybe Maybe.just(list.maxBy { it.id }!!)
            }
    }

    private fun getRemoteScheduleOrCreateAndGetIfNotExist(userId: Int): Single<Optional<Schedule>> {
        return getRemoteScheduleByUserId(userId).onErrorResumeNext { t ->
            if (t is HttpException && t.code() == 500) {
                Log.w("AppRepository", "server did not created schedule?")
                return@onErrorResumeNext createDefaultRemoteScheduleAndGetIt(userId)
            }
            Single.error(t)
        }
    }

    private fun getRemoteScheduleByUserId(userId: Int): Single<Optional<Schedule>> {
        return serverCommunicator.getSchedule(userId)
            .map { Optional.of(it) }
            .onErrorResumeNext { t ->
                if (t is IOException || t is TimeoutException) {
                    Log.e("AppRepository", t.message!!)
                    return@onErrorResumeNext Single.just(Optional.empty())
                }
                Single.error(t)
            }
    }

    private fun createDefaultRemoteScheduleAndGetIt(userId: Int): Single<Optional<Schedule>> {
        return createDefaultRemoteSchedule(userId).flatMap { getRemoteScheduleByUserId(userId) }
    }

    private fun createDefaultRemoteSchedule(userId: Int): Single<Schedule> {
        return serverCommunicator.createSchedule(
            userId,
            ScheduleCreateDto(null, null, null, null)
        ).doOnError { t: Throwable ->
            Log.e("AppRepository", "tried to create schedule. May be error?", t)
        }
    }

    private fun refreshLocalScheduleAndGetIt(
        newSchedule: Schedule,
        userId: Int
    ): Single<Schedule> {
        return createOrReplaceSchedule(newSchedule).andThen(getLocalScheduleByUserId(userId))
            .toSingle()
    }


    private fun createOrReplaceSchedule(newSchedule: Schedule): Completable {
        return Completable.fromCallable { mainDatabase.scheduleDao().upsert(newSchedule) }
    }
}