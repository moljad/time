package com.example.personalTimeManager.repository.server.dto

import com.example.personalTimeManager.repository.database.Status
import com.example.personalTimeManager.repository.database.entity.ActivityState
import com.example.personalTimeManager.repository.server.gson.Exclude


data class ActivityStateDto(
    val id: Int?,
    val activityId: Int,
    val stateId: Int
) {
    @Suppress("unused")
    private constructor() : this(null, -1, -1)

    @Exclude
    @delegate:Exclude
    val activityState by lazy {
        ActivityState(activityId, stateId, Status.SYNCHRONIZED)
    }
}
