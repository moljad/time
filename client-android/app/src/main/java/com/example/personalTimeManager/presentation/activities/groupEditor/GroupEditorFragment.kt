package com.example.personalTimeManager.presentation.activities.groupEditor

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.personalTimeManager.R
import com.example.personalTimeManager.di.component.ViewModelComponent
import com.example.personalTimeManager.presentation.base.BaseFragment
import com.google.android.material.floatingactionbutton.FloatingActionButton
import javax.inject.Inject

class GroupEditorFragment : BaseFragment() {

    lateinit var viewModel: GroupEditorViewModel
        @Inject set

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fab: FloatingActionButton = container!!.rootView.findViewById(R.id.fab)
        fab.hide()

        return inflater.inflate(R.layout.fragment_group_editor, container, false)
    }

    override fun injectDependency(component: ViewModelComponent) {
        component.inject(this)
    }

}
