package com.example.personalTimeManager.presentation.activities.stateEditor

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.example.personalTimeManager.presentation.base.BaseViewModel
import com.example.personalTimeManager.repository.AppRepository
import com.example.personalTimeManager.repository.CustomColor
import com.example.personalTimeManager.repository.database.entity.State
import com.example.personalTimeManager.repository.server.dto.StateCreateDto
import com.example.personalTimeManager.repository.server.dto.StateUpdateDto
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

class StateEditorViewModel(application: Application, private val repository: AppRepository) :
    BaseViewModel(application) {
    val state: MutableLiveData<StateModel> = MutableLiveData()

    fun getState(id: Int): Maybe<State> {
        return repository.getStateByIdAndUserId(id)
    }

    fun updateState(id: Int, stateUpdateDto: StateUpdateDto): Single<State> {
        return repository.updateState(id, stateUpdateDto)
    }

    fun deleteState(id: Int): Completable {
        return repository.deleteState(id)
    }

    fun createState(stateCreateDto: StateCreateDto): Single<State> {
        return repository.createState(stateCreateDto)
    }

    data class StateModel(
        var id: Int,
        var name: String,
        var description: String,
        var color: Int
    )
}
