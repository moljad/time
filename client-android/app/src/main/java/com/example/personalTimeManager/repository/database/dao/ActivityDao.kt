package com.example.personalTimeManager.repository.database.dao

import androidx.room.Dao
import androidx.room.Query
import com.example.personalTimeManager.repository.database.dao.base.BaseDao
import com.example.personalTimeManager.repository.database.entity.Activity
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
abstract class ActivityDao : BaseDao<Activity>() {
    @Query("SELECT * FROM Activities")
    abstract fun getAll(): Single<List<Activity>>

    @Query("SELECT * FROM Activities WHERE id = :id")
    abstract fun getById(id: Int): Maybe<Activity>

    @Query("SELECT * FROM Activities WHERE userId = :userId")
    abstract fun getActivitiesByUserId(userId: Int): Single<List<Activity>>

    @Query("SELECT MIN(id) FROM Activities")
    abstract fun getMinId(): Int

    @Query(
        """UPDATE Activities SET id =:id , name = :name, description = :description, 
        priority = :priority, isRest = :isRest, link = :link, 
        lengthMin =:lengthMin, lengthMax = :lengthMax, lengthRequired = :lengthRequired,
        color = :color, userId = :userId, createdDate = :createdDate, updatedDate = :updatedDate,
        status = :status
        WHERE id = :oldActivityId"""
    )
    abstract fun replace(
        oldActivityId: Int,
        id: Int, name: String, description: String, priority: Int, isRest: Boolean, link: String,
        lengthMin: Int, lengthMax: Int, lengthRequired: Int, color: String, userId: Int,
        createdDate: String, updatedDate: String, status: String
    ): Completable
}