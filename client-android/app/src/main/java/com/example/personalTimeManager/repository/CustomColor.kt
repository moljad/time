package com.example.personalTimeManager.repository

import android.graphics.Color

class CustomColor {
    val intColor: Int
    val stringColor: String

    constructor(intColor: Int) {
        this.stringColor = intColorToString(intColor)
        this.intColor = Color.parseColor(stringColor)
    }

    constructor(stringColor: String) {
        this.stringColor = stringColor
        this.intColor = Color.parseColor(stringColor)
    }

    companion object {
        @JvmStatic
        fun intColorToString(value: Int): String {
            /*return java.lang.String.format("#%06X", (0xFFFFFF and value))*/

            //val alpha = Color.alpha(value)
            val blue = Color.blue(value)
            val green = Color.green(value)
            val red = Color.red(value)

            //val alphaHex: String = To00Hex(alpha)
            val blueHex: String = to00Hex(blue)
            val greenHex: String = to00Hex(green)
            val redHex: String = to00Hex(red)


            val str = StringBuilder("#")
            //str.append(alphaHex)
            str.append(redHex)
            str.append(greenHex)
            str.append(blueHex)

            return str.toString() // hexBinary value: bbggrr
        }

        private fun to00Hex(value: Int): String {
            val hex = "00" + Integer.toHexString(value)
            return hex.substring(hex.length - 2, hex.length)
        }
    }
}