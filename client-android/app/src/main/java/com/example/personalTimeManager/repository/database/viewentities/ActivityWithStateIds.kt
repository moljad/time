package com.example.personalTimeManager.repository.database.viewentities

import androidx.room.Embedded
import androidx.room.Relation
import com.example.personalTimeManager.repository.database.entity.Activity
import com.example.personalTimeManager.repository.database.entity.ActivityState


data class ActivityWithStateIds(
    @Embedded val activity: Activity,
    @Relation(
        parentColumn = "id",
        entityColumn = "activityId",
        entity = ActivityState::class,
        projection = ["stateId"]
    ) val stateIdList: List<Int>
)