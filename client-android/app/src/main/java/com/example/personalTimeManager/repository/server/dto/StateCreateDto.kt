package com.example.personalTimeManager.repository.server.dto

import androidx.room.Ignore
import com.example.personalTimeManager.R
import com.example.personalTimeManager.repository.CustomColor
import com.example.personalTimeManager.repository.database.Status
import com.example.personalTimeManager.repository.database.entity.State
import com.example.personalTimeManager.repository.server.gson.Exclude
import com.google.gson.annotations.SerializedName
import java.time.LocalDateTime

data class StateCreateDto(
    @SerializedName("name")
    val name: String?,
    @SerializedName("description")
    val description: String?,
    @SerializedName("color")
    val color: CustomColor?
) {
    @Suppress("unused")
    private constructor() : this("", "", CustomColor(R.color.colorAccent))

    @Exclude
    @delegate:Exclude
    @delegate:Ignore
    val state by lazy {
        State(
            -1073741824,
            name ?: "",
            description ?: "",
            color ?: CustomColor(R.color.colorAccent),
            -1073741824,
            createdDate = LocalDateTime.now(),
            updatedDate = LocalDateTime.now(),
            status = Status.LOCAL_TO_CREATE
        )
    }
}