package com.example.personalTimeManager.repository.database.dao

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.example.personalTimeManager.repository.database.dao.base.BaseDao
import com.example.personalTimeManager.repository.database.entity.UserEntity
import com.example.personalTimeManager.repository.database.viewentities.UserEntityWithSchedules
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
abstract class UserDao : BaseDao<UserEntity>() {
    @Query("SELECT * FROM AspNetUsers")
    abstract fun getAll(): Single<List<UserEntity>>

    @Query("SELECT * FROM AspNetUsers WHERE id = :id")
    abstract fun getById(id: Int): Maybe<UserEntity>

    @Query("SELECT * FROM AspNetUsers WHERE id = :id")
    @Transaction
    abstract fun getUserByIdWithPrefetchedSchedules(id: Int): Maybe<UserEntityWithSchedules>

    @Query("SELECT * FROM AspNetUsers WHERE email = :email")
    abstract fun getByEmail(email: String): Maybe<UserEntity>

    @Query("SELECT MIN(id) FROM AspNetUsers")
    abstract fun getMinId(): Int
}