package com.example.personalTimeManager.repository.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import com.example.personalTimeManager.repository.database.Status
import com.example.personalTimeManager.repository.server.gson.Exclude
import com.google.gson.annotations.SerializedName

@Entity(
    primaryKeys = ["activityId", "stateId"],
    foreignKeys = [
        ForeignKey(
            entity = Activity::class,
            parentColumns = ["id"],
            childColumns = ["activityId"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = State::class,
            parentColumns = ["id"],
            childColumns = ["stateId"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class ActivityState(
    @ColumnInfo(name = "activityId", index = true)
    @SerializedName("activityId")
    val activityId: Int,
    @ColumnInfo(name = "stateId", index = true)
    @SerializedName("stateId")
    val stateId: Int,
    @Exclude
    val status: Status = Status.UNKNOWN
) {
    @Suppress("unused")
    private constructor() : this(-1073741824, -1073741824)
}


