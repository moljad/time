package com.example.personalTimeManager.di.component

import com.example.personalTimeManager.di.module.DatabaseModule
import com.example.personalTimeManager.repository.database.AppDatabase
import dagger.Component

@Component(modules = [DatabaseModule::class])
interface DatabaseComponent {
    val database: AppDatabase
}