package com.example.personalTimeManager.presentation.activities.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.personalTimeManager.R
import com.example.personalTimeManager.databinding.FragmentSettingsBinding
import com.example.personalTimeManager.di.component.ViewModelComponent
import com.example.personalTimeManager.presentation.base.BaseFragment
import com.google.android.material.floatingactionbutton.FloatingActionButton
import javax.inject.Inject

class SettingsFragment : BaseFragment() {

    lateinit var settingsViewModel: SettingsViewModel
        @Inject set

    override fun injectDependency(component: ViewModelComponent) {
        component.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentSettingsBinding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_settings, container, false
            )
        //binding.fragment = this
        binding.lifecycleOwner = this

        val view = binding.root

        val fab: FloatingActionButton = container!!.rootView.findViewById(R.id.fab)
        fab.hide()

        return view
    }
}