package com.example.personalTimeManager.presentation.activities.settings

import android.app.Application
import com.example.personalTimeManager.presentation.base.BaseViewModel
import com.example.personalTimeManager.repository.AppRepository

class SettingsViewModel(application: Application, private val repository: AppRepository) : BaseViewModel(application)