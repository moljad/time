package com.example.personalTimeManager.presentation.base

import android.app.ActionBar
import android.os.Bundle
import android.view.MenuItem
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.personalTimeManager.App
import com.example.personalTimeManager.di.component.ViewModelComponent
import com.example.personalTimeManager.util.hideKeyboardEx
import com.example.personalTimeManager.util.showSnack
import com.example.personalTimeManager.util.showToast

abstract class BaseFragment : Fragment() {
    private val appBar: ActionBar? = activity?.actionBar
    protected fun disableHomeAsUp() = appBar?.setDisplayHomeAsUpEnabled(false)

    protected fun initializeNavigationBar(
        title: String,
        showBackButton: Boolean, @DrawableRes resId: Int
    ) {
        appBar?.apply {
            this.setDisplayHomeAsUpEnabled(showBackButton)
            this.setHomeAsUpIndicator(resId)
            this.elevation = 4f
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        createDaggerDependencies()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> parentFragmentManager.popBackStackImmediate()
        }
        return super.onOptionsItemSelected(item)
    }

    protected abstract fun injectDependency(component: ViewModelComponent)

    private fun createDaggerDependencies() {
        if (activity is AppCompatActivity) {
            val appCompatActivity = activity as AppCompatActivity
            val app = appCompatActivity.application as App
            injectDependency(app.getViewModelComponent())
        }

    }

    protected fun showToast(text: String) = activity?.showToast(text)
    protected fun showSnack(text: String) = activity?.showSnack(text)
    protected fun hideKeyboard() = activity?.hideKeyboardEx()
}