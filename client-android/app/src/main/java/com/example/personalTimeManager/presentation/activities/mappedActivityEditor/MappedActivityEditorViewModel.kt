package com.example.personalTimeManager.presentation.activities.mappedActivityEditor


import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.example.personalTimeManager.presentation.base.BaseViewModel
import com.example.personalTimeManager.repository.AppRepository
import com.example.personalTimeManager.repository.database.entity.Activity
import com.example.personalTimeManager.repository.database.entity.MappedActivity
import com.example.personalTimeManager.repository.server.dto.MappedActivityCreateDto
import com.example.personalTimeManager.repository.server.dto.MappedActivityUpdateDto
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

class MappedActivityEditorViewModel(
    application: Application,
    private val repository: AppRepository
) : BaseViewModel(application) {
    val mappedActivity: MutableLiveData<MappedActivityModel> = MutableLiveData()
    var activities: List<Activity> = listOf()

    fun getMappedActivity(id: Int): Maybe<MappedActivity> {
        return repository.getMappedActivity(id)
    }

    fun updateMappedActivity(
        id: Int,
        mappedActivityUpdateDto: MappedActivityUpdateDto
    ): Single<MappedActivity> {
        return repository.updateMappedActivity(id, mappedActivityUpdateDto)
    }

    fun deleteMappedActivity(id: Int): Completable {
        return repository.deleteMappedActivity(id)
    }

    fun createMappedActivity(mappedActivityCreateDto: MappedActivityCreateDto): Single<MappedActivity> {
        return repository.createMappedActivity(mappedActivityCreateDto)
    }

    fun getActivities(): Single<List<Activity>> {
        return repository.getActivitiesByUserId()
    }

    data class MappedActivityModel(
        var id: Int,
        var begin: Int,
        var end: Int,
        var activityId: Int
    )

}
