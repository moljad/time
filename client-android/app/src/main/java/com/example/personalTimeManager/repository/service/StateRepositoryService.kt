package com.example.personalTimeManager.repository.service

import android.content.res.Resources
import android.util.Log
import com.example.personalTimeManager.repository.database.AppDatabase
import com.example.personalTimeManager.repository.database.Status
import com.example.personalTimeManager.repository.database.converter.ColorConverters
import com.example.personalTimeManager.repository.database.converter.DateConverters
import com.example.personalTimeManager.repository.database.converter.StatusConverters
import com.example.personalTimeManager.repository.database.entity.State
import com.example.personalTimeManager.repository.server.ServerCommunicator
import com.example.personalTimeManager.repository.server.dto.StateCreateDto
import com.example.personalTimeManager.repository.server.dto.StateUpdateDto
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.MaybeSource
import io.reactivex.Single
import io.reactivex.rxkotlin.zipWith
import retrofit2.HttpException
import java.io.IOException
import java.time.LocalDateTime
import java.util.*
import java.util.concurrent.TimeoutException
import javax.inject.Inject
import kotlin.collections.ArrayList

class StateRepositoryService @Inject constructor(
    private val userRepositoryService: UserRepositoryService,
    private val serverCommunicator: ServerCommunicator,
    private val mainDatabase: AppDatabase
) {

    /**gets from local state.
     * If is not found, then tries to synchronize it with current user and get it again**/
    fun guessUserIdByStateId(id: Int): Maybe<Int> {
        return getLocalState(id).map { it.userId }
            .switchIfEmpty(MaybeSource<Int> { maybeObserver ->
                userRepositoryService.guessCurrentUser()
                    .flatMapMaybe { guessedUser ->
                        val maybeSource = getStateByUserIdAndId(guessedUser.id!!, id)
                            .map { state -> maybeObserver.onSuccess(state.userId) }
                        //maybeSource.onErrorReturn { maybeObserver.onError(it) }
                        //maybeSource.isEmpty.filter { it }.doOnComplete {  }
                        //maybeSource.doOnComplete {  }
                        maybeSource
                    }
            }).onErrorResumeNext { _: Throwable -> return@onErrorResumeNext Maybe.empty<Int>() }
    }

    fun getStatesByUserId(userId: Int): Single<List<State>> {
        val refreshedStates = synchronizeStatesAndGetThemByUserId(userId)
        val localStates = getLocalStatesByUserId(userId)

        return refreshedStates.zipWith(localStates)
            .flatMap { (refreshedStates, localStates) ->
                if (!refreshedStates.isPresent) {
                    Log.i("AppRepository", "returning local states instead")
                    val markedLocalStates = markAsLocalIfNotDeferred(localStates)
                    return@flatMap createOrReplaceLocalStates(markedLocalStates).andThen(
                        Single.just(markedLocalStates)
                    )
                }
                return@flatMap Single.just(refreshedStates.get())
            }
    }

    private fun markAsLocalIfNotDeferred(states: List<State>): List<State> {
        return states.map {
            markAsLocalIfNotDeferred(it)
        }
    }

    private fun markAsLocalIfNotDeferred(state: State): State {
        if (state.status != Status.LOCAL_TO_CREATE
            && state.status != Status.LOCAL_TO_UPDATE
            && state.status != Status.LOCAL_TO_DELETE
        ) {
            return state.copy(status = Status.LOCAL)
        }
        return state
    }


    fun getLocalStatesByUserId(userId: Int): Single<List<State>> {
        return mainDatabase.stateDao().getStatesByUserId(userId)
    }

    fun synchronizeStatesAndGetThemByUserId(
        userId: Int
    ): Single<Optional<List<State>>> {
        return synchronizeStates(userId).andThen(
            getLocalStatesByUserId(userId)
        ).map { Optional.of(it) }.onErrorResumeNext { t ->
            if (t is IOException || t is TimeoutException) {
                return@onErrorResumeNext Single.just(Optional.empty<List<State>>())
            }
            Single.error<Optional<List<State>>>(t)
        }
    }

    private fun synchronizeStates(userId: Int): Completable {
        return getLocalStatesByUserId(userId).flatMapCompletable { localStates ->
            val synchronizeToCreate = synchronizeDeferredToCreateStates(localStates)
            val synchronizeToUpdate = synchronizeDeferredToUpdateStates(localStates)
            val synchronizeToDelete = synchronizeDeferredToDeleteStates(localStates)

            Completable.mergeArrayDelayError(
                synchronizeToCreate, synchronizeToUpdate, synchronizeToDelete
            ).andThen(getRemotesAndRefreshStates(userId, localStates))
        }
    }

    private fun synchronizeDeferredToCreateStates(
        localStates: List<State>
    ): Completable {
        val taskList = ArrayList<Completable>()
        localStates.forEach { localState ->
            if (localState.status == Status.LOCAL_TO_CREATE) {
                val createRemoteState =
                    synchronizeDeferredToCreateState(localState)
                taskList.add(createRemoteState)
            }
        }
        val params = taskList.toTypedArray()
        return Completable.mergeArrayDelayError(*params)
    }

    private fun synchronizeDeferredToCreateState(localState: State): Completable {
        return createRemoteState(localState.userId, localState.stateCreateDto)
            .flatMapCompletable { newState ->
                val (id, name, description, color, userId, createdDate, updatedDate, status) = newState
                val stringColor = ColorConverters.colorToString(color)
                val stringCreateDate = DateConverters.dateToString(createdDate)
                val stringUpdatedDate = DateConverters.dateToString(updatedDate)
                val stringStatus = StatusConverters.statusToString(status)

                mainDatabase.stateDao().replace(
                    localState.id, id, name, description, stringColor, userId,
                    stringCreateDate, stringUpdatedDate, stringStatus
                )
            }.onErrorResumeNext { t ->
                if (t is HttpException && t.code() == 500) {
                    return@onErrorResumeNext deleteLocalState(localState)
                }
                if (t is IOException || t is TimeoutException) {
                    return@onErrorResumeNext Completable.complete()
                }
                return@onErrorResumeNext Completable.error(t)
            }
    }

    private fun synchronizeDeferredToUpdateStates(localStates: List<State>): Completable {
        val taskList = ArrayList<Completable>()
        localStates.forEach { localState ->
            if (localState.status == Status.LOCAL_TO_UPDATE) {
                val updateRemoteState =
                    synchronizeDeferredToUpdateState(localState)
                taskList.add(updateRemoteState)
            }
        }
        val params = taskList.toTypedArray()
        return Completable.mergeArrayDelayError(*params)
    }

    private fun synchronizeDeferredToUpdateState(localState: State): Completable {
        return updateRemoteState(
            localState.userId,
            localState.id,
            localState.stateUpdateDto
        ).flatMapCompletable { Completable.complete() }
            .onErrorResumeNext { t ->
                if (t is HttpException && t.code() == 500) {
                    return@onErrorResumeNext getStateByUserIdAndId(
                        localState.userId,
                        localState.id
                    ).flatMapCompletable { Completable.complete() }
                }
                if (t is IOException || t is TimeoutException) {
                    return@onErrorResumeNext Completable.complete()
                }
                return@onErrorResumeNext Completable.error(t)
            }
    }

    private fun synchronizeDeferredToDeleteStates(localStates: List<State>): Completable {
        val taskList = ArrayList<Completable>()
        localStates.forEach { localState ->
            if (localState.status == Status.LOCAL_TO_DELETE) {
                val deleteSynchronizationTask =
                    synchronizeDeferredToDeleteState(localState)
                taskList.add(deleteSynchronizationTask)
            }
        }
        val params = taskList.toTypedArray()
        return Completable.mergeArrayDelayError(*params)
    }

    private fun synchronizeDeferredToDeleteState(localState: State): Completable {
        return deleteRemoteState(localState.userId, localState.id)
            .andThen(deleteLocalState(localState))
            .onErrorResumeNext { t ->
                if (t is HttpException && t.code() == 500) {
                    return@onErrorResumeNext getStateByUserIdAndId(
                        localState.userId,
                        localState.id
                    ).flatMapCompletable { Completable.complete() }
                }
                if (t is IOException || t is TimeoutException) {
                    return@onErrorResumeNext Completable.complete()
                }
                return@onErrorResumeNext Completable.error(t)
            }
    }

    private fun getRemotesAndRefreshStates(
        userId: Int,
        localStates: List<State>
    ): Completable {
        return getRemoteStates(userId).flatMapCompletable { listOptional ->
            if (listOptional.isPresent) {
                val remoteStates = listOptional.get()
                return@flatMapCompletable removeRedundantStates(
                    localStates,
                    remoteStates
                ).andThen(
                    createOrReplaceLocalStates(remoteStates)
                )
            }
            return@flatMapCompletable Completable.error(IOException("Could not receive remote entities"))
        }
    }

    fun getRemoteStates(userId: Int): Single<Optional<List<State>>> {
        return serverCommunicator.getStates(userId)
            .map { Optional.of(it.map { newState -> newState.copy(status = Status.SYNCHRONIZED) }) }
            .onErrorResumeNext { t ->
                if (t is IOException || t is TimeoutException) {
                    Log.e("AppRepository", t.message!!)
                    return@onErrorResumeNext Single.just(Optional.empty())
                }
                return@onErrorResumeNext Single.just(Optional.empty())
            }
    }

    private fun removeRedundantStates(
        localStates: List<State>,
        actualStates: List<State>
    ): Completable {
        val newIds = actualStates.map { it.id }
        val localIds = localStates.filter {
            it.status != Status.LOCAL_TO_CREATE
                    && it.status != Status.LOCAL_TO_UPDATE
                    && it.status != Status.LOCAL_TO_DELETE
        }.map { it.id }
        val redundantIds = localIds.subtract(newIds)

        val redundant = localStates.filter { it.id in redundantIds }
        if (redundant.isNotEmpty()) {
            return deleteStatesLocally(redundant)
        }
        return Completable.complete()
    }

    private fun deleteStatesLocally(list: List<State>): Completable {
        return mainDatabase.stateDao().deleteList(list)
    }

    private fun createOrReplaceLocalStates(actualStates: List<State>): Completable {
        return Completable.fromCallable {
            mainDatabase.stateDao().upsertList(actualStates)
        }
    }

    fun getStateByUserIdAndId(userId: Int, id: Int): Maybe<State> {
        val communicatorState =
            getRemoteState(userId, id).map { Optional.of(it) }.defaultIfEmpty(Optional.empty())
        val localState =
            getLocalState(id).map { Optional.of(it) }.defaultIfEmpty(Optional.empty())

        return communicatorState.zipWith(localState)
            .flatMap { (communicatorState, localState) ->
                if (!communicatorState.isPresent) {    //server doesn't have it, but local does
                    if (localState.isPresent) {  //redundant local data
                        deleteRedundantStateAsync(localState.get())
                    }
                    return@flatMap Maybe.empty<State>()
                }
                if (communicatorState.isPresent && communicatorState.get().isPresent) { //ok. just refresh local data with this new value
                    val actualState = communicatorState.get().get()
                    return@flatMap createOrReplaceLocalStateAndGetIt(actualState).toMaybe()
                }
                if (communicatorState.isPresent && !communicatorState.get().isPresent && localState.isPresent) {
                    Log.i("AppRepository", "returning local state instead")
                    val markedState = markAsLocalIfNotDeferred(localState.get())
                    if (markedState == localState.get()) {
                        return@flatMap Maybe.just(localState.get())
                    }
                    return@flatMap createOrReplaceLocalState(markedState).andThen(
                        Maybe.just(markedState)
                    )
                }
                return@flatMap Maybe.error<State>(Resources.NotFoundException("state was never fetched and connection problem occurred"))
            }
    }

    private fun createOrReplaceLocalStateAndGetIt(state: State): Single<State> {
        return createOrReplaceLocalState(state).andThen(getLocalState(state.id))
            .toSingle()
    }

    private fun createOrReplaceLocalState(actualState: State): Completable {
        return Completable.fromCallable {
            mainDatabase.stateDao().upsert(actualState)
        }
    }

    private fun deleteRedundantStateAsync(
        localStateValue: State
    ) {
        if (localStateValue.status != Status.LOCAL_TO_CREATE
            && localStateValue.status != Status.LOCAL_TO_UPDATE
            && localStateValue.status != Status.LOCAL_TO_DELETE
        ) {
            deleteStateLocally(localStateValue).subscribe()   //would be removed in async way
        }
    }

    private fun deleteStateLocally(state: State): Completable {
        return mainDatabase.stateDao().delete(state)
    }

    /**
     * optional exist over optional empty = connection problem
     * optional exist over optional exist = exists and connection was ok
     * optional empty over optional empty = ??? impossible. Server throws exception on 500 and i dont have a chance to wrap it. There is no Maybe.just(Optional.empty).map(Optional.empty(it))
     * optional empty over optional exist = server doesn't have it and connection was ok
     */
    private fun getRemoteState(userId: Int, id: Int): Maybe<Optional<State>> {
        return serverCommunicator.getState(userId, id)
            .map { it.copy(status = Status.SYNCHRONIZED) }
            .map { Optional.of(it) }.toMaybe()
            .onErrorResumeNext { t: Throwable ->
                if (t is HttpException && t.code() == 500) {
                    return@onErrorResumeNext Maybe.empty<Optional<State>>()
                }
                if (t is IOException || t is TimeoutException) {
                    return@onErrorResumeNext Maybe.just(Optional.empty<State>())
                }
                Maybe.error(t)
            }
    }

    fun createStateAndGetItById(userId: Int, createDto: StateCreateDto): Single<State> {
        val newCommunicatorState = createRemoteState(userId, createDto)
        return newCommunicatorState.flatMap {
            createOrReplaceLocalStateAndGetIt(it)
        }.onErrorResumeNext { t ->
            if (t is IOException || t is TimeoutException) {
                Log.e("AppRepository", t.message!!)
                return@onErrorResumeNext createLocalStateByDtoAndGetIt(createDto)
            }
            Single.error(t)
        }
    }

    private fun createRemoteState(userId: Int, createDto: StateCreateDto): Single<State> {
        return serverCommunicator.createState(userId, createDto)
            .map { it.copy(status = Status.SYNCHRONIZED) }
    }

    fun getLocalState(id: Int): Maybe<State> {
        return mainDatabase.stateDao().getById(id)
    }

    fun updateStateAndGetItById(id: Int, updateDto: StateUpdateDto): Single<State> {
        val localStateById = getLocalState(id).switchIfEmpty(
            userRepositoryService.guessCurrentUser().flatMapMaybe {
                getStateByUserIdAndId(it.id!!, id)
            }.switchIfEmpty(Single.error<State>(Resources.NotFoundException("can not find state to update")))
        )

        return localStateById.flatMap {
            updateRemoteState(it.userId, it.id, updateDto)
        }.flatMap {
            updateAndGetLocalState(it)
        }.onErrorResumeNext { t ->
            if (t is IOException || t is TimeoutException) {
                Log.e("AppRepository", t.message!!)
                return@onErrorResumeNext updateLocalStateByDtoAndGetIt(id, updateDto)
            }
            Single.error(t)
        }
    }

    private fun updateRemoteState(
        userId: Int,
        id: Int,
        updateDto: StateUpdateDto
    ): Single<State> {
        return serverCommunicator.updateState(userId, id, updateDto)
            .map { it.copy(status = Status.SYNCHRONIZED) }
    }

    private fun updateAndGetLocalState(state: State): Single<State> {
        return updateLocalState(state).andThen(getLocalState(state.id).toSingle())
    }

    private fun updateLocalState(state: State): Completable {
        return mainDatabase.stateDao().update(state)
    }

    fun deleteState(id: Int): Completable {
        val localStateById = getLocalState(id).switchIfEmpty(
            userRepositoryService.guessCurrentUser().flatMapMaybe {
                getStateByUserIdAndId(it.id!!, id)
            }
        )
        return localStateById.isEmpty.flatMapCompletable { isLocalStateEmpty ->
            if (isLocalStateEmpty) { //just send a request with current user
                return@flatMapCompletable userRepositoryService.guessCurrentUser()
                    .flatMapCompletable { user ->
                        deleteRemoteState(user.id!!, id)
                    }
            }
            return@flatMapCompletable localStateById.flatMapCompletable(
                deleteExistingState()
            )
        }
    }

    private fun deleteExistingState(): (State) -> Completable {
        return completable@{ localState ->
            if (localState.status == Status.LOCAL_TO_CREATE) {
                return@completable deleteLocalState(localState)
            }
            return@completable deleteRemoteState(
                localState.userId,
                localState.id
            ).andThen(deleteLocalState(localState)).onErrorResumeNext { t ->
                if (t is IOException || t is TimeoutException) {
                    return@onErrorResumeNext updateLocalState(localState.copy(status = Status.LOCAL_TO_DELETE))
                }
                if (t is HttpException && t.code() == 500) {
                    return@onErrorResumeNext deleteLocalState(localState)
                }
                return@onErrorResumeNext Completable.error(t)
            }
        }
    }

    private fun deleteRemoteState(userId: Int, id: Int): Completable {
        return serverCommunicator.deleteState(userId, id)
    }

    private fun deleteLocalState(state: State): Completable {
        return mainDatabase.stateDao().delete(state)
    }

    private fun createLocalStateByDtoAndGetIt(createDto: StateCreateDto): Single<State> {
        val stateFromCreateDto = createDto.state
        val minId: Int = if (mainDatabase.stateDao().getMinId() > 0) {
            -mainDatabase.stateDao().getMinId()
        } else {
            mainDatabase.stateDao().getMinId() - 1
        }
        return userRepositoryService.getCurrentUser().flatMap {
            val state = stateFromCreateDto.copy(
                id = minId,
                userId = it.id!!,
                createdDate = LocalDateTime.now(),
                updatedDate = LocalDateTime.now()
            )
            createOrReplaceLocalStateAndGetIt(state)
        }
    }

    private fun updateLocalStateByDtoAndGetIt(
        id: Int,
        updateDto: StateUpdateDto
    ): Single<State> {
        val (name, description, color) = updateDto
        return getLocalState(id).flatMapSingle { stateToUpdate ->
            val unsavedUpdatedState = stateToUpdate.copy(
                name = name ?: stateToUpdate.name,
                description = description ?: stateToUpdate.description,
                color = color ?: stateToUpdate.color,
                updatedDate = LocalDateTime.now(),
                status = if (stateToUpdate.status == Status.LOCAL_TO_CREATE) Status.LOCAL_TO_CREATE else Status.LOCAL_TO_UPDATE
            )
            updateAndGetLocalState(unsavedUpdatedState)
        }
    }
}