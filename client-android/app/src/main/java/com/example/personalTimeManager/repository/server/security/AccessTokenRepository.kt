package com.example.personalTimeManager.repository.server.security


import android.util.Log
import com.example.personalTimeManager.repository.database.entity.UserEntity
import com.example.personalTimeManager.repository.user.UserManager
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import io.reactivex.schedulers.Schedulers
import okhttp3.FormBody
import okhttp3.OkHttpClient
import okhttp3.Request

class AccessTokenRepository(
    private val okHttpClient: OkHttpClient?,
    private val userManager: UserManager
) {
    var accessToken: String? = null
    var user: UserEntity? = null

    companion object {
        const val TOKEN_URL: String = "https://10.0.2.2:5001/connect/token"

        fun createTokenRequest(email: String, password: String): Request {
            return Request.Builder()
                .url(TOKEN_URL)
                .post(
                    FormBody.Builder()
                        .add("grant_type", "password")
                        .add("username", email)
                        .add("password", password)
                        .add("scope", "ServerAPI")
                        .add("client_secret", "secret")
                        .add("client_id", "client")
                        .build()
                )
                .addHeader("Authorization", "Basic Y2xpZW50OnNlY3JldA==")
                .addHeader("Accept-Encoding", "gzip, deflate")
                .build()
        }
    }

    fun refreshAccessToken(): String? {
        val currentUser = userManager.getCurrentUser()
            .subscribeOn(Schedulers.io())
            .blockingGet()

        val email = currentUser?.email
        val password = currentUser?.password
        if (email == null || password == null) {
            Log.e("AccessTokenRepository", "not enough info to acquire token. UserInfo: $user")
            return null
        }

        val request: Request =
            createTokenRequest(
                email,
                password
            )
        val response = okHttpClient?.newCall(request)
        val executed = response?.execute()
        val body = executed?.body?.string()

        accessToken = Gson().fromJson(body, AccessToken::class.java).accessToken

        return accessToken
    }


    class AccessToken {
        @SerializedName("access_token")
        var accessToken: String? = null
    }
}