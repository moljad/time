package com.example.personalTimeManager.util

import com.alamkanak.weekview.DateTimeInterpreter
import java.text.SimpleDateFormat
import java.util.*

class CustomDateTimeInterpreter : DateTimeInterpreter {

    private var sdfDate = SimpleDateFormat("EEE dd", Locale.getDefault())
    private val sdfTime = SimpleDateFormat("HH:mm", Locale.getDefault())

    private val calendar = firstDayOfYear()

    override fun interpretDate(date: Calendar): String {
        return sdfDate.format(date.time).toUpperCase(Locale.getDefault())
    }

    override fun interpretTime(hour: Int): String {
        val time = calendar.withTime(hour, minutes = 0)
        return sdfTime.format(time.time)
    }

    private fun Calendar.withTime(hour: Int, minutes: Int): Calendar {
        return copy().apply {
            set(Calendar.HOUR_OF_DAY, hour)
            set(Calendar.MINUTE, minutes)
            set(Calendar.SECOND, 0)
            set(Calendar.MILLISECOND, 0)
        }
    }

    private fun Calendar.copy(): Calendar = clone() as Calendar

    private fun firstDayOfYear(): Calendar {
        return today().apply {
            set(Calendar.MONTH, Calendar.JANUARY)
            set(Calendar.DAY_OF_MONTH, 1)
        }
    }

    private fun today() = now().atStartOfDay

    private fun now() = Calendar.getInstance()

    private val Calendar.atStartOfDay: Calendar
        get() = withTimeAtStartOfPeriod(0)

    private fun Calendar.withTimeAtStartOfPeriod(hour: Int): Calendar {
        return copy().apply {
            set(Calendar.HOUR_OF_DAY, hour)
            set(Calendar.MINUTE, 0)
            set(Calendar.SECOND, 0)
            set(Calendar.MILLISECOND, 0)
        }
    }
}