package com.example.personalTimeManager.presentation.activities.activityEditor

import android.view.View
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.personalTimeManager.R

class ActivityEditorHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val textView: TextView = itemView.findViewById(R.id.activityStateName)
    private val checkView: CheckBox = itemView.findViewById(R.id.activityStateCheckbox)

    fun setName(string: String) {
        textView.text = string
    }

    fun setChecked(value: Boolean) {
        checkView.isChecked = value
    }

    fun isChecked(): Boolean {

        return checkView.isChecked
    }
}