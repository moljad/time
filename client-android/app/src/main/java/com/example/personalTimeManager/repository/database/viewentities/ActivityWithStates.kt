package com.example.personalTimeManager.repository.database.viewentities

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import com.example.personalTimeManager.repository.database.entity.Activity
import com.example.personalTimeManager.repository.database.entity.ActivityState
import com.example.personalTimeManager.repository.database.entity.State


data class ActivityWithStates(
    @Embedded val activity: Activity,
    @Relation(
        parentColumn = "id",
        entityColumn = "id",
        associateBy = Junction(
            value = ActivityState::class,
            parentColumn = "activityId",
            entityColumn = "stateId"
        )
    ) val states: List<State>
)

/*
data class ActivityWithStates(
    @Embedded val activity: Activity,
    @Relation(
        parentColumn = "id",
        entityColumn = "stateId",
        entity = Activity::class,
        associateBy = Junction(
            value = ActivityState::class,
            parentColumn = "activityId",
            entityColumn = "stateId"
        )
    ) val states: List<State>
)*/
