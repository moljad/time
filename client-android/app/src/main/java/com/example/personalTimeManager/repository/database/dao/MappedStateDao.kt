package com.example.personalTimeManager.repository.database.dao

import androidx.room.Dao
import androidx.room.Query
import com.example.personalTimeManager.repository.database.dao.base.BaseDao
import com.example.personalTimeManager.repository.database.entity.MappedState
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
abstract class MappedStateDao : BaseDao<MappedState>() {
    @Query("SELECT * FROM MappedStates")
    abstract fun getAll(): Single<List<MappedState>>

    @Query(
        """SELECT  MappedStates.id, MappedStates.createdDate, MappedStates.updatedDate,
         MappedStates.scheduleId, MappedStates.stateId, 
         MappedStates.`begin`, MappedStates.`end`, MappedStates.status
        FROM MappedStates
        LEFT JOIN States as S on MappedStates.stateId == S.id
        WHERE S.userId == :userId"""
    )
    abstract fun getByUserId(userId: Int): Single<List<MappedState>>

    @Query("SELECT * FROM MappedStates WHERE id = :id")
    abstract fun getById(id: Int): Maybe<MappedState>

    @Query("SELECT MIN(id) FROM MappedStates")
    abstract fun getMinId(): Int

    @Query(
        """UPDATE MappedStates SET id = :id, `begin` = :spanBegin, `end` = :spanEnd, 
            stateId = :stateId, scheduleId = :scheduleId, 
            createdDate = :createdDate, updatedDate = :updatedDate, status = :status 
            WHERE id = :oldMappedStateId"""
    )
    abstract fun replace(
        oldMappedStateId: Int,
        id: Int, spanBegin: Int, spanEnd: Int, stateId: Int, scheduleId: Int,
        createdDate: String, updatedDate: String, status: String
    ): Completable
}