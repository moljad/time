package com.example.personalTimeManager.di.module

import com.example.personalTimeManager.repository.database.AppDatabase
import com.example.personalTimeManager.repository.server.okhttpclient.DefaultOkHttpClientBuilder
import com.example.personalTimeManager.repository.server.security.AccessTokenAuthenticator
import com.example.personalTimeManager.repository.server.security.AccessTokenRepository
import com.example.personalTimeManager.repository.user.UserManager
import dagger.Module
import dagger.Provides

@Module
class UserModule(private val appDatabase: AppDatabase) {

    @Provides
    internal fun provideAuthentificator(accessTokenRepository: AccessTokenRepository): AccessTokenAuthenticator {
        return AccessTokenAuthenticator(
            accessTokenRepository
        )
    }

    @Provides
    internal fun provideAccessTokenRepository(
        userRep: UserManager
    ): AccessTokenRepository {
        return AccessTokenRepository(
            DefaultOkHttpClientBuilder.okHttpClientBuilder().build(),
            userRep

        )
    }

    @Provides
    internal fun provideUserRepository(): UserManager {
        return UserManager(appDatabase)
    }

}