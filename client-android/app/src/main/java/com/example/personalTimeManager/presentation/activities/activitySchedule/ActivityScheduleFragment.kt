package com.example.personalTimeManager.presentation.activities.activitySchedule

import android.content.Context
import android.graphics.RectF
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.LinearLayout
import android.widget.PopupWindow
import androidx.navigation.fragment.findNavController
import com.alamkanak.weekview.WeekView
import com.alamkanak.weekview.WeekViewDisplayable
import com.example.personalTimeManager.R
import com.example.personalTimeManager.di.component.ViewModelComponent
import com.example.personalTimeManager.presentation.base.BaseFragment
import com.example.personalTimeManager.repository.CustomColor
import com.example.personalTimeManager.repository.database.entity.Activity
import com.example.personalTimeManager.repository.database.entity.MappedActivity
import com.example.personalTimeManager.repository.server.dto.MappedActivityCreateDto
import com.example.personalTimeManager.util.CustomDateTimeInterpreter
import com.example.personalTimeManager.util.getStartOfTheWeek
import com.google.android.material.floatingactionbutton.FloatingActionButton
import io.reactivex.Completable
import io.reactivex.Single.mergeDelayError
import io.reactivex.functions.BiFunction
import java.util.*
import javax.inject.Inject


class ActivityScheduleFragment : BaseFragment() {
    lateinit var viewModel: ActivityScheduleViewModel
        @Inject set

    var generatingPopupWindow: PopupWindow? = null

    override fun injectDependency(component: ViewModelComponent) {
        component.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_activity_schedule, container, false)
        val fab: FloatingActionButton = container!!.rootView.findViewById(R.id.fab)
        fab.show()

        val schedule: WeekView<ActivityScheduleEvent> = view.findViewById(R.id.weekView)
        schedule.dateTimeInterpreter = CustomDateTimeInterpreter()

        viewModel.getActivities()
            .zipWith(
                viewModel.getMappedActivities(),
                BiFunction { a: List<Activity>, b: List<MappedActivity> -> a to b })
            .subscribe { (a, b) ->
                val events = convertToEvents(a, b)
                schedule.submit(events)
            }

        schedule.setOnEventClickListener(this::onWeekEventClick)

        fab.setOnClickListener(this::onFabClick)

        return view
    }

    private fun onWeekEventClick(event: ActivityScheduleEvent, rect: RectF) {
        Log.e("Navigation", """Open Mapped Activity Editor with id ${event.mappedActivityId}""")

        val action =
            ActivityScheduleFragmentDirections.actionNavScheduleToNavMappedActivityEditorFragment(
                event.mappedActivityId
            )

        findNavController().navigate(action)
    }

    private fun onFabClick(v: View) {
        Log.e("Navigation", "Open Mapped Activity Editor with id 0 (NEW)")

        val action =
            ActivityScheduleFragmentDirections.actionNavScheduleToNavMappedActivityEditorFragment(
                0
            )

        findNavController().navigate(action)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        menu.clear()
        val generate = menu.add("Generate")
        generate.setShowAsAction(1)
        generate.setOnMenuItemClickListener {
            viewModel.getMappedActivities().flatMapCompletable {
                showGeneratingScreen()
                val a = it.map({ viewModel.deleteMappedActivity(it.id) }).toTypedArray()
                Completable.mergeArrayDelayError(*a)
            }.andThen(
                viewModel.getGeneratedMappedActivities().flatMapCompletable {
                    val b = it.map {
                        viewModel.createMappedActivity(
                            MappedActivityCreateDto(
                                it.span,
                                it.activityId
                            )
                        )
                    }
                    val mergeDelayError = mergeDelayError(b)
                    mergeDelayError.ignoreElements()
                }
            ).subscribe({
                val schedule: WeekView<ActivityScheduleEvent> = view!!.findViewById(R.id.weekView)

                viewModel.getActivities()
                    .zipWith(
                        viewModel.getMappedActivities(),
                        BiFunction { a: List<Activity>, b: List<MappedActivity> -> a to b })
                    .subscribe { (a, b) ->
                        val events = convertToEvents(a, b)
                        schedule.submit(events)
                        hideGeneratingScreen()
                    }
            }, { Log.e(this.javaClass.simpleName, it.message, it) })

            false
        }
    }

    private fun convertToEvents(
        activities: List<Activity>,
        mappedActivity: List<MappedActivity>
    ): List<WeekViewDisplayable<ActivityScheduleEvent>> {
        val names = activities.map { it.id to it.name }.toMap()
        val colors = activities.map { it.id to it.color }.toMap()
        val startOfWeek = Calendar.getInstance().getStartOfTheWeek()

        return mappedActivity.mapIndexed { id: Int, a: MappedActivity ->
            val name = if (names[a.activityId] != null) names[a.activityId]!! else "EMPTY NAME"
            val color = if (colors[a.activityId] != null) colors[a.activityId]!! else CustomColor(0)

            ActivityScheduleEvent(
                id.toLong(),
                a.id,
                name,
                startOfWeek,
                a.span,
                color.intColor
            )
        }
    }

    private fun showGeneratingScreen() {
        val inflater =
            context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        val popupView = inflater.inflate(R.layout.schedule_generation_popup, null)

        generatingPopupWindow =
            PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,
                true
            )
        generatingPopupWindow!!.showAtLocation(this.view, Gravity.CENTER, 0, 0)
    }

    private fun hideGeneratingScreen() {
        generatingPopupWindow?.dismiss()
        generatingPopupWindow = null
    }
}