package com.example.personalTimeManager.repository.database.dao

import androidx.room.Dao
import androidx.room.Query
import com.example.personalTimeManager.repository.database.dao.base.BaseDao
import com.example.personalTimeManager.repository.database.entity.MappedActivity
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
abstract class MappedActivityDao : BaseDao<MappedActivity>() {
    @Query("SELECT * FROM MappedActivities")
    abstract fun getAll(): Single<List<MappedActivity>>

    @Query("SELECT * FROM MappedActivities WHERE id = :id")
    abstract fun getById(id: Int): Maybe<MappedActivity>

    @Query(
        """SELECT  MappedActivities.id, MappedActivities.createdDate, MappedActivities.updatedDate,
         MappedActivities.scheduleId, MappedActivities.activityId, 
         MappedActivities.`begin`, MappedActivities.`end`, MappedActivities.status
        FROM MappedActivities
        LEFT JOIN Activities as A on MappedActivities.activityId == A.id
        WHERE A.userId == :userId"""
    )
    abstract fun getByUserId(userId: Int): Single<List<MappedActivity>>

    @Query("SELECT MIN(id) FROM MappedActivities")
    abstract fun getMinId(): Int

    @Query(
        """UPDATE MappedActivities SET id = :id, `begin` = :spanBegin, `end` = :spanEnd, 
            activityId = :activityId, scheduleId = :scheduleId, 
            createdDate = :createdDate, updatedDate = :updatedDate, status = :status 
            WHERE id = :oldMappedActivityId"""
    )
    abstract fun replace(
        oldMappedActivityId: Int,
        id: Int, spanBegin: Int, spanEnd: Int, activityId: Int, scheduleId: Int,
        createdDate: String, updatedDate: String, status: String
    ): Completable
}