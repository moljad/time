package com.example.personalTimeManager.presentation.activities.stateSchedule

import com.alamkanak.weekview.WeekViewDisplayable
import com.alamkanak.weekview.WeekViewEvent
import com.example.personalTimeManager.repository.Span
import java.util.*

data class StateScheduleEvent(
    val id: Long,
    val mappedStateId: Int,
    val title: String,
    val startOfWeek: Calendar,
    val span: Span,
    val color: Int
) : WeekViewDisplayable<StateScheduleEvent> {

    override fun toWeekViewEvent(): WeekViewEvent<StateScheduleEvent> {
        val style = WeekViewEvent.Style.Builder()
            .setBackgroundColor(color)
            .setTextStrikeThrough(false)
            .build()

        val start = startOfWeek.clone() as Calendar
        val end = startOfWeek.clone() as Calendar

        start.add(Calendar.MINUTE, span.begin * 10)
        end.add(Calendar.MINUTE, span.end * 10)

        return WeekViewEvent.Builder(this)
            .setId(id)
            .setTitle(title)
            .setStartTime(start)
            .setEndTime(end)
            .setLocation("")
            .setAllDay(false)
            .setStyle(style)
            .build()
    }
}