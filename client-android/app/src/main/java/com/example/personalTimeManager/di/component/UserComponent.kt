package com.example.personalTimeManager.di.component

import com.example.personalTimeManager.di.module.UserModule
import com.example.personalTimeManager.repository.server.security.AccessTokenAuthenticator
import com.example.personalTimeManager.repository.user.UserManager
import dagger.Component

@Component(modules = [UserModule::class], dependencies = [DatabaseComponent::class])
interface UserComponent {
    val manager: UserManager
    val authenticator: AccessTokenAuthenticator
}