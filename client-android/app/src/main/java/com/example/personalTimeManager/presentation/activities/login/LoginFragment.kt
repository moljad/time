package com.example.personalTimeManager.presentation.activities.login

import android.content.Context
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.example.personalTimeManager.R
import com.example.personalTimeManager.databinding.FragmentLoginBinding
import com.example.personalTimeManager.di.component.ViewModelComponent
import com.example.personalTimeManager.presentation.base.BaseFragment
import com.example.personalTimeManager.repository.server.okhttpclient.DefaultOkHttpClientBuilder
import com.example.personalTimeManager.repository.server.security.AccessTokenRepository
import com.example.personalTimeManager.util.replaceFragment
import com.example.personalTimeManager.util.showToast
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import java.io.IOException
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class LoginFragment : BaseFragment() {

    var listener: OnFragmentInteractionListener? = null

    var viewModel: LoginRegistrationViewModel? = null
        @Inject set

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentLoginBinding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_login, container, false
            )
        binding.viewModel = this.viewModel
        return binding.root
    }

    override fun injectDependency(component: ViewModelComponent) {
        component.inject(this)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    fun onLoginButtonPressed() {
        val email = viewModel?.loginModel?.value?.email
        val password = viewModel?.loginModel?.value?.password

        val okHttpClient =
            DefaultOkHttpClientBuilder.okHttpClientBuilder()
                .connectTimeout(5, TimeUnit.SECONDS)
                .readTimeout(5, TimeUnit.SECONDS)
                .writeTimeout(5, TimeUnit.SECONDS)
                .build()
        val request = AccessTokenRepository.createTokenRequest(email!!, password!!)
        val newCall = okHttpClient.newCall(request)
        newCall.enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                Looper.prepare()
                activity?.showToast("Connectivity problem")
                Looper.loop()
            }

            override fun onResponse(call: Call, response: Response) {
                if (response.isSuccessful) {
                    viewModel?.let {
                        it.loginUser(it.loginModel.value!!)
                    }
                } else {
                    Looper.prepare()
                    activity?.showToast("Check your credentials and try again")
                    Looper.loop()
                }
            }

        })
    }

    /**Tries to replace current container with provided Registration fragment and adds it to backstack of fragmentManager.
     * If function is not provided with Registration Fragment, then tries to create it by itself
     * Tries to get fragmentManager of current activity by fragmentActivity method
     * If current activity is not fragmentActivity, then tries to obtain fragmentManager by basic fragment method
     * If fragmentManager can not be obtained, then just returns
     * **/
    fun onRegistrationRedirectTextPressed(
        registrationFragment: Fragment = RegistrationFragment.newInstance(),
        viewGroup: ViewGroup? = view?.parent as? ViewGroup
    ) {
        if (viewGroup == null) {
            Log.e("LoginFragment", "Can not obtain current container")
            return
        }
        val transaction = if (activity is FragmentActivity) {
            activity?.supportFragmentManager?.replaceFragment(
                viewGroup.id,
                registrationFragment,
                addToBackStack = true,
                needAnimate = true
            )
        } else {
            val replaceFragment = parentFragmentManager.replaceFragment(
                viewGroup.id,
                registrationFragment,
                addToBackStack = true,
                needAnimate = true
            )
        }
    }

    interface OnFragmentInteractionListener {
        fun onLoginButtonPressed(view: View)
        fun onRegistrationRedirectTextPressed(view: View)
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            LoginFragment()
    }
}
