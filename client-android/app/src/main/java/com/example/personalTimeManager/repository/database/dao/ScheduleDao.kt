package com.example.personalTimeManager.repository.database.dao

import androidx.room.Dao
import androidx.room.Query
import com.example.personalTimeManager.repository.database.dao.base.BaseDao
import com.example.personalTimeManager.repository.database.entity.Schedule
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
abstract class ScheduleDao : BaseDao<Schedule>() {
    @Query("SELECT * FROM Schedules")
    abstract fun getAll(): Single<List<Schedule>>

    @Query("SELECT * FROM Schedules WHERE id = :id")
    abstract fun getById(id: Int): Maybe<Schedule>

    @Query("SELECT * FROM Schedules WHERE userId = :userId")
    abstract fun getSchedulesByUserId(userId: Int): Single<List<Schedule>>

    @Query("SELECT MIN(id) FROM Schedules")
    abstract fun getMinId(): Int
}