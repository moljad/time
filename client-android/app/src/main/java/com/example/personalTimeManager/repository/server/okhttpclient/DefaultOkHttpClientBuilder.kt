package com.example.personalTimeManager.repository.server.okhttpclient

import com.example.personalTimeManager.repository.server.security.TrustAllManager
import okhttp3.ConnectionPool
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.security.SecureRandom
import java.util.concurrent.TimeUnit
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSession
import javax.net.ssl.TrustManager

object DefaultOkHttpClientBuilder {
    fun okHttpClientBuilder(): OkHttpClient.Builder {
        return try {
            val trustAllCerts: Array<TrustManager> = arrayOf(TrustAllManager())
            val sslContext = SSLContext.getInstance("SSL")
            sslContext.init(null, trustAllCerts, SecureRandom())

            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.apply {
                httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            }

            OkHttpClient.Builder()

                .connectionPool(ConnectionPool(5, 30, TimeUnit.SECONDS))
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)

                .addInterceptor(httpLoggingInterceptor)


                .sslSocketFactory(
                    sslContext.socketFactory,
                    TrustAllManager()
                )
                .hostnameVerifier(HostnameVerifier { _: String?, _: SSLSession? -> true })

        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }
}