package com.example.personalTimeManager.presentation.activities.states

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.observe
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.personalTimeManager.R
import com.example.personalTimeManager.di.component.ViewModelComponent
import com.example.personalTimeManager.presentation.base.BaseAdapter
import com.example.personalTimeManager.presentation.base.BaseFragment
import com.example.personalTimeManager.repository.database.entity.State
import com.google.android.material.floatingactionbutton.FloatingActionButton
import javax.inject.Inject


class StatesFragment : BaseFragment() {
    lateinit var viewModel: StatesViewModel
        @Inject set

    override fun injectDependency(component: ViewModelComponent) {
        component.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_states, container, false)

        val fab: FloatingActionButton = container!!.rootView.findViewById(R.id.fab)
        fab.show()
        fab.setOnClickListener(this::onFabClick)

        val recyclerView: RecyclerView = view.findViewById(R.id.states_list)

        val adapter = object : BaseAdapter<StatesViewHolder, State>(context!!, mutableListOf()) {
            override fun onBindViewHolder(holder: StatesViewHolder, position: Int) {
                val state = list[position]

                holder.setName(state.name)
                holder.setColor(state.color.stringColor)
            }

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StatesViewHolder {
                val recycleView = inflater.inflate(R.layout.recycle_state_item, parent, false)

                recycleView.setOnClickListener {
                    val position = recyclerView.getChildAdapterPosition(it)
                    val state: State = getItem(position)
                    val action = StatesFragmentDirections.actionNavStatesToNavEditState(state.id)

                    recycleView.findNavController().navigate(action)
                }

                return StatesViewHolder(recycleView)
            }
        }

        recyclerView.adapter = adapter

        viewModel.states.observe(this) {
            Log.e("State list set", it.size.toString())
            adapter.clear()
            adapter.addAll(it)
        }

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.getStates().subscribe(
            viewModel.states::postValue
        ) {
            Log.e("StatesViewModel", "", it)
        }
    }

    fun onFabClick(v: View) {
        val action = StatesFragmentDirections.actionNavStatesToNavEditState(0)

        findNavController().navigate(action)
    }
}