package com.example.personalTimeManager.presentation.widgets

import android.content.Context
import android.content.Context.LAYOUT_INFLATER_SERVICE
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnTouchListener
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.Spinner
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import androidx.databinding.Observable
import com.example.personalTimeManager.R
import com.shawnlin.numberpicker.NumberPicker


class DayTimeSelector(context: Context, attrs: AttributeSet) :
    LinearLayout(context, attrs), Observable {
    private var popupTitle: String = "Day Time Selector"

    private var realSpanValue: Int = 0

    var listener: InverseBindingListener? = null

    companion object {
        @BindingAdapter("spanValue")
        @JvmStatic
        fun setSpanValue(view: DayTimeSelector, newValue: Int) {
            view.realSpanValue = newValue
            view.updateView(newValue)
        }

        @InverseBindingAdapter(attribute = "spanValue")
        @JvmStatic
        fun getSpanValue(view: DayTimeSelector): Int {
            return view.realSpanValue
        }

        @BindingAdapter(value = ["spanValueAttrChanged"])
        @JvmStatic
        fun setListener(view: DayTimeSelector, listener: InverseBindingListener?) {
            if (listener != null) {
                view.listener = listener
            }
        }
    }

    private val dayView: TextView
    private val timeView: TextView

    private var popupWindow: PopupWindow? = null
    private var popupDayView: Spinner? = null
    private var popupTimeView: NumberPicker? = null

    init {
        val inflater = context
            .getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.day_time_selector, this, true)

        timeView = findViewById(R.id.time)
        dayView = findViewById(R.id.day)

        this.setOnClickListener(this::onClickListener)

        val arr: TypedArray = context.obtainStyledAttributes(attrs, R.styleable.DayTimeSelector)

        val newSpanValue = arr.getInteger(R.styleable.DayTimeSelector_spanValue, 0)
        setSpanValue(
            this,
            newSpanValue
        )

        val newTitle = arr.getString(R.styleable.DayTimeSelector_title)
        newTitle?.let {
            popupTitle = it
        }

        arr.recycle()
    }

    private fun onClickListener(view: View) {
        val inflater =
            context.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater

        val popupView = inflater.inflate(R.layout.day_time_popup, null)

        val popupTitleView: TextView = popupView.findViewById(R.id.title)
        popupTitleView.text = popupTitle

        val popupCancelView: View = popupView.findViewById(R.id.cancel)
        popupCancelView.setOnClickListener(this::onPopupCancelClickListener)

        val popupOkView: View = popupView.findViewById(R.id.ok)
        popupOkView.setOnClickListener(this::onPopupOkClickListener)

        popupTimeView = (popupView.findViewById(R.id.time) as NumberPicker).apply {
            this.minValue = 0
            this.maxValue = 143
            this.displayedValues = generateTimeValues()
            this.value = realSpanValue % 144
        }

        popupDayView = (popupView.findViewById(R.id.day) as Spinner).apply {
            this.setSelection(realSpanValue / 144)
        }

        popupWindow =
            PopupWindow(popupView, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, true)
        popupWindow!!.showAtLocation(this, Gravity.CENTER, 0, 0)

        popupView.setOnTouchListener(OnTouchListener { _, _ ->
            popupWindow!!.dismiss()
            true
        })
    }

    private fun onPopupCancelClickListener(view: View) {
        popupWindow?.dismiss()
    }

    private fun onPopupOkClickListener(view: View) {
        val day = popupDayView!!.selectedItemId.toInt()
        val time = popupTimeView!!.value

        setSpanValue(
            this,
            day * 144 + time
        )

        listener?.onChange()
        popupWindow!!.dismiss()
    }

    private fun generateTimeValues(): Array<String> = IntArray(144) { x -> x }
        .map { "${(it / 6)}:${it % 6}0" }
        .toTypedArray()


    private fun updateView(spanValue: Int) {
        val day = spanValue / 144
        val time = spanValue % 144
        val hours = time / 6
        val minutes = time % 6

        val dayNames = resources.getStringArray(R.array.day_names)

        dayView.text = dayNames[day]
        timeView.text = "${hours}:${minutes}0"
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
    }
}