package com.example.personalTimeManager

import android.app.Application
import androidx.room.Room
import com.example.personalTimeManager.di.component.*
import com.example.personalTimeManager.di.module.*
import com.example.personalTimeManager.repository.database.AppDatabase
import com.facebook.stetho.Stetho


class App : Application() {
    private var viewModelComponent: ViewModelComponent? = null
    private var database: AppDatabase? = null

    override fun onCreate() {
        super.onCreate()
        initRoom()
        initDagger()
        Stetho.initializeWithDefaults(this)
    }

    private fun initRoom() {
        database = Room.databaseBuilder(this, AppDatabase::class.java, "database")
            .allowMainThreadQueries()
            .build()
    }

    private fun initDagger() {
        val databaseComponent = DaggerDatabaseComponent.builder()
            .databaseModule(DatabaseModule(this.database!!))
            .build()

        val userComponent = DaggerUserComponent.builder()
            .userModule(UserModule(databaseComponent.database))
            .databaseComponent(databaseComponent)
            .build()

        val apiComponent = DaggerApiComponent.builder()
            .apiModule(ApiModule(userComponent.manager, this))
            .userComponent(userComponent)
            .build()

        val repositoryComponent = DaggerRepositoryComponent.builder()
            .apiComponent(apiComponent)
            .databaseComponent(databaseComponent)
            .userComponent(userComponent)
            .repositoryModule(RepositoryModule())
            .build()

        viewModelComponent = DaggerViewModelComponent.builder()
            .repositoryComponent(repositoryComponent)
            .userComponent(userComponent)
            .viewModelModule(ViewModelModule(this))
            .build()
    }

    fun getViewModelComponent(): ViewModelComponent {
        return this.viewModelComponent!!
    }
}