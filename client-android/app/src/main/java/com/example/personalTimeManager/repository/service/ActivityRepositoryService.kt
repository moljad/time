package com.example.personalTimeManager.repository.service

import android.content.res.Resources
import android.util.Log
import com.example.personalTimeManager.repository.database.AppDatabase
import com.example.personalTimeManager.repository.database.Status
import com.example.personalTimeManager.repository.database.converter.ColorConverters
import com.example.personalTimeManager.repository.database.converter.DateConverters
import com.example.personalTimeManager.repository.database.converter.StatusConverters
import com.example.personalTimeManager.repository.database.entity.Activity
import com.example.personalTimeManager.repository.server.ServerCommunicator
import com.example.personalTimeManager.repository.server.dto.ActivityCreateDto
import com.example.personalTimeManager.repository.server.dto.ActivityUpdateDto
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.MaybeSource
import io.reactivex.Single
import io.reactivex.rxkotlin.zipWith
import retrofit2.HttpException
import java.io.IOException
import java.time.LocalDateTime
import java.util.*
import java.util.concurrent.TimeoutException
import javax.inject.Inject
import kotlin.collections.ArrayList

class ActivityRepositoryService @Inject constructor(
    private val userRepositoryService: UserRepositoryService,
    private val serverCommunicator: ServerCommunicator,
    private val mainDatabase: AppDatabase
) {

    /**gets from local state.
     * If is not found, then tries to synchronize it with current user and get it again**/
    fun guessUserIdByActivityId(id: Int): Maybe<Int> {
        return getLocalActivity(id).map { it.userId }
            .switchIfEmpty(MaybeSource<Int> { maybeObserver ->
                userRepositoryService.guessCurrentUser()
                    .flatMapMaybe { guessedUser ->
                        val maybeSource = getActivityByUserIdAndId(guessedUser.id!!, id)
                            .map { activity -> maybeObserver.onSuccess(activity.userId) }
                        //maybeSource.onErrorReturn { maybeObserver.onError(it) }
                        //maybeSource.isEmpty.filter { it }.doOnComplete {  }
                        //maybeSource.doOnComplete {  }
                        maybeSource
                    }
            }).onErrorResumeNext { _: Throwable -> return@onErrorResumeNext Maybe.empty<Int>() }
    }

    fun getActivitiesByUserId(userId: Int): Single<List<Activity>> {
        val refreshedActivities = synchronizeActivitiesAndGetThemByUserId(userId)
        val localActivities = getLocalActivitiesByUserId(userId)

        return refreshedActivities.zipWith(localActivities)
            .flatMap { (refreshedActivities, localActivities) ->
                if (!refreshedActivities.isPresent) {
                    Log.i("AppRepository", "returning local activities instead")
                    val markedLocalActivities = markAsLocalIfNotDeferred(localActivities)
                    return@flatMap createOrReplaceLocalActivities(markedLocalActivities).andThen(
                        Single.just(markedLocalActivities)
                    )
                }
                return@flatMap Single.just(refreshedActivities.get())
            }
    }

    private fun markAsLocalIfNotDeferred(activities: List<Activity>): List<Activity> {
        return activities.map {
            markAsLocalIfNotDeferred(it)
        }
    }

    private fun markAsLocalIfNotDeferred(activity: Activity): Activity {
        if (activity.status != Status.LOCAL_TO_CREATE
            && activity.status != Status.LOCAL_TO_UPDATE
            && activity.status != Status.LOCAL_TO_DELETE
        ) {
            return activity.copy(status = Status.LOCAL)
        }
        return activity
    }


    fun getLocalActivitiesByUserId(userId: Int): Single<List<Activity>> {
        return mainDatabase.activityDao().getActivitiesByUserId(userId)
    }

    fun synchronizeActivitiesAndGetThemByUserId(
        userId: Int
    ): Single<Optional<List<Activity>>> {
        return synchronizeActivities(userId).andThen(
            getLocalActivitiesByUserId(userId)
        ).map { Optional.of(it) }.onErrorResumeNext { t ->
            if (t is IOException || t is TimeoutException) {
                return@onErrorResumeNext Single.just(Optional.empty<List<Activity>>())
            }
            Single.error<Optional<List<Activity>>>(t)
        }
    }

    private fun synchronizeActivities(userId: Int): Completable {
        return getLocalActivitiesByUserId(userId).flatMapCompletable { localActivities ->
            val synchronizeToCreate = synchronizeDeferredToCreateActivities(localActivities)
            val synchronizeToUpdate = synchronizeDeferredToUpdateActivities(localActivities)
            val synchronizeToDelete = synchronizeDeferredToDeleteActivities(localActivities)

            Completable.mergeArrayDelayError(
                synchronizeToCreate, synchronizeToUpdate, synchronizeToDelete
            ).andThen(getRemotesAndRefreshActivities(userId, localActivities))
        }
    }

    private fun synchronizeDeferredToCreateActivities(
        localActivities: List<Activity>
    ): Completable {
        val taskList = ArrayList<Completable>()
        localActivities.forEach { localActivity ->
            if (localActivity.status == Status.LOCAL_TO_CREATE) {
                val createRemoteActivity =
                    synchronizeDeferredToCreateActivity(localActivity)
                taskList.add(createRemoteActivity)
            }
        }
        val params = taskList.toTypedArray()
        return Completable.mergeArrayDelayError(*params)
    }

    private fun synchronizeDeferredToCreateActivity(localActivity: Activity): Completable {
        return createRemoteActivity(localActivity.userId, localActivity.activityCreateDto)
            .flatMapCompletable { newActivity ->
                val (id, name, description, priority, isRest, link, lengthMin, lengthMax, lengthRequired, color, userId, createdDate, updatedDate, status) = newActivity
                val stringColor = ColorConverters.colorToString(color)
                val stringCreateDate = DateConverters.dateToString(createdDate)
                val stringUpdatedDate = DateConverters.dateToString(updatedDate)
                val stringStatus = StatusConverters.statusToString(status)

                mainDatabase.activityDao().replace(
                    localActivity.id, id, name, description, priority, isRest, link,
                    lengthMin, lengthMax, lengthRequired, stringColor, userId,
                    stringCreateDate, stringUpdatedDate, stringStatus
                )
            }.onErrorResumeNext { t ->
                if (t is HttpException && t.code() == 500) {
                    return@onErrorResumeNext deleteLocalActivity(localActivity)
                }
                if (t is IOException || t is TimeoutException) {
                    return@onErrorResumeNext Completable.complete()
                }
                return@onErrorResumeNext Completable.error(t)
            }
    }

    private fun synchronizeDeferredToUpdateActivities(localActivities: List<Activity>): Completable {
        val taskList = ArrayList<Completable>()
        localActivities.forEach { localActivity ->
            if (localActivity.status == Status.LOCAL_TO_UPDATE) {
                val updateRemoteActivity =
                    synchronizeDeferredToUpdateActivity(localActivity)
                taskList.add(updateRemoteActivity)
            }
        }
        val params = taskList.toTypedArray()
        return Completable.mergeArrayDelayError(*params)
    }

    private fun synchronizeDeferredToUpdateActivity(localActivity: Activity): Completable {
        return updateRemoteActivity(
            localActivity.userId,
            localActivity.id,
            localActivity.activityUpdateDto
        ).flatMapCompletable { Completable.complete() }
            .onErrorResumeNext { t ->
                if (t is HttpException && t.code() == 500) {
                    return@onErrorResumeNext getActivityByUserIdAndId(
                        localActivity.userId,
                        localActivity.id
                    ).flatMapCompletable { Completable.complete() }
                }
                if (t is IOException || t is TimeoutException) {
                    return@onErrorResumeNext Completable.complete()
                }
                return@onErrorResumeNext Completable.error(t)
            }
    }

    private fun synchronizeDeferredToDeleteActivities(localActivities: List<Activity>): Completable {
        val taskList = ArrayList<Completable>()
        localActivities.forEach { localActivity ->
            if (localActivity.status == Status.LOCAL_TO_DELETE) {
                val deleteSynchronizationTask =
                    synchronizeDeferredToDeleteActivity(localActivity)
                taskList.add(deleteSynchronizationTask)
            }
        }
        val params = taskList.toTypedArray()
        return Completable.mergeArrayDelayError(*params)
    }

    private fun synchronizeDeferredToDeleteActivity(localActivity: Activity): Completable {
        return deleteRemoteActivity(localActivity.userId, localActivity.id)
            .andThen(deleteLocalActivity(localActivity))
            .onErrorResumeNext { t ->
                if (t is HttpException && t.code() == 500) {
                    return@onErrorResumeNext getActivityByUserIdAndId(
                        localActivity.userId,
                        localActivity.id
                    ).flatMapCompletable { Completable.complete() }
                }
                if (t is IOException || t is TimeoutException) {
                    return@onErrorResumeNext Completable.complete()
                }
                return@onErrorResumeNext Completable.error(t)
            }
    }

    private fun getRemotesAndRefreshActivities(
        userId: Int,
        localActivities: List<Activity>
    ): Completable {
        return getRemoteActivities(userId).flatMapCompletable { listOptional ->
            if (listOptional.isPresent) {
                val remoteActivities = listOptional.get()
                return@flatMapCompletable removeRedundantActivities(
                    localActivities,
                    remoteActivities
                ).andThen(
                    createOrReplaceLocalActivities(remoteActivities)
                )
            }
            return@flatMapCompletable Completable.error(IOException("Could not receive remote entities"))
        }
    }

    fun getRemoteActivities(userId: Int): Single<Optional<List<Activity>>> {
        return serverCommunicator.getActivities(userId)
            .map { Optional.of(it.map { newActivity -> newActivity.copy(status = Status.SYNCHRONIZED) }) }
            .onErrorResumeNext { t ->
                if (t is IOException || t is TimeoutException) {
                    Log.e("AppRepository", t.message!!)
                    return@onErrorResumeNext Single.just(Optional.empty())
                }
                return@onErrorResumeNext Single.just(Optional.empty())
            }
    }

    private fun removeRedundantActivities(
        localActivities: List<Activity>,
        actualActivities: List<Activity>
    ): Completable {
        val newIds = actualActivities.map { it.id }
        val localIds = localActivities.filter {
            it.status != Status.LOCAL_TO_CREATE
                    && it.status != Status.LOCAL_TO_UPDATE
                    && it.status != Status.LOCAL_TO_DELETE
        }.map { it.id }
        val redundantIds = localIds.subtract(newIds)

        val redundant = localActivities.filter { it.id in redundantIds }
        if (redundant.isNotEmpty()) {
            return deleteActivitiesLocally(redundant)
        }
        return Completable.complete()
    }

    private fun deleteActivitiesLocally(list: List<Activity>): Completable {
        return mainDatabase.activityDao().deleteList(list)
    }

    private fun createOrReplaceLocalActivities(actualActivities: List<Activity>): Completable {
        return Completable.fromCallable {
            mainDatabase.activityDao().upsertList(actualActivities)
        }
    }

    fun getActivityByUserIdAndId(userId: Int, id: Int): Maybe<Activity> {
        val communicatorActivity =
            getRemoteActivity(userId, id).map { Optional.of(it) }.defaultIfEmpty(Optional.empty())
        val localActivity =
            getLocalActivity(id).map { Optional.of(it) }.defaultIfEmpty(Optional.empty())

        return communicatorActivity.zipWith(localActivity)
            .flatMap { (communicatorActivity, localActivity) ->
                if (!communicatorActivity.isPresent) {    //server doesn't have it, but local does
                    if (localActivity.isPresent) {  //redundant local data
                        deleteRedundantActivityAsync(localActivity.get())
                    }
                    return@flatMap Maybe.empty<Activity>()
                }
                if (communicatorActivity.isPresent && communicatorActivity.get().isPresent) { //ok. just refresh local data with this new value
                    val actualActivity = communicatorActivity.get().get()
                    return@flatMap createOrReplaceLocalActivityAndGetIt(actualActivity).toMaybe()
                }
                if (communicatorActivity.isPresent && !communicatorActivity.get().isPresent && localActivity.isPresent) {
                    Log.i("AppRepository", "returning local activity instead")
                    val markedActivity = markAsLocalIfNotDeferred(localActivity.get())
                    if (markedActivity == localActivity.get()) {
                        return@flatMap Maybe.just(localActivity.get())
                    }
                    return@flatMap createOrReplaceLocalActivity(markedActivity).andThen(
                        Maybe.just(markedActivity)
                    )
                }
                return@flatMap Maybe.error<Activity>(Resources.NotFoundException("activity was never fetched and connection problem occurred"))
            }
    }

    private fun createOrReplaceLocalActivityAndGetIt(activity: Activity): Single<Activity> {
        return createOrReplaceLocalActivity(activity).andThen(getLocalActivity(activity.id))
            .toSingle()
    }

    private fun createOrReplaceLocalActivity(actualActivity: Activity): Completable {
        return Completable.fromCallable {
            mainDatabase.activityDao().upsert(actualActivity)
        }
    }

    private fun deleteRedundantActivityAsync(
        localActivityValue: Activity
    ) {
        if (localActivityValue.status != Status.LOCAL_TO_CREATE
            && localActivityValue.status != Status.LOCAL_TO_UPDATE
            && localActivityValue.status != Status.LOCAL_TO_DELETE
        ) {
            deleteActivityLocally(localActivityValue).subscribe()   //would be removed in async way
        }
    }

    private fun deleteActivityLocally(activity: Activity): Completable {
        return mainDatabase.activityDao().delete(activity)
    }

    /**
     * optional exist over optional empty = connection problem
     * optional exist over optional exist = exists and connection was ok
     * optional empty over optional empty = ??? impossible. Server throws exception on 500 and i dont have a chance to wrap it. There is no Maybe.just(Optional.empty).map(Optional.empty(it))
     * optional empty over optional exist = server doesn't have it and connection was ok
     */
    private fun getRemoteActivity(userId: Int, id: Int): Maybe<Optional<Activity>> {
        return serverCommunicator.getActivity(userId, id)
            .map { it.copy(status = Status.SYNCHRONIZED) }
            .map { Optional.of(it) }.toMaybe()
            .onErrorResumeNext { t: Throwable ->
                if (t is HttpException && t.code() == 500) {
                    return@onErrorResumeNext Maybe.empty<Optional<Activity>>()
                }
                if (t is IOException || t is TimeoutException) {
                    return@onErrorResumeNext Maybe.just(Optional.empty<Activity>())
                }
                Maybe.error(t)
            }
    }

    fun createActivityAndGetItById(userId: Int, createDto: ActivityCreateDto): Single<Activity> {
        val newCommunicatorActivity = createRemoteActivity(userId, createDto)
        return newCommunicatorActivity.flatMap {
            createOrReplaceLocalActivityAndGetIt(it)
        }.onErrorResumeNext { t ->
            if (t is IOException || t is TimeoutException) {
                Log.e("AppRepository", t.message!!)
                return@onErrorResumeNext createLocalActivityByDtoAndGetIt(createDto)
            }
            Single.error(t)
        }
    }

    private fun createRemoteActivity(userId: Int, createDto: ActivityCreateDto): Single<Activity> {
        return serverCommunicator.createActivity(userId, createDto)
            .map { it.copy(status = Status.SYNCHRONIZED) }
    }

    fun getLocalActivity(id: Int): Maybe<Activity> {
        return mainDatabase.activityDao().getById(id)
    }

    fun updateActivityAndGetItById(id: Int, updateDto: ActivityUpdateDto): Single<Activity> {
        val localActivityById = getLocalActivity(id).switchIfEmpty(
            userRepositoryService.guessCurrentUser().flatMapMaybe {
                getActivityByUserIdAndId(it.id!!, id)
            }.switchIfEmpty(Single.error<Activity>(Resources.NotFoundException("can not find activity to update")))
        )

        return localActivityById.flatMap {
            updateRemoteActivity(it.userId, it.id, updateDto)
        }.flatMap {
            updateAndGetLocalActivity(it)
        }.onErrorResumeNext { t ->
            if (t is IOException || t is TimeoutException) {
                Log.e("AppRepository", t.message!!)
                return@onErrorResumeNext updateLocalActivityByDtoAndGetIt(id, updateDto)
            }
            Single.error(t)
        }
    }

    private fun updateRemoteActivity(
        userId: Int,
        id: Int,
        updateDto: ActivityUpdateDto
    ): Single<Activity> {
        return serverCommunicator.updateActivity(userId, id, updateDto)
            .map { it.copy(status = Status.SYNCHRONIZED) }
    }

    private fun updateAndGetLocalActivity(activity: Activity): Single<Activity> {
        return updateLocalActivity(activity).andThen(getLocalActivity(activity.id).toSingle())
    }

    private fun updateLocalActivity(activity: Activity): Completable {
        return mainDatabase.activityDao().update(activity)
    }

    fun deleteActivity(id: Int): Completable {
        val localActivityById = getLocalActivity(id).switchIfEmpty(
            userRepositoryService.guessCurrentUser().flatMapMaybe {
                getActivityByUserIdAndId(it.id!!, id)
            }
        )
        return localActivityById.isEmpty.flatMapCompletable { isLocalActivityEmpty ->
            if (isLocalActivityEmpty) { //just send a request with current user
                return@flatMapCompletable userRepositoryService.guessCurrentUser()
                    .flatMapCompletable { user ->
                        deleteRemoteActivity(user.id!!, id)
                    }
            }
            return@flatMapCompletable localActivityById.flatMapCompletable(
                deleteExistingActivity()
            )
        }
    }

    private fun deleteExistingActivity(): (Activity) -> Completable {
        return completable@{ localActivity ->
            if (localActivity.status == Status.LOCAL_TO_CREATE) {
                return@completable deleteLocalActivity(localActivity)
            }
            return@completable deleteRemoteActivity(
                localActivity.userId,
                localActivity.id
            ).andThen(deleteLocalActivity(localActivity)).onErrorResumeNext { t ->
                if (t is IOException || t is TimeoutException) {
                    return@onErrorResumeNext updateLocalActivity(localActivity.copy(status = Status.LOCAL_TO_DELETE))
                }
                if (t is HttpException && t.code() == 500) {
                    return@onErrorResumeNext deleteLocalActivity(localActivity)
                }
                return@onErrorResumeNext Completable.error(t)
            }
        }
    }

    private fun deleteRemoteActivity(userId: Int, id: Int): Completable {
        return serverCommunicator.deleteActivity(userId, id)
    }

    private fun deleteLocalActivity(activity: Activity): Completable {
        return mainDatabase.activityDao().delete(activity)
    }

    private fun createLocalActivityByDtoAndGetIt(createDto: ActivityCreateDto): Single<Activity> {
        val activityFromCreateDto = createDto.activity
        val minId: Int = if (mainDatabase.activityDao().getMinId() > 0) {
            -mainDatabase.activityDao().getMinId()
        } else {
            mainDatabase.activityDao().getMinId() - 1
        }
        return userRepositoryService.getCurrentUser().flatMap {
            val activity = activityFromCreateDto.copy(
                id = minId,
                userId = it.id!!,
                createdDate = LocalDateTime.now(),
                updatedDate = LocalDateTime.now()
            )
            createOrReplaceLocalActivityAndGetIt(activity)
        }
    }

    private fun updateLocalActivityByDtoAndGetIt(
        id: Int,
        updateDto: ActivityUpdateDto
    ): Single<Activity> {
        val (name, description, priority, isRest, link, lengthMin, lengthMax, lengthRequired, color) = updateDto
        return getLocalActivity(id).flatMapSingle { activityToUpdate ->
            val unsavedUpdatedActivity = activityToUpdate.copy(
                name = name ?: activityToUpdate.name,
                description = description ?: activityToUpdate.description,
                priority = priority ?: activityToUpdate.priority,
                isRest = isRest ?: activityToUpdate.isRest,
                link = link ?: activityToUpdate.link,
                lengthMin = lengthMin ?: activityToUpdate.lengthMin,
                lengthMax = lengthMax ?: activityToUpdate.lengthMax,
                lengthRequired = lengthRequired ?: activityToUpdate.lengthRequired,
                color = color ?: activityToUpdate.color,
                updatedDate = LocalDateTime.now(),
                status = if (activityToUpdate.status == Status.LOCAL_TO_CREATE) Status.LOCAL_TO_CREATE else Status.LOCAL_TO_UPDATE
            )
            updateAndGetLocalActivity(unsavedUpdatedActivity)
        }
    }
}