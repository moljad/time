package com.example.personalTimeManager.repository.database.viewentities

import androidx.room.Embedded
import androidx.room.Relation
import com.example.personalTimeManager.repository.database.entity.Schedule
import com.example.personalTimeManager.repository.database.entity.UserEntity


data class UserEntityWithSchedules(
    @Embedded val user: UserEntity,
    @Relation(
        parentColumn = "id",
        entityColumn = "userId",
        entity = Schedule::class
    ) val schedules: List<Schedule>
)