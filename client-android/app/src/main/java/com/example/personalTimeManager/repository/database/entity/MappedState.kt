package com.example.personalTimeManager.repository.database.entity

import androidx.room.*
import com.example.personalTimeManager.repository.Span
import com.example.personalTimeManager.repository.database.Status
import com.example.personalTimeManager.repository.database.converter.DateConverters
import com.example.personalTimeManager.repository.server.dto.MappedStateCreateDto
import com.example.personalTimeManager.repository.server.dto.MappedStateUpdateDto
import com.example.personalTimeManager.repository.server.gson.Exclude
import com.google.gson.annotations.SerializedName
import java.time.LocalDateTime

@Entity(
    tableName = "MappedStates",
    foreignKeys = [
        ForeignKey(
            entity = State::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("stateId"),
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = Schedule::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("scheduleId"),
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class MappedState(
    @PrimaryKey
    @SerializedName("id")
    val id: Int,
    @Embedded
    @SerializedName("span")
    val span: Span,
    @ColumnInfo(name = "stateId", index = true)
    @SerializedName("stateId")
    val stateId: Int,
    @ColumnInfo(name = "scheduleId", index = true)
    @SerializedName("scheduleId")
    val scheduleId: Int,
    @TypeConverters(DateConverters::class)
    @SerializedName("createdDate")
    val createdDate: LocalDateTime,
    @TypeConverters(DateConverters::class)
    @SerializedName("updatedDate")
    val updatedDate: LocalDateTime,
    @Exclude
    val status: Status = Status.UNKNOWN
) {
    @Suppress("unused")
    private constructor() : this(
        -1073741824, Span(0, 1), -1073741824, -1073741824,
        LocalDateTime.now(), LocalDateTime.now()
    )

    @Exclude
    @delegate:Exclude
    @delegate:Ignore
    val mappedStateCreateDto by lazy {
        MappedStateCreateDto(
            this.span, this.stateId
        )
    }

    @Exclude
    @delegate:Exclude
    @delegate:Ignore
    val mappedStateUpdateDto by lazy {
        MappedStateUpdateDto(
            this.span, this.stateId
        )
    }
}