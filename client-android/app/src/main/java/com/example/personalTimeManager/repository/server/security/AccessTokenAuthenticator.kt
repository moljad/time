package com.example.personalTimeManager.repository.server.security

import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route

class AccessTokenAuthenticator(private val accessTokenRepository: AccessTokenRepository) :
    Authenticator {

    override fun authenticate(route: Route?, response: Response): Request? {

        // Need to refresh an access token
        val updatedAccessToken: String = accessTokenRepository.refreshAccessToken() ?: return null
        return newRequestWithAccessToken(response.request, updatedAccessToken)

    }

    private fun newRequestWithAccessToken(request: Request, accessToken: String): Request {
        return request.newBuilder()
            .header("Authorization", "Bearer $accessToken")
            .build()
    }
}