package com.example.personalTimeManager.di.module

import android.content.Context
import com.example.personalTimeManager.di.scope.ApiScope
import com.example.personalTimeManager.repository.CustomColor
import com.example.personalTimeManager.repository.server.ApiService
import com.example.personalTimeManager.repository.server.ServerCommunicator
import com.example.personalTimeManager.repository.server.gson.CustomColorAdapter
import com.example.personalTimeManager.repository.server.gson.CustomExclusionStrategy
import com.example.personalTimeManager.repository.server.gson.LocalDateTimeAdapter
import com.example.personalTimeManager.repository.server.okhttpclient.AuthorizationInterceptor
import com.example.personalTimeManager.repository.server.okhttpclient.DefaultOkHttpClientBuilder
import com.example.personalTimeManager.repository.server.okhttpclient.NetworkConnectionInterceptor
import com.example.personalTimeManager.repository.server.security.AccessTokenAuthenticator
import com.example.personalTimeManager.repository.server.security.AccessTokenRepository
import com.example.personalTimeManager.repository.user.UserManager
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.time.LocalDateTime


@Module
class ApiModule(
    private val userManager: UserManager,
    private val context: Context? = null
) {

    companion object {
        private const val API_URL = "https://10.0.2.2:5001"
    }

    @Provides
    @ApiScope
    fun provideCommunicator(apiService: ApiService): ServerCommunicator {
        return ServerCommunicator(apiService)
    }

    @Provides
    @ApiScope
    fun provideApiService(retrofit: Retrofit): ApiService {
        return retrofit.create<ApiService>(ApiService::class.java)
    }

    @Provides
    @ApiScope
    fun provideRetrofit(builder: Retrofit.Builder): Retrofit {
        return builder.baseUrl(API_URL).build()
    }

    @Provides
    @ApiScope
    fun provideRetrofitBuilder(accessTokenRepository: AccessTokenRepository): Retrofit.Builder {
        val okHttpClientBuilder =
            DefaultOkHttpClientBuilder.okHttpClientBuilder()
        okHttpClientBuilder.addNetworkInterceptor(StethoInterceptor())
        if (context != null) {
            okHttpClientBuilder.addInterceptor(NetworkConnectionInterceptor(context))
        }
        okHttpClientBuilder.addNetworkInterceptor(AuthorizationInterceptor(accessTokenRepository))
        okHttpClientBuilder.authenticator(
            AccessTokenAuthenticator(accessTokenRepository)
        )

        val gson = GsonBuilder()
            .registerTypeAdapter(LocalDateTime::class.java, LocalDateTimeAdapter)
            .registerTypeAdapter(CustomColor::class.java, CustomColorAdapter)
            .setExclusionStrategies(CustomExclusionStrategy)
            .create()
        return Retrofit.Builder()
            .client(okHttpClientBuilder.build())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
    }

    @Provides
    @ApiScope
    fun provideAccessTokenRepository(userManager: UserManager): AccessTokenRepository {
        return AccessTokenRepository(
            DefaultOkHttpClientBuilder.okHttpClientBuilder().build(),
            userManager
        )
    }
}