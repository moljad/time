package com.example.personalTimeManager.presentation.activities.activitySchedule

import com.alamkanak.weekview.WeekViewDisplayable
import com.alamkanak.weekview.WeekViewEvent
import com.example.personalTimeManager.repository.Span
import java.util.*

data class ActivityScheduleEvent(
    val id: Long,
    val mappedActivityId: Int,
    val title: String,
    val startOfWeek: Calendar,
    val span: Span,
    val color: Int
) : WeekViewDisplayable<ActivityScheduleEvent> {

    override fun toWeekViewEvent(): WeekViewEvent<ActivityScheduleEvent> {
        val style = WeekViewEvent.Style.Builder()
            .setBackgroundColor(color)
            .setTextStrikeThrough(false)
            .build()

        val start = startOfWeek.clone() as Calendar
        val end = startOfWeek.clone() as Calendar

        start.add(Calendar.MINUTE, span.begin * 10)
        end.add(Calendar.MINUTE, span.end * 10)

        return WeekViewEvent.Builder(this)
            .setId(id)
            .setTitle(title)
            .setStartTime(start)
            .setEndTime(end)
            .setLocation("")
            .setAllDay(false)
            .setStyle(style)
            .build()
    }

}