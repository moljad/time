package com.example.personalTimeManager.repository.database.dao.base

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Single


@Dao
abstract class BaseDao<T> {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insert(entity: T): Completable

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insertWithSingleResult(entity: T): Single<Long>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insertList(list: List<T>): Completable

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insertListWithSingleResult(list: List<T>): Single<List<Long>>

    @Update
    abstract fun update(entity: T): Completable

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun updateWithSingleResult(entity: T): Single<Long>

    @Update
    abstract fun updateList(list: List<T>): Completable

    @Delete
    abstract fun delete(entity: T): Completable

    @Delete
    abstract fun deleteList(list: List<T>): Completable

    @Transaction
    open fun upsert(obj: T) {
        return insertWithSingleResult(obj).flatMapCompletable { id ->
            if (id == -1L) {
                return@flatMapCompletable update(obj)
            }
            Completable.complete()
        }.blockingAwait()
    }

    @Transaction
    open fun upsertList(objList: List<T>) {
        return insertListWithSingleResult(objList).flatMapCompletable { insertResult ->
            val updateList: MutableList<T> = ArrayList()
            for (i in insertResult.indices) {
                if (insertResult[i] == -1L) {
                    updateList.add(objList[i])
                }
            }
            if (!updateList.isEmpty()) {
                return@flatMapCompletable updateList(updateList)
            }
            Completable.complete()
        }.blockingAwait()
    }
}