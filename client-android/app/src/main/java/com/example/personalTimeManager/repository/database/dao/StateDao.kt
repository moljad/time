package com.example.personalTimeManager.repository.database.dao

import androidx.room.Dao
import androidx.room.Query
import com.example.personalTimeManager.repository.database.dao.base.BaseDao
import com.example.personalTimeManager.repository.database.entity.State
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
abstract class StateDao : BaseDao<State>() {
    @Query("SELECT * FROM States")
    abstract fun getAll(): Single<List<State>>

    @Query("SELECT * FROM States WHERE id = :id")
    abstract fun getById(id: Int): Maybe<State>

    @Query("SELECT * FROM States WHERE userId = :userId")
    abstract fun getStatesByUserId(userId: Int): Single<List<State>>

    @Query("SELECT MIN(id) FROM States")
    abstract fun getMinId(): Int

    @Query(
        """UPDATE States SET id = :id, name = :name, description = :description, color = :color,
        userId = :userId, createdDate = :createdDate, updatedDate = :updatedDate, status = :status
        WHERE id = :oldStateId"""
    )
    abstract fun replace(
        oldStateId: Int,
        id: Int, name: String, description: String, color: String, userId: Int,
        createdDate: String, updatedDate: String, status: String
    ): Completable
}