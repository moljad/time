package com.example.personalTimeManager.presentation.activities.mappedStateEditor

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.example.personalTimeManager.R
import com.example.personalTimeManager.databinding.FragmentMappedStateEditorBinding
import com.example.personalTimeManager.di.component.ViewModelComponent
import com.example.personalTimeManager.presentation.base.BaseFragment
import com.example.personalTimeManager.repository.Span
import com.example.personalTimeManager.repository.database.entity.State
import com.example.personalTimeManager.repository.server.dto.MappedStateCreateDto
import com.example.personalTimeManager.repository.server.dto.MappedStateUpdateDto
import com.google.android.material.floatingactionbutton.FloatingActionButton
import io.reactivex.functions.Consumer
import javax.inject.Inject

class MappedStateEditorFragment : BaseFragment() {
    lateinit var viewModel: MappedStateEditorViewModel
        @Inject set

    override fun injectDependency(component: ViewModelComponent) {
        component.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentMappedStateEditorBinding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_mapped_state_editor, container, false
            )
        binding.fragment = this
        binding.lifecycleOwner = this

        val view = binding.root

        val fab: FloatingActionButton = container!!.rootView.findViewById(R.id.fab)
        fab.hide()

        val state: Spinner = view.findViewById(R.id.state)!!

        val arrayAdapter = ArrayAdapter<String>(
            context!!,
            android.R.layout.simple_spinner_item,
            arrayOf("a", "b", "c")
        )
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        state.adapter = arrayAdapter

        state.onItemSelectedListener = object : AdapterView.OnItemClickListener,
            AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (viewModel.states.size > 0) {
                    viewModel.mappedState.value?.stateId = viewModel.states[position].id
                }
            }

            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

            }
        }

        val updateButtonView: Button = view.findViewById(R.id.update)
        updateButtonView.setOnClickListener(this::onUpdateButtonPressed)

        val deleteButtonView: Button = view.findViewById(R.id.delete)
        deleteButtonView.setOnClickListener(this::onDeleteButtonPressed)

        val createButtonView: Button = view.findViewById(R.id.create)
        createButtonView.setOnClickListener(this::onCreateButtonPressed)

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val mappedStateId = MappedStateEditorFragmentArgs.fromBundle(arguments!!).mappedStateId

        if (mappedStateId != 0) {
            viewModel.getMappedState(mappedStateId).subscribe {
                viewModel.mappedState.postValue(
                    MappedStateEditorViewModel.MappedStateModel(
                        mappedStateId,
                        it.span.begin,
                        it.span.end,
                        it.stateId
                    )
                )
            }
        } else {
            viewModel.mappedState.postValue(
                MappedStateEditorViewModel.MappedStateModel(
                    mappedStateId,
                    0,
                    6,
                    -1
                )
            )
        }

        viewModel.getStates().subscribe({
            viewModel.states = it

            val selectedState =
                viewModel.states.find { it.id == viewModel.mappedState.value?.stateId }
            val statePos = viewModel.states.indexOf(selectedState)

            val names = viewModel.states.map(State::name)

            val arrayAdapter =
                ArrayAdapter<String>(context!!, android.R.layout.simple_spinner_item, names)
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

            val state: Spinner = view!!.findViewById(R.id.state)!!
            state.adapter = arrayAdapter
            state.setSelection(statePos)
        },
            {
                throw Exception()
            })
    }

    fun onUpdateButtonPressed(v: View) {
        val mappedState = viewModel.mappedState.value!!

        val mappedStateUpdateDto = MappedStateUpdateDto(
            Span(mappedState.begin, mappedState.end),
            mappedState.stateId
        )

        viewModel.updateMappedState(mappedState.id, mappedStateUpdateDto).subscribe(Consumer {
            findNavController().popBackStack()
        })
    }

    fun onDeleteButtonPressed(v: View) {
        viewModel.deleteMappedState(viewModel.mappedState.value?.id!!).subscribe {
            findNavController().popBackStack()
        }
    }

    fun onCreateButtonPressed(v: View) {
        val mappedState = viewModel.mappedState.value!!

        val mappedStateCreateDto = MappedStateCreateDto(
            Span(mappedState.begin, mappedState.end),
            mappedState.stateId
        )

        viewModel.createMappedState(mappedStateCreateDto).subscribe(Consumer {
            findNavController().popBackStack()
        })
    }

}
