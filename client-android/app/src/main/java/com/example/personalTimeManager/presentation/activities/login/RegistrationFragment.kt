package com.example.personalTimeManager.presentation.activities.login

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.example.personalTimeManager.R
import com.example.personalTimeManager.databinding.FragmentRegistrationBinding
import com.example.personalTimeManager.di.component.ViewModelComponent
import com.example.personalTimeManager.presentation.base.BaseFragment
import com.example.personalTimeManager.util.replaceFragment
import com.example.personalTimeManager.util.showToast
import javax.inject.Inject

class RegistrationFragment : BaseFragment() {

    var listener: OnFragmentInteractionListener? = null

    var viewModel: LoginRegistrationViewModel? = null
        @Inject set

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentRegistrationBinding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_registration, container, false
            )
        binding.viewModel = this.viewModel
        return binding.root
    }

    override fun injectDependency(component: ViewModelComponent) {
        component.inject(this)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    fun onRegistrationButtonPressed() {
        try {
            viewModel?.let {
                it.registerUser(it.registrationModel.value!!)
            }
        } catch (e: RuntimeException) {
            activity?.showToast("Can not register user. Reason: ${e.message}")
        }


        /*val okHttpClient = ApiModule.okHttpClientBuilder().build()
        val request = AccessTokenRepository.createTokenRequest(email!!, password!!)
        val newCall = okHttpClient.newCall(request)
        val response = newCall.execute()
        if (response.isSuccessful) {
            Log.i("LoginRegActivity", "OK")
        } else {
            Log.e("LoginRegActivity", "Not authorized")
        }*/
    }

    /**Tries to replace current container with provided Registration fragment and adds it to backstack of fragmentManager.
     * If function is not provided with Registration Fragment, then tries to create it by itself
     * Tries to get fragmentManager of current activity by fragmentActivity method
     * If current activity is not fragmentActivity, then tries to obtain fragmentManager by basic fragment method
     * If fragmentManager can not be obtained, then just returns
     * **/
    fun onLoginRedirectTextPressed(
        loginFragment: Fragment = LoginFragment.newInstance(),
        viewGroup: ViewGroup? = view?.parent as? ViewGroup
    ) {
        if (viewGroup == null) {
            Log.e("LoginFragment", "Can not obtain current container")
            return
        }
        val transaction = if (activity is FragmentActivity) {
            activity?.supportFragmentManager?.replaceFragment(
                viewGroup.id,
                loginFragment,
                addToBackStack = true,
                needAnimate = true
            )
        } else {
            val replaceFragment = parentFragmentManager.replaceFragment(
                viewGroup.id,
                loginFragment,
                addToBackStack = true,
                needAnimate = true
            )
        }
    }

    interface OnFragmentInteractionListener {
        fun onRegistrationButtonPressed(view: View)
        fun onLoginRedirectTextPressed(view: View)
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            RegistrationFragment()
    }
}
