package com.example.personalTimeManager.repository.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.personalTimeManager.repository.database.converter.ColorConverters
import com.example.personalTimeManager.repository.database.converter.DateConverters
import com.example.personalTimeManager.repository.database.converter.StatusConverters
import com.example.personalTimeManager.repository.database.dao.*
import com.example.personalTimeManager.repository.database.entity.*

@Database(
    entities = [Activity::class, Group::class, MappedActivity::class, MappedState::class,
        Schedule::class, State::class, UserEntity::class, ActivityState::class], version = 1
)
@TypeConverters(ColorConverters::class, DateConverters::class, StatusConverters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun scheduleDao(): ScheduleDao
    abstract fun stateDao(): StateDao
    abstract fun activityDao(): ActivityDao
    abstract fun activityStateDao(): ActivityStateDao
    abstract fun mappedStateDao(): MappedStateDao
    abstract fun mappedActivityDao(): MappedActivityDao
}