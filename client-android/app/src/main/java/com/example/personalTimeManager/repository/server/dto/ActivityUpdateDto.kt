package com.example.personalTimeManager.repository.server.dto

import com.example.personalTimeManager.repository.CustomColor
import com.google.gson.annotations.SerializedName

data class ActivityUpdateDto(
    @SerializedName("name")
    val name: String?,
    @SerializedName("description")
    val description: String?,
    @SerializedName("priority")
    val priority: Int?,
    @SerializedName("isRest")
    val isRest: Boolean?,
    @SerializedName("link")
    val link: String?,
    @SerializedName("lengthMin")
    val lengthMin: Int?,
    @SerializedName("lengthMax")
    val lengthMax: Int?,
    @SerializedName("lengthRequired")
    val lengthRequired: Int?,
    @SerializedName("color")
    val color: CustomColor?
)