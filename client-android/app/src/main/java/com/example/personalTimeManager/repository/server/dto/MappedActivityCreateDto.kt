package com.example.personalTimeManager.repository.server.dto

import androidx.room.Ignore
import com.example.personalTimeManager.repository.Span
import com.example.personalTimeManager.repository.database.Status
import com.example.personalTimeManager.repository.database.entity.MappedActivity
import com.example.personalTimeManager.repository.server.gson.Exclude
import com.google.gson.annotations.SerializedName
import java.time.LocalDateTime

data class MappedActivityCreateDto(
    @SerializedName("span")
    val span: Span,
    @SerializedName("activityId")
    val activityId: Int
) {
    @Suppress("unused")
    private constructor() : this(Span(0, 6), -1073741824)

    @Exclude
    @delegate:Exclude
    @delegate:Ignore
    val mappedActivity by lazy {
        MappedActivity(
            -1073741824,
            this.span,
            this.activityId,
            -1073741824,
            createdDate = LocalDateTime.now(),
            updatedDate = LocalDateTime.now(),
            status = Status.LOCAL_TO_CREATE
        )
    }
}