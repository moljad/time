package com.example.personalTimeManager.repository.database.converter

import androidx.room.TypeConverter
import com.example.personalTimeManager.repository.CustomColor


object ColorConverters {

    //HEX
    @TypeConverter
    @JvmStatic
    fun colorFromString(value: String): CustomColor {
        return CustomColor(value)
    }

    @TypeConverter
    @JvmStatic
    fun colorToString(value: CustomColor): String {
        return value.stringColor
    }
}