package com.example.personalTimeManager.presentation.base

import android.content.Context
import android.view.LayoutInflater
import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<VH : RecyclerView.ViewHolder, M>
    (context: Context, protected var list: MutableList<M>) :
    RecyclerView.Adapter<VH>() {

    var inflater: LayoutInflater = LayoutInflater.from(context)

    abstract override fun onBindViewHolder(holder: VH, position: Int)

    fun getItems(): MutableList<M> {
        return list
    }

    fun getItem(position: Int): M {
        return list[position]
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun add(item: M) {
        list.add(item)
        notifyDataSetChanged()
    }

    fun addAll(list: List<M>) {
        for (item: M in list) {
            add(item)
        }
        notifyDataSetChanged()
    }

    fun remove(item: M) {
        val position = list.indexOf(item)
        if (position > -1) {
            list.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun clear() {
        list.clear()
        notifyDataSetChanged()
    }
}