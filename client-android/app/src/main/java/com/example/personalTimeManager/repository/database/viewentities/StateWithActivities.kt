package com.example.personalTimeManager.repository.database.viewentities

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import com.example.personalTimeManager.repository.database.entity.Activity
import com.example.personalTimeManager.repository.database.entity.ActivityState
import com.example.personalTimeManager.repository.database.entity.State


data class StateWithActivities(
    @Embedded val state: State,
    @Relation(
        parentColumn = "id",
        entityColumn = "id",
        associateBy = Junction(
            value = ActivityState::class,
            parentColumn = "stateId",
            entityColumn = "activityId"
        )
    ) val activities: List<Activity>
)


/*
data class StateWithActivities(
    @Embedded val state: State,
    @Relation(
        parentColumn = "id",
        entityColumn = "activityId",
        entity = State::class,
        associateBy = Junction(
            value = ActivityState::class,
            parentColumn = "stateId",
            entityColumn = "activityId"
        )
    ) val activities: List<Activity>
)*/
