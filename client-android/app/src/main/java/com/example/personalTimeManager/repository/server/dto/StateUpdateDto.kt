package com.example.personalTimeManager.repository.server.dto

import com.example.personalTimeManager.repository.CustomColor
import com.google.gson.annotations.SerializedName

data class StateUpdateDto(
    @SerializedName("name")
    val name: String?,
    @SerializedName("description")
    val description: String?,
    @SerializedName("color")
    val color: CustomColor?
)