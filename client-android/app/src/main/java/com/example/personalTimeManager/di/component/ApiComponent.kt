package com.example.personalTimeManager.di.component

import com.example.personalTimeManager.di.module.ApiModule
import com.example.personalTimeManager.di.scope.ApiScope
import com.example.personalTimeManager.repository.server.ServerCommunicator
import dagger.Component

@ApiScope
@Component(modules = [ApiModule::class], dependencies = [UserComponent::class])
interface ApiComponent {
    val serverCommunicator: ServerCommunicator
}