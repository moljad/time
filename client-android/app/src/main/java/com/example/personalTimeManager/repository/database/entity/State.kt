package com.example.personalTimeManager.repository.database.entity

import androidx.room.*
import com.example.personalTimeManager.R
import com.example.personalTimeManager.repository.CustomColor
import com.example.personalTimeManager.repository.database.Status
import com.example.personalTimeManager.repository.database.converter.ColorConverters
import com.example.personalTimeManager.repository.database.converter.DateConverters
import com.example.personalTimeManager.repository.server.dto.StateCreateDto
import com.example.personalTimeManager.repository.server.dto.StateUpdateDto
import com.example.personalTimeManager.repository.server.gson.Exclude
import com.google.gson.annotations.SerializedName
import java.time.LocalDateTime

@Entity(
    tableName = "States",
    foreignKeys = [ForeignKey(
        entity = UserEntity::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("userId"),
        onUpdate = ForeignKey.CASCADE,
        onDelete = ForeignKey.CASCADE
    )]
)
data class State(
    @PrimaryKey
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("description")
    val description: String,
    @TypeConverters(ColorConverters::class)
    @SerializedName("color")
    val color: CustomColor,
    @ColumnInfo(name = "userId", index = true)
    @SerializedName("userId")
    val userId: Int,
    @TypeConverters(DateConverters::class)
    @SerializedName("createdDate")
    val createdDate: LocalDateTime,
    @TypeConverters(DateConverters::class)
    @SerializedName("updatedDate")
    val updatedDate: LocalDateTime,
    @Exclude
    val status: Status = Status.UNKNOWN
) {
    @Suppress("unused")
    private constructor() : this(
        -1073741824, "", "", CustomColor(R.color.colorAccent),
        -1073741824, LocalDateTime.now(), LocalDateTime.now()
    )

    @Exclude
    @delegate:Exclude
    @delegate:Ignore
    val stateCreateDto by lazy {
        StateCreateDto(
            this.name, this.description, this.color
        )
    }

    @Exclude
    @delegate:Exclude
    @delegate:Ignore
    val stateUpdateDto by lazy {
        StateUpdateDto(
            this.name, this.description, this.color
        )
    }
}