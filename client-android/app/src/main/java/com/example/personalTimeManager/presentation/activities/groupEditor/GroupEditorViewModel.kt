package com.example.personalTimeManager.presentation.activities.groupEditor

import android.app.Application
import com.example.personalTimeManager.presentation.base.BaseViewModel
import com.example.personalTimeManager.repository.AppRepository

class GroupEditorViewModel(application: Application, private val repository: AppRepository) : BaseViewModel(application)
