package com.example.personalTimeManager.repository.database.converter

import androidx.room.TypeConverter
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

object DateConverters {
    val dateTimeFormatter: DateTimeFormatter =
        DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SS")

    @TypeConverter
    @JvmStatic
    fun dateFromString(value: String): LocalDateTime {
        return LocalDateTime.parse(value, dateTimeFormatter)
    }

    @TypeConverter
    @JvmStatic
    fun dateToString(value: LocalDateTime): String {
        return value.format(dateTimeFormatter)
    }
}