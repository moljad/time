package com.example.personalTimeManager.repository.service

import android.content.res.Resources
import android.util.Log
import com.example.personalTimeManager.repository.database.AppDatabase
import com.example.personalTimeManager.repository.database.Status
import com.example.personalTimeManager.repository.database.entity.Activity
import com.example.personalTimeManager.repository.database.entity.ActivityState
import com.example.personalTimeManager.repository.database.entity.State
import com.example.personalTimeManager.repository.server.ServerCommunicator
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.rxkotlin.zipWith
import retrofit2.HttpException
import java.io.IOException
import java.util.*
import java.util.concurrent.TimeoutException
import javax.inject.Inject

class ActivityStateRepositoryService @Inject constructor(
    private val serverCommunicator: ServerCommunicator,
    private val activityRepositoryService: ActivityRepositoryService,
    private val stateRepositoryService: StateRepositoryService,
    private val mainDatabase: AppDatabase
) {

    fun connectActivityAndStateByIds(
        userId: Int,
        activityId: Int,
        stateId: Int
    ): Single<ActivityState> {
        val localActivityCheck = activityRepositoryService.getLocalActivity(activityId)
            .switchIfEmpty(activityRepositoryService.getActivityByUserIdAndId(userId, activityId))
            .toSingle()
        val localStateCheck = stateRepositoryService.getLocalState(stateId)
            .switchIfEmpty(stateRepositoryService.getStateByUserIdAndId(userId, stateId))
            .toSingle()

        val zipWith = localActivityCheck.zipWith(localStateCheck).onErrorResumeNext {
            Single.error<Pair<Activity, State>>(Resources.NotFoundException("Can not connect entities, that do not exist"))
        }

        return getLocalActivityState(activityId, stateId).map {
            if (it.status == Status.LOCAL_TO_DELETE) {
                return@map it.copy(status = Status.LOCAL)
            }
            return@map it
        }.switchIfEmpty(zipWith.flatMap {
            createAndGetRemoteActivityState(
                userId, activityId, stateId
            ).onErrorResumeNext { t ->
                if (t is HttpException && t.code() == 500) {
                    //
                }
                if (t is IOException || t is TimeoutException) {
                    return@onErrorResumeNext Single.just(
                        ActivityState(
                            activityId, stateId, Status.LOCAL_TO_CREATE
                        )
                    )
                }
                return@onErrorResumeNext Single.error<ActivityState>(t)
            }
        }).flatMap { createAndGetLocalActivityState(it) }
    }

    private fun createAndGetRemoteActivityState(
        userId: Int,
        activityId: Int,
        stateId: Int
    ): Single<ActivityState> {
        return createRemoteActivityState(userId, activityId, stateId).andThen(
            getRemoteActivityState(userId, activityId, stateId)
                .filter { it.isPresent }.map { it.get() })
            .toSingle()
    }

    private fun createRemoteActivityState(userId: Int, activityId: Int, stateId: Int): Completable {
        return serverCommunicator.createActivityState(userId, activityId, stateId)
    }

    private fun getRemoteActivityState(
        userId: Int,
        activityId: Int,
        stateId: Int
    ): Maybe<Optional<ActivityState>> {
        return getRemoteActivityStatesByActivityId(userId, activityId)
            .flatMapMaybe { optionalList ->
                if (!optionalList.isPresent) {
                    return@flatMapMaybe Maybe.just(Optional.empty<ActivityState>())
                }
                val foundActivityStates = optionalList.get().filter { it.stateId == stateId }
                if (foundActivityStates.isEmpty()) {
                    return@flatMapMaybe Maybe.empty<Optional<ActivityState>>()
                }
                return@flatMapMaybe Maybe.just(Optional.of(foundActivityStates.first()))
            }
    }

    private fun getRemoteActivityStatesByActivityId(
        userId: Int,
        activityId: Int
    ): Single<Optional<List<ActivityState>>> {
        return serverCommunicator.getActivityStates(userId, activityId)
            .map { listDto ->
                return@map listDto.map { it.activityState }
            }.map { Optional.of(it) }
            .onErrorResumeNext { t: Throwable ->
                if (t is IOException || t is TimeoutException) {
                    Log.e("AppRepository", t.message!!)
                    return@onErrorResumeNext Single.just(Optional.empty<List<ActivityState>>())
                }
                Single.error(t)
            }
    }

    private fun createAndGetLocalActivityState(activityState: ActivityState): Single<ActivityState> {
        return createOrReplaceLocalActivityState(activityState).andThen(
            getLocalActivityState(activityState.activityId, activityState.stateId)
        ).toSingle()
    }

    private fun getLocalActivityState(activityId: Int, stateId: Int): Maybe<ActivityState> {
        return mainDatabase.activityStateDao()
            .getActivityStateByActivityAndStateIds(activityId, stateId)
    }

    private fun createOrReplaceLocalActivityState(activityState: ActivityState): Completable {
        return Completable.fromCallable {
            mainDatabase.activityStateDao().upsert(activityState)
        }
    }

    fun getActivityStatesByActivityId(userId: Int, activityId: Int): Single<List<ActivityState>> {
        val remoteActivityStates =
            synchronizeActivityStatesAndGetThemByActivityId(userId, activityId)
        val localActivityStates = getLocalActivityStatesByActivityId(activityId)

        val zipWith = remoteActivityStates.zipWith(localActivityStates)
        return zipWith.flatMap { (remoteActivityStates, localActivityStates) ->
            if (!remoteActivityStates.isPresent) {
                Log.i("AppRepository", "returning local activities instead")
                return@flatMap createOrReplaceLocalActivityStates(
                    markAsLocalIfNotDeferred(
                        localActivityStates
                    )
                ).andThen(Single.just(markAsLocalIfNotDeferred(localActivityStates)))
            }
            return@flatMap Single.just(remoteActivityStates.get())
        }
    }

    private fun markAsLocalIfNotDeferred(activityStates: List<ActivityState>): List<ActivityState> {
        return activityStates.map {
            markAsLocalIfNotDeferred(it)
        }
    }

    private fun markAsLocalIfNotDeferred(activityState: ActivityState): ActivityState {
        if (activityState.status != Status.LOCAL_TO_CREATE
            && activityState.status != Status.LOCAL_TO_UPDATE
            && activityState.status != Status.LOCAL_TO_DELETE
        ) {
            return activityState.copy(status = Status.LOCAL)
        }
        return activityState
    }

    private fun synchronizeActivityStatesAndGetThemByActivityId(
        userId: Int,
        activityId: Int
    ): Single<Optional<List<ActivityState>>> {
        val activitySynchronization =
            activityRepositoryService.synchronizeActivitiesAndGetThemByUserId(userId)
                .ignoreElement()
        val stateSynchronization =
            stateRepositoryService.synchronizeStatesAndGetThemByUserId(userId)
                .ignoreElement()
        val activitiesAndStatesSynchronization =
            Completable.mergeArrayDelayError(activitySynchronization, stateSynchronization)
        return activitiesAndStatesSynchronization.andThen(synchronizeActivityStates(
            userId, activityId
        ).andThen(
            getLocalActivityStatesByActivityId(activityId)
        ).map { Optional.of(it) }.onErrorResumeNext { t ->
            if (t is IOException || t is TimeoutException) {
                return@onErrorResumeNext Single.just(Optional.empty<List<ActivityState>>())
            }
            Single.error<Optional<List<ActivityState>>>(t)
        })
    }

    private fun synchronizeActivityStates(userId: Int, activityId: Int): Completable {
        return getLocalActivityStatesByActivityId(activityId).flatMapCompletable { localActivityStates ->
            val synchronizeToCreate =
                synchronizeDeferredToCreateActivityStates(userId, localActivityStates)
            val synchronizeToDelete =
                this.synchronizeDeferredToDeleteActivityStates(userId, localActivityStates)

            Completable.mergeArrayDelayError(
                synchronizeToCreate, synchronizeToDelete
            ).andThen(getRemotesAndRefreshActivityStates(userId, activityId, localActivityStates))
        }
    }

    private fun synchronizeDeferredToCreateActivityStates(
        userId: Int,
        localActivityStates: List<ActivityState>
    ): Completable {
        val taskList = ArrayList<Completable>()
        localActivityStates.forEach { localActivityState ->
            if (localActivityState.status == Status.LOCAL_TO_CREATE) {
                val createRemoteActivityState =
                    synchronizeDeferredToCreateActivity(userId, localActivityState)
                taskList.add(createRemoteActivityState)
            }
        }
        val params = taskList.toTypedArray()
        return Completable.mergeArrayDelayError(*params)
    }

    private fun synchronizeDeferredToCreateActivity(
        userId: Int,
        localActivityState: ActivityState
    ): Completable {
        return createAndGetRemoteActivityState(
            userId,
            localActivityState.activityId,
            localActivityState.stateId
        )
            .flatMapCompletable { newActivityState ->
                createOrReplaceLocalActivityState(newActivityState)
            }.onErrorResumeNext { t ->
                if (t is HttpException && t.code() == 500) {
                    return@onErrorResumeNext deleteLocalActivityState(localActivityState)
                }
                if (t is IOException || t is TimeoutException) {
                    return@onErrorResumeNext Completable.complete()
                }
                return@onErrorResumeNext Completable.error(t)
            }
    }

    private fun synchronizeDeferredToDeleteActivityStates(
        userId: Int,
        localActivityStates: List<ActivityState>
    ): Completable {
        val taskList = ArrayList<Completable>()
        localActivityStates.forEach { localActivityState ->
            if (localActivityState.status == Status.LOCAL_TO_DELETE) {
                val deleteSynchronizationTask =
                    synchronizeDeferredToDeleteActivityState(userId, localActivityState)
                taskList.add(deleteSynchronizationTask)
            }
        }
        val params = taskList.toTypedArray()
        return Completable.mergeArrayDelayError(*params)
    }

    private fun synchronizeDeferredToDeleteActivityState(
        userId: Int,
        localActivityState: ActivityState
    ): Completable {
        return disconnectActivityAndStateByIds(
            userId,
            localActivityState.activityId,
            localActivityState.stateId
        )
            .onErrorResumeNext { t ->
                if (t is HttpException && t.code() == 500) {
                    return@onErrorResumeNext deleteLocalActivityState(localActivityState)
                }
                if (t is IOException || t is TimeoutException) {
                    return@onErrorResumeNext Completable.complete()
                }
                return@onErrorResumeNext Completable.error(t)
            }
    }

    private fun getRemotesAndRefreshActivityStates(
        userId: Int,
        activityId: Int,
        localActivityStates: List<ActivityState>
    ): Completable {
        return getRemoteActivityStatesByActivityId(
            userId,
            activityId
        ).flatMapCompletable { listOptional ->
            if (listOptional.isPresent) {
                val remoteActivityStates = listOptional.get()
                return@flatMapCompletable removeRedundantActivityStates(
                    localActivityStates,
                    remoteActivityStates
                ).andThen(
                    createOrReplaceLocalActivityStates(remoteActivityStates)
                )
            }
            return@flatMapCompletable Completable.error(IOException("Could not receive remote entities"))
        }
    }

    private fun getLocalActivityStatesByActivityId(activityId: Int): Single<List<ActivityState>> {
        return mainDatabase.activityStateDao()
            .getActivityStatesByActivityId(activityId)
    }

    private fun refreshLocalActivityStatesAndGetThemByActivityId(
        activityStates: List<ActivityState>,
        activityId: Int
    ): Single<List<ActivityState>> {
        getLocalActivityStatesByActivityId(activityId).flatMapCompletable {
            removeRedundantActivityStates(it, activityStates)
        }.subscribe()

        return createOrReplaceLocalActivityStates(activityStates).andThen(
            getLocalActivityStatesByActivityId(activityId)
        )
    }

    private fun removeRedundantActivityStates(
        localActivityStates: List<ActivityState>,
        actualActivityStates: List<ActivityState>
    ): Completable {
        val redundant = localActivityStates.subtract(actualActivityStates).toList()
        if (redundant.isNotEmpty()) {
            return deleteLocalActivityStates(redundant)
        }
        return Completable.complete()
    }

    private fun deleteLocalActivityStates(list: List<ActivityState>): Completable {
        return mainDatabase.activityStateDao().deleteList(list)
    }


    private fun createOrReplaceLocalActivityStates(activityStates: List<ActivityState>): Completable {
        return Completable.fromCallable {
            mainDatabase.activityStateDao().upsertList(activityStates)
        }
    }

    fun disconnectActivityAndStateByIds(userId: Int, activityId: Int, stateId: Int): Completable {
        val localActivityState = getLocalActivityState(activityId, stateId).switchIfEmpty(
            getRemoteActivityState(
                userId, activityId, stateId
            ).filter { it.isPresent }.map { it.get() }
        )

        return localActivityState.isEmpty.flatMapCompletable { isLocalActivityEmpty ->
            if (isLocalActivityEmpty) { //just send a request with current user
                return@flatMapCompletable Completable.complete()
            }
            localActivityState.flatMapSingle { createAndGetLocalActivityState(it) }
                .flatMapCompletable(deleteExistingActivity(userId))
        }
    }

    private fun deleteExistingActivity(userId: Int): (ActivityState) -> Completable {
        return completable@{ localActivityState ->
            if (localActivityState.status == Status.LOCAL_TO_CREATE) {
                return@completable deleteLocalActivityState(localActivityState)
            }
            return@completable deleteRemoteActivityState(
                userId, localActivityState.activityId, localActivityState.stateId
            ).andThen(deleteLocalActivityState(localActivityState)).onErrorResumeNext { t ->
                if (t is IOException || t is TimeoutException) {
                    return@onErrorResumeNext createOrReplaceLocalActivityState(
                        localActivityState.copy(
                            status = Status.LOCAL_TO_DELETE
                        )
                    )
                }
                if (t is HttpException && t.code() == 500) {
                    return@onErrorResumeNext deleteLocalActivityState(localActivityState)
                }
                return@onErrorResumeNext Completable.error(t)
            }
        }
    }

    private fun deleteRemoteActivityState(userId: Int, activityId: Int, stateId: Int): Completable {
        return serverCommunicator.deleteActivityState(userId, activityId, stateId)
    }

    private fun deleteLocalActivityStateByIds(activityId: Int, stateId: Int): Completable {
        return getLocalActivityState(activityId, stateId).flatMapCompletable {
            deleteLocalActivityState(it)
        }
    }

    private fun deleteLocalActivityState(activityState: ActivityState): Completable {
        return mainDatabase.activityStateDao().delete(activityState)
    }
}