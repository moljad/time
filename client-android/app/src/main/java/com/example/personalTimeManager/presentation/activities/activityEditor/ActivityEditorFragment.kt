package com.example.personalTimeManager.presentation.activities.activityEditor

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.Switch
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.personalTimeManager.R
import com.example.personalTimeManager.databinding.FragmentActivityEditorBinding
import com.example.personalTimeManager.di.component.ViewModelComponent
import com.example.personalTimeManager.presentation.base.BaseAdapter
import com.example.personalTimeManager.presentation.base.BaseFragment
import com.example.personalTimeManager.repository.CustomColor
import com.example.personalTimeManager.repository.database.Status
import com.example.personalTimeManager.repository.database.entity.ActivityState
import com.example.personalTimeManager.repository.server.dto.ActivityCreateDto
import com.example.personalTimeManager.repository.server.dto.ActivityUpdateDto
import com.example.personalTimeManager.util.fromSpanFormat
import com.example.personalTimeManager.util.toSpanFormat
import com.google.android.material.floatingactionbutton.FloatingActionButton
import dev.sasikanth.colorsheet.ColorSheet
import io.reactivex.Completable
import io.reactivex.Completable.mergeArrayDelayError
import io.reactivex.rxkotlin.zipWith
import javax.inject.Inject


class ActivityEditorFragment : BaseFragment() {
    lateinit var viewModel: ActivityEditorViewModel
        @Inject set

    lateinit var adapter: BaseAdapter<ActivityEditorHolder, ActivityStateElement>

    var activityId: Int = 0
    var defaultColor: Int = 0

    override fun injectDependency(component: ViewModelComponent) {
        component.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentActivityEditorBinding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_activity_editor, container, false
            )
        binding.fragment = this
        binding.lifecycleOwner = this

        val view = binding.root

        val fab: FloatingActionButton = container!!.rootView.findViewById(R.id.fab)
        fab.hide()

        val recyclerView: RecyclerView = view.findViewById(R.id.selectedStates)

        adapter = object :
            BaseAdapter<ActivityEditorHolder, ActivityStateElement>(context!!, mutableListOf()) {
            override fun onBindViewHolder(holder: ActivityEditorHolder, position: Int) {
                val state = list[position]

                holder.setName(state.name)
                holder.setChecked(state.isEnabled)
            }

            override fun onCreateViewHolder(
                parent: ViewGroup,
                viewType: Int
            ): ActivityEditorHolder {
                val recycleView =
                    inflater.inflate(R.layout.recycle_activity_state_item, parent, false)

                val checkbox: CheckBox = recycleView.findViewById(R.id.activityStateCheckbox)
                val recyclerViewListener = View.OnClickListener { v ->
                    val position = recyclerView.getChildAdapterPosition(v)
                    val data: ActivityStateElement = getItem(position)
                    checkbox.isChecked = !checkbox.isChecked
                    Log.e("sdf", checkbox.isChecked.toString())
                    data.isEnabled = checkbox.isChecked
                }

                checkbox.setOnClickListener {
                    recyclerViewListener.onClick(recycleView)
                }

                recycleView.setOnClickListener(recyclerViewListener)

                return ActivityEditorHolder(recycleView)
            }
        }

        recyclerView.adapter = adapter

        val updateButtonView: Button = view.findViewById(R.id.update)
        updateButtonView.setOnClickListener(this::onUpdateButtonPressed)

        val deleteButtonView: Button = view.findViewById(R.id.delete)
        deleteButtonView.setOnClickListener(this::onDeleteButtonPressed)

        val createButtonView: Button = view.findViewById(R.id.create)
        createButtonView.setOnClickListener(this::onCreateButtonPressed)

        val colors = resources.getIntArray(R.array.entityColors)

        defaultColor = colors[0]

        val changeColor: View = view.findViewById(R.id.changeColor)
        changeColor.setOnClickListener {
            ColorSheet().colorPicker(colors,
                listener = {
                    viewModel.activity.value!!.color = it
                    viewModel.activity.postValue(viewModel.activity.value)
                    Log.e(
                        it.toString(),
                        CustomColor(it).stringColor + " " + CustomColor(it).intColor
                    )
                }
            ).show(activity!!.supportFragmentManager)
        }

        val isRestSwitchText: TextView = view.findViewById(R.id.isRestText)
        isRestSwitchText.setOnClickListener {
            onIsRestSwitchTextPressed(view)
        }

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        activityId = ActivityEditorFragmentArgs.fromBundle(arguments!!).activityId

        Log.e("a", activityId.toString())

        if (activityId != 0) {
            viewModel.getActivity(activityId).subscribe {
                viewModel.activity.postValue(
                    ActivityEditorViewModel.ActivityModel(
                        activityId,
                        it.name,
                        it.description,
                        it.priority,
                        it.isRest,
                        it.link,
                        it.lengthMin.fromSpanFormat(),
                        it.lengthMax.fromSpanFormat(),
                        it.lengthRequired.fromSpanFormat(),
                        it.color.intColor
                    )
                )
            }
            viewModel.getStates().zipWith(viewModel.getActivityStates(activityId))
                .subscribe({
                    viewModel.oldActivityStates = it.second

                    val connected = it.second
                        .filter { activityState -> activityState.activityId == activityId }
                        .filter { activityState -> activityState.status != Status.LOCAL_TO_DELETE }
                        .map(ActivityState::stateId).toSet()

                    val elements = it.first.map {
                        ActivityStateElement(
                            it.id,
                            it.name,
                            connected.contains(it.id)
                        )
                    }

                    Log.e("sd", elements.toString())
                    adapter.clear()
                    adapter.addAll(elements)
                }, {})
        } else {
            viewModel.activity.postValue(
                ActivityEditorViewModel.ActivityModel(
                    activityId,
                    "",
                    "",
                    1,
                    false,
                    "",
                    3.fromSpanFormat(),
                    6.fromSpanFormat(),
                    0.fromSpanFormat(),
                    defaultColor
                )
            )
            viewModel.getStates()
                .subscribe({
                    val elements = it.map {
                        ActivityStateElement(
                            it.id,
                            it.name,
                            false
                        )
                    }

                    adapter.clear()
                    adapter.addAll(elements)
                },
                    {})
        }

    }

    fun onUpdateButtonPressed(v: View) {
        val value = viewModel.activity.value!!

        val activityUpdateDto = ActivityUpdateDto(
            value.name,
            value.description,
            value.priority,
            value.isRest,
            value.link,
            value.lengthMin.toSpanFormat(),
            value.lengthMax.toSpanFormat(),
            value.lengthRequired.toSpanFormat(),
            CustomColor(value.color)
        )

        viewModel.updateActivity(viewModel.activity.value?.id!!, activityUpdateDto)
            .flatMapCompletable { UpdateLinks() }.subscribe({
                findNavController().popBackStack()
            }, {
                throw it
            })
    }

    fun onDeleteButtonPressed(v: View) {
        viewModel.deleteActivity(viewModel.activity.value?.id!!).subscribe {
            findNavController().popBackStack()
        }
    }

    fun onCreateButtonPressed(v: View) {
        val value = viewModel.activity.value!!

        val activityCreateDto = ActivityCreateDto(
            value.name,
            value.description,
            value.priority,
            value.isRest,
            value.link,
            value.lengthMin.toSpanFormat(),
            value.lengthMax.toSpanFormat(),
            value.lengthRequired.toSpanFormat(),
            CustomColor(value.color)
        )

        viewModel.createActivity(activityCreateDto)
            .flatMapCompletable { UpdateLinks(it.id) }.subscribe({
                findNavController().popBackStack()
            }, {
                throw it
            })
    }

    fun UpdateLinks(id: Int? = null): Completable {
        if (id != null) {
            activityId = id
        }

        val connected = viewModel.oldActivityStates
            .filter { activityState -> activityState.activityId == activityId }
            .filter { activityState -> activityState.status != Status.LOCAL_TO_DELETE }
            .map(ActivityState::stateId).toSet()

        val elements = adapter.getItems()

        val actions = elements
            .filter {
                connected.contains(it.id) != it.isEnabled
            }
            .map {
                return@map if (it.isEnabled) {
                    viewModel.connectActivityState(activityId, it.id).ignoreElement()
                } else {
                    viewModel.disconnectActivityState(activityId, it.id)
                }
            }.toTypedArray()

        Log.e("!actions", elements.filter(ActivityStateElement::isEnabled).size.toString())

        return mergeArrayDelayError(*actions)
    }

    fun onIsRestSwitchTextPressed(v: View) {
        val switch: Switch = v.findViewById(R.id.isRest)
        switch.isChecked = !switch.isChecked
    }
}
