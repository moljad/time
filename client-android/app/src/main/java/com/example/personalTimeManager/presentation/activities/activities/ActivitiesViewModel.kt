package com.example.personalTimeManager.presentation.activities.activities

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.example.personalTimeManager.presentation.base.BaseViewModel
import com.example.personalTimeManager.repository.AppRepository
import com.example.personalTimeManager.repository.database.entity.Activity
import io.reactivex.Single

class ActivitiesViewModel(application: Application, private val repository: AppRepository) :
    BaseViewModel(application) {
    val activities: MutableLiveData<List<Activity>> = MutableLiveData(listOf())

    fun getActivities(): Single<List<Activity>> {
        return repository.getActivitiesByUserId()
    }
}