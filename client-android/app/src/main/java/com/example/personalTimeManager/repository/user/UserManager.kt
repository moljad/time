package com.example.personalTimeManager.repository.user

import android.content.res.Resources
import com.example.personalTimeManager.repository.database.AppDatabase
import com.example.personalTimeManager.repository.database.entity.UserEntity
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.zipWith
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class UserManager(private val appDatabase: AppDatabase) {

    companion object {
        private const val DEFAULT_TIMEOUT = 5
        private const val DEFAULT_RETRY_ATTEMPTS = 1L
    }

    fun getCurrentUser(): Maybe<UserEntity> {
        val all = appDatabase.userDao().getAll().compose(singleTransformer())
        return all.flatMapMaybe { list ->
            val activeUsers = list.filter { user -> user.isCurrentUser }

            val realActualUser =
                activeUsers.maxBy { it.id ?: -1 }  //well i'll just return one with most id
                    ?: return@flatMapMaybe Maybe.empty<UserEntity>()

            /*to make only 1 user to be actual and return it, it makes others to be unactual*/
            val fakeActualUsers = activeUsers
                .filterNot { fakeActualUser -> fakeActualUser == realActualUser }

            unactualUsers(fakeActualUsers).subscribe()  //in background

            return@flatMapMaybe Maybe.just(realActualUser).compose(maybeTransformer())
        }
    }

    fun unactualAllUsers(): Completable {
        val all = appDatabase.userDao().getAll()
        return all.flatMapCompletable { list ->
            unactualUsers(list)
        }.compose(completableTransformer())
    }

    fun unactualUsers(list: List<UserEntity>): Completable {
        val unactualizedUsers = list.map { user ->
            user.copy(isCurrentUser = false)
        }
        return updateLocalUsers(unactualizedUsers)
    }

    private fun updateLocalUsers(unactualizedUsers: List<UserEntity>): Completable {
        return appDatabase.userDao().updateList(unactualizedUsers)
    }

    fun updateUser(userEntity: UserEntity): Completable {
        return updateLocalUser(userEntity).compose(completableTransformer())
    }

    fun areUsersExists(): Single<Boolean> {
        val all = appDatabase.userDao().getAll()
        return all.map { list -> list.isNotEmpty() }.compose(singleTransformer())
    }

    fun createLocalActualUser(email: String, password: String): Completable {
        return unactualAllUsers().andThen(
            createOrReplaceUser(
                UserEntity(
                    null,
                    null,
                    email,
                    password,
                    true
                )
            )
        ).compose(completableTransformer())
    }

    private fun createOrReplaceUser(userEntity: UserEntity): Completable {
        return Completable.fromCallable { appDatabase.userDao().upsert(userEntity) }
    }


    fun makeExistingUserActual(email: String): Completable {
        val userByEmailOrError =
            appDatabase.userDao().getByEmail(email).toSingle()
                .onErrorResumeNext(Single.error(Resources.NotFoundException()))
        val unactualUserTask = unactualAllUsers().toSingleDefault(Unit)
        val allUsersAreUnactualAndUserFound = unactualUserTask.zipWith(userByEmailOrError)


        return allUsersAreUnactualAndUserFound.flatMapCompletable { (_, userToBeUpdated) ->
            val actualUser = userToBeUpdated.copy(isCurrentUser = true)
            updateLocalUser(actualUser)
        }.compose(completableTransformer())
    }

    private fun updateLocalUser(actualUser: UserEntity): Completable {
        return appDatabase.userDao().update(actualUser)
    }


    private fun completableTransformer(): CompletableTransformer = CompletableTransformer {
        it.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .timeout(DEFAULT_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .retry(DEFAULT_RETRY_ATTEMPTS)
    }

    private fun <T> singleTransformer(): SingleTransformer<T, T> = SingleTransformer {
        it.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .timeout(DEFAULT_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .retry(DEFAULT_RETRY_ATTEMPTS)
    }

    private fun <T> maybeTransformer(): MaybeTransformer<T, T> = MaybeTransformer {
        it.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .retry(DEFAULT_RETRY_ATTEMPTS)
            .timeout(DEFAULT_TIMEOUT.toLong(), TimeUnit.SECONDS)
    }
}