package com.example.personalTimeManager.presentation.activities.scheduleEditor

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.example.personalTimeManager.presentation.base.BaseViewModel
import com.example.personalTimeManager.repository.AppRepository
import com.example.personalTimeManager.repository.database.entity.Schedule
import io.reactivex.Single

class ScheduleEditorViewModel(application: Application, var repository: AppRepository) :
    BaseViewModel(application) {
    val schedule: MutableLiveData<Schedule> = MutableLiveData()

    fun getSchedule(): Single<Schedule> {
        return repository.getScheduleByUserId(null)
    }
}
