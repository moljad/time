package com.example.personalTimeManager.repository.database.converter

import androidx.room.TypeConverter
import com.example.personalTimeManager.repository.database.Status


object StatusConverters {

    @TypeConverter
    @JvmStatic
    fun statusFromString(value: String): Status {
        return enumValueOf(value)
    }

    @TypeConverter
    @JvmStatic
    fun statusToString(value: Status): String {
        return value.name
    }
}