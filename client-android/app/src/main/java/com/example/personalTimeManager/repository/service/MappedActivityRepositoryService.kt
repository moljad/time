package com.example.personalTimeManager.repository.service

import android.content.res.Resources
import android.util.Log
import com.example.personalTimeManager.repository.database.AppDatabase
import com.example.personalTimeManager.repository.database.Status
import com.example.personalTimeManager.repository.database.converter.DateConverters
import com.example.personalTimeManager.repository.database.converter.StatusConverters
import com.example.personalTimeManager.repository.database.entity.MappedActivity
import com.example.personalTimeManager.repository.server.ServerCommunicator
import com.example.personalTimeManager.repository.server.dto.MappedActivityCreateDto
import com.example.personalTimeManager.repository.server.dto.MappedActivityUpdateDto
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.MaybeSource
import io.reactivex.Single
import io.reactivex.rxkotlin.zipWith
import retrofit2.HttpException
import java.io.IOException
import java.time.LocalDateTime
import java.util.*
import java.util.concurrent.TimeoutException
import javax.inject.Inject
import kotlin.collections.ArrayList

class MappedActivityRepositoryService @Inject constructor(
    private val userRepositoryService: UserRepositoryService,
    private val activityRepositoryService: ActivityRepositoryService,
    private val scheduleRepositoryService: ScheduleRepositoryService,
    private val serverCommunicator: ServerCommunicator,
    private val mainDatabase: AppDatabase
) {

    /**gets from local mappedActivity.
     * If is not found, then tries to synchronize it with current user and get it again**/
    fun guessUserIdByMappedActivityId(id: Int): Maybe<Int> {
        return getLocalMappedActivity(id)
            .switchIfEmpty(MaybeSource<MappedActivity> { maybeObserver ->
                userRepositoryService.guessCurrentUser()
                    .flatMapMaybe { guessedUser ->
                        getMappedActivityByUserIdAndId(guessedUser.id!!, id).map {
                            maybeObserver.onSuccess(it)
                        }
                    }
                //maybeSource.onErrorReturn { maybeObserver.onError(it) }
                //maybeSource.isEmpty.filter { it }.doOnComplete {  }
                //maybeSource.doOnComplete {  }
            }).flatMap {
                activityRepositoryService.guessUserIdByActivityId(it.activityId)
            }.onErrorResumeNext { _: Throwable -> return@onErrorResumeNext Maybe.empty<Int>() }
    }

    fun getMappedActivitiesByUserId(userId: Int): Single<List<MappedActivity>> {
        val refreshedMappedActivities = synchronizeMappedActivitiesAndGetThemByUserId(userId)
        val localMappedActivities = getLocalMappedActivitiesByUserId(userId)

        return refreshedMappedActivities.zipWith(localMappedActivities)
            .flatMap { (refreshedMappedActivities, localMappedActivities) ->
                if (!refreshedMappedActivities.isPresent) {
                    Log.i("AppRepository", "returning local mappedActivities instead")
                    val markedLocalMappedActivities =
                        markAsLocalIfNotDeferred(localMappedActivities)
                    return@flatMap createOrReplaceLocalMappedActivities(markedLocalMappedActivities).andThen(
                        Single.just(markedLocalMappedActivities)
                    )
                }
                return@flatMap Single.just(refreshedMappedActivities.get())
            }
    }

    private fun markAsLocalIfNotDeferred(mappedActivities: List<MappedActivity>): List<MappedActivity> {
        return mappedActivities.map {
            markAsLocalIfNotDeferred(it)
        }
    }

    private fun markAsLocalIfNotDeferred(mappedActivity: MappedActivity): MappedActivity {
        if (mappedActivity.status != Status.LOCAL_TO_CREATE
            && mappedActivity.status != Status.LOCAL_TO_UPDATE
            && mappedActivity.status != Status.LOCAL_TO_DELETE
        ) {
            return mappedActivity.copy(status = Status.LOCAL)
        }
        return mappedActivity
    }


    private fun getLocalMappedActivitiesByUserId(userId: Int): Single<List<MappedActivity>> {
        return mainDatabase.mappedActivityDao().getByUserId(userId)
    }

    private fun synchronizeMappedActivitiesAndGetThemByUserId(
        userId: Int
    ): Single<Optional<List<MappedActivity>>> {
        return synchronizeMappedActivities(userId).andThen(
            getLocalMappedActivitiesByUserId(userId)
        ).map { Optional.of(it) }.onErrorResumeNext { t ->
            if (t is IOException || t is TimeoutException) {
                return@onErrorResumeNext Single.just(Optional.empty<List<MappedActivity>>())
            }
            Single.error<Optional<List<MappedActivity>>>(t)
        }
    }

    private fun synchronizeMappedActivities(userId: Int): Completable {
        return activityRepositoryService.synchronizeActivitiesAndGetThemByUserId(userId)
            .ignoreElement()
            .andThen(getLocalMappedActivitiesByUserId(userId).flatMapCompletable { localMappedActivities ->
                val synchronizeToCreate =
                    synchronizeDeferredToCreateMappedActivities(userId, localMappedActivities)
                val synchronizeToUpdate =
                    synchronizeDeferredToUpdateMappedActivities(userId, localMappedActivities)
                val synchronizeToDelete =
                    synchronizeDeferredToDeleteMappedActivities(userId, localMappedActivities)

                Completable.mergeArrayDelayError(
                    synchronizeToCreate, synchronizeToUpdate, synchronizeToDelete
                ).andThen(getRemotesAndRefreshMappedActivities(userId, localMappedActivities))
            })
    }

    private fun synchronizeDeferredToCreateMappedActivities(
        userId: Int,
        localMappedActivities: List<MappedActivity>
    ): Completable {
        val taskList = ArrayList<Completable>()
        localMappedActivities.forEach { localMappedActivity ->
            if (localMappedActivity.status == Status.LOCAL_TO_CREATE) {
                val createRemoteMappedActivity =
                    synchronizeDeferredToCreateMappedActivity(userId, localMappedActivity)
                taskList.add(createRemoteMappedActivity)
            }
        }
        val params = taskList.toTypedArray()
        return Completable.mergeArrayDelayError(*params)
    }

    private fun synchronizeDeferredToCreateMappedActivity(
        userId: Int,
        localMappedActivity: MappedActivity
    ): Completable {
        return createRemoteMappedActivity(
            userId,
            localMappedActivity.mappedActivityCreateDto
        ).flatMapCompletable { newMappedActivity ->
            val (id, span, activityId, scheduleId,
                createdDate, updatedDate, status) = newMappedActivity
            val stringCreateDate = DateConverters.dateToString(createdDate)
            val stringUpdatedDate = DateConverters.dateToString(updatedDate)
            val stringStatus = StatusConverters.statusToString(status)

            mainDatabase.mappedActivityDao().replace(
                localMappedActivity.id, id, span.begin, span.end, activityId, scheduleId,
                stringCreateDate, stringUpdatedDate, stringStatus
            )
        }.onErrorResumeNext { t ->
            if (t is HttpException && t.code() == 500) {
                return@onErrorResumeNext deleteLocalMappedActivity(localMappedActivity)
            }
            if (t is IOException || t is TimeoutException) {
                return@onErrorResumeNext Completable.complete()
            }
            return@onErrorResumeNext Completable.error(t)
        }
    }

    private fun synchronizeDeferredToUpdateMappedActivities(
        userId: Int,
        localMappedActivities: List<MappedActivity>
    ): Completable {
        val taskList = ArrayList<Completable>()
        localMappedActivities.forEach { localMappedActivity ->
            if (localMappedActivity.status == Status.LOCAL_TO_UPDATE) {
                val updateRemoteMappedActivity =
                    synchronizeDeferredToUpdateMappedActivity(userId, localMappedActivity)
                taskList.add(updateRemoteMappedActivity)
            }
        }
        val params = taskList.toTypedArray()
        return Completable.mergeArrayDelayError(*params)
    }

    private fun synchronizeDeferredToUpdateMappedActivity(
        userId: Int,
        localMappedActivity: MappedActivity
    ): Completable {
        return updateRemoteMappedActivity(
            userId,
            localMappedActivity.id,
            localMappedActivity.mappedActivityUpdateDto
        ).flatMapCompletable { Completable.complete() }
            .onErrorResumeNext { t ->
                if (t is HttpException && t.code() == 500) {
                    return@onErrorResumeNext getMappedActivityByUserIdAndId(
                        userId,
                        localMappedActivity.id
                    ).flatMapCompletable { Completable.complete() }
                }
                if (t is IOException || t is TimeoutException) {
                    return@onErrorResumeNext Completable.complete()
                }
                return@onErrorResumeNext Completable.error(t)
            }
    }

    private fun synchronizeDeferredToDeleteMappedActivities(
        userId: Int,
        localMappedActivities: List<MappedActivity>
    ): Completable {
        val taskList = ArrayList<Completable>()
        localMappedActivities.forEach { localMappedActivity ->
            if (localMappedActivity.status == Status.LOCAL_TO_DELETE) {
                val deleteSynchronizationTask =
                    synchronizeDeferredToDeleteMappedActivity(userId, localMappedActivity)
                taskList.add(deleteSynchronizationTask)
            }
        }
        val params = taskList.toTypedArray()
        return Completable.mergeArrayDelayError(*params)
    }

    private fun synchronizeDeferredToDeleteMappedActivity(
        userId: Int,
        localMappedActivity: MappedActivity
    ): Completable {
        return deleteRemoteMappedActivity(userId, localMappedActivity.id)
            .andThen(deleteLocalMappedActivity(localMappedActivity))
            .onErrorResumeNext { t ->
                if (t is HttpException && t.code() == 500) {
                    return@onErrorResumeNext getMappedActivityByUserIdAndId(
                        userId,
                        localMappedActivity.id
                    ).flatMapCompletable { Completable.complete() }
                }
                if (t is IOException || t is TimeoutException) {
                    return@onErrorResumeNext Completable.complete()
                }
                return@onErrorResumeNext Completable.error(t)
            }
    }

    private fun getRemotesAndRefreshMappedActivities(
        userId: Int,
        localMappedActivities: List<MappedActivity>
    ): Completable {
        return getRemoteMappedActivities(userId).flatMapCompletable { listOptional ->
            if (listOptional.isPresent) {
                val remoteMappedActivities = listOptional.get()
                return@flatMapCompletable removeRedundantMappedActivities(
                    localMappedActivities,
                    remoteMappedActivities
                ).andThen(
                    createOrReplaceLocalMappedActivities(remoteMappedActivities)
                )
            }
            return@flatMapCompletable Completable.error(IOException("Could not receive remote entities"))
        }
    }

    fun getRemoteMappedActivities(userId: Int): Single<Optional<List<MappedActivity>>> {
        return serverCommunicator.getMappedActivities(userId)
            .map { Optional.of(it.map { newMappedActivity -> newMappedActivity.copy(status = Status.SYNCHRONIZED) }) }
            .onErrorResumeNext { t ->
                if (t is IOException || t is TimeoutException) {
                    Log.e("AppRepository", t.message!!)
                    return@onErrorResumeNext Single.just(Optional.empty())
                }
                return@onErrorResumeNext Single.just(Optional.empty())
            }
    }

    private fun removeRedundantMappedActivities(
        localMappedActivities: List<MappedActivity>,
        actualMappedActivities: List<MappedActivity>
    ): Completable {
        val newIds = actualMappedActivities.map { it.id }
        val localIds = localMappedActivities.filter {
            it.status != Status.LOCAL_TO_CREATE
                    && it.status != Status.LOCAL_TO_UPDATE
                    && it.status != Status.LOCAL_TO_DELETE
        }.map { it.id }
        val redundantIds = localIds.subtract(newIds)

        val redundant = localMappedActivities.filter { it.id in redundantIds }
        if (redundant.isNotEmpty()) {
            return deleteMappedActivitiesLocally(redundant)
        }
        return Completable.complete()
    }

    private fun deleteMappedActivitiesLocally(list: List<MappedActivity>): Completable {
        return mainDatabase.mappedActivityDao().deleteList(list)
    }

    private fun createOrReplaceLocalMappedActivities(actualMappedActivities: List<MappedActivity>): Completable {
        return Completable.fromCallable {
            mainDatabase.mappedActivityDao().upsertList(actualMappedActivities)
        }
    }

    fun getMappedActivityByUserIdAndId(userId: Int, id: Int): Maybe<MappedActivity> {
        val communicatorMappedActivity =
            getRemoteMappedActivity(userId, id).map { Optional.of(it) }
                .defaultIfEmpty(Optional.empty())
        val localMappedActivity =
            getLocalMappedActivity(id).map { Optional.of(it) }.defaultIfEmpty(Optional.empty())

        return communicatorMappedActivity.zipWith(localMappedActivity)
            .flatMap { (communicatorMappedActivity, localMappedActivity) ->
                if (!communicatorMappedActivity.isPresent) {    //server doesn't have it, but local does
                    if (localMappedActivity.isPresent) {  //redundant local data
                        deleteRedundantMappedActivityAsync(localMappedActivity.get())
                    }
                    return@flatMap Maybe.empty<MappedActivity>()
                }
                if (communicatorMappedActivity.isPresent && communicatorMappedActivity.get().isPresent) { //ok. just refresh local data with this new value
                    val actualMappedActivity = communicatorMappedActivity.get().get()
                    return@flatMap createOrReplaceLocalMappedActivityAndGetIt(actualMappedActivity).toMaybe()
                }
                if (communicatorMappedActivity.isPresent && !communicatorMappedActivity.get().isPresent && localMappedActivity.isPresent) {
                    Log.i("AppRepository", "returning local mappedActivity instead")
                    val markedMappedActivity = markAsLocalIfNotDeferred(localMappedActivity.get())
                    if (markedMappedActivity == localMappedActivity.get()) {
                        return@flatMap Maybe.just(localMappedActivity.get())
                    }
                    return@flatMap createOrReplaceLocalMappedActivity(markedMappedActivity).andThen(
                        Maybe.just(markedMappedActivity)
                    )
                }
                return@flatMap Maybe.error<MappedActivity>(Resources.NotFoundException("mappedActivity was never fetched and connection problem occurred"))
            }
    }

    private fun createOrReplaceLocalMappedActivityAndGetIt(mappedActivity: MappedActivity): Single<MappedActivity> {
        return createOrReplaceLocalMappedActivity(mappedActivity).andThen(
            getLocalMappedActivity(
                mappedActivity.id
            )
        )
            .toSingle()
    }

    private fun createOrReplaceLocalMappedActivity(actualMappedActivity: MappedActivity): Completable {
        return Completable.fromCallable {
            mainDatabase.mappedActivityDao().upsert(actualMappedActivity)
        }
    }

    private fun deleteRedundantMappedActivityAsync(
        localMappedActivityValue: MappedActivity
    ) {
        if (localMappedActivityValue.status != Status.LOCAL_TO_CREATE
            && localMappedActivityValue.status != Status.LOCAL_TO_UPDATE
            && localMappedActivityValue.status != Status.LOCAL_TO_DELETE
        ) {
            deleteMappedActivityLocally(localMappedActivityValue).subscribe()   //would be removed in async way
        }
    }

    private fun deleteMappedActivityLocally(mappedActivity: MappedActivity): Completable {
        return mainDatabase.mappedActivityDao().delete(mappedActivity)
    }

    /**
     * optional exist over optional empty = connection problem
     * optional exist over optional exist = exists and connection was ok
     * optional empty over optional empty = ??? impossible. Server throws exception on 500 and i dont have a chance to wrap it. There is no Maybe.just(Optional.empty).map(Optional.empty(it))
     * optional empty over optional exist = server doesn't have it and connection was ok
     */
    private fun getRemoteMappedActivity(userId: Int, id: Int): Maybe<Optional<MappedActivity>> {
        return serverCommunicator.getMappedActivity(userId, id)
            .map { it.copy(status = Status.SYNCHRONIZED) }
            .map { Optional.of(it) }.toMaybe()
            .onErrorResumeNext { t: Throwable ->
                if (t is HttpException && t.code() == 500) {
                    return@onErrorResumeNext Maybe.empty<Optional<MappedActivity>>()
                }
                if (t is IOException || t is TimeoutException) {
                    return@onErrorResumeNext Maybe.just(Optional.empty<MappedActivity>())
                }
                Maybe.error(t)
            }
    }

    fun createMappedActivityAndGetItById(
        userId: Int,
        createDto: MappedActivityCreateDto
    ): Single<MappedActivity> {
        val newCommunicatorMappedActivity = createRemoteMappedActivity(userId, createDto)
        return newCommunicatorMappedActivity.flatMap {
            createOrReplaceLocalMappedActivityAndGetIt(it)
        }.onErrorResumeNext { t ->
            if (t is IOException || t is TimeoutException) {
                Log.e("AppRepository", t.message!!)
                return@onErrorResumeNext createLocalMappedActivityByDtoAndGetIt(createDto)
            }
            Single.error(t)
        }
    }

    private fun createRemoteMappedActivity(
        userId: Int,
        createDto: MappedActivityCreateDto
    ): Single<MappedActivity> {
        return serverCommunicator.createMappedActivity(userId, createDto)
            .map { it.copy(status = Status.SYNCHRONIZED) }
    }

    private fun getLocalMappedActivity(id: Int): Maybe<MappedActivity> {
        return mainDatabase.mappedActivityDao().getById(id)
    }

    fun updateMappedActivityAndGetItById(
        id: Int,
        updateDto: MappedActivityUpdateDto
    ): Single<MappedActivity> {
        val localMappedActivityById = getLocalMappedActivity(id).switchIfEmpty(
            userRepositoryService.guessCurrentUser().flatMapMaybe {
                getMappedActivityByUserIdAndId(it.id!!, id)
            }.switchIfEmpty(Single.error<MappedActivity>(Resources.NotFoundException("can not find mappedActivity to update")))
        )

        return localMappedActivityById.flatMap { localMappedActivity ->
            activityRepositoryService.getLocalActivity(localMappedActivity.activityId)
                .flatMapSingle { localActivity ->
                    updateRemoteMappedActivity(
                        localActivity.userId,
                        localMappedActivity.id,
                        updateDto
                    )
                }
        }.flatMap {
            updateAndGetLocalMappedActivity(it)
        }.onErrorResumeNext { t ->
            if (t is IOException || t is TimeoutException) {
                Log.e("AppRepository", t.message!!)
                return@onErrorResumeNext updateLocalMappedActivityByDtoAndGetIt(id, updateDto)
            }
            Single.error(t)
        }
    }

    private fun updateRemoteMappedActivity(
        userId: Int,
        id: Int,
        updateDto: MappedActivityUpdateDto
    ): Single<MappedActivity> {
        return serverCommunicator.updateMappedActivity(userId, id, updateDto)
            .map { it.copy(status = Status.SYNCHRONIZED) }
    }

    private fun updateAndGetLocalMappedActivity(mappedActivity: MappedActivity): Single<MappedActivity> {
        return updateLocalMappedActivity(mappedActivity).andThen(
            getLocalMappedActivity(
                mappedActivity.id
            ).toSingle()
        )
    }

    private fun updateLocalMappedActivity(mappedActivity: MappedActivity): Completable {
        return mainDatabase.mappedActivityDao().update(mappedActivity)
    }

    fun deleteMappedActivity(id: Int): Completable {
        val localMappedActivityById = getLocalMappedActivity(id).switchIfEmpty(
            userRepositoryService.guessCurrentUser().flatMapMaybe {
                getMappedActivityByUserIdAndId(it.id!!, id)
            }
        )
        return localMappedActivityById.isEmpty.flatMapCompletable { isLocalMappedActivityEmpty ->
            if (isLocalMappedActivityEmpty) { //just send a request with current user
                return@flatMapCompletable userRepositoryService.guessCurrentUser()
                    .flatMapCompletable { user ->
                        deleteRemoteMappedActivity(user.id!!, id)
                    }
            }
            return@flatMapCompletable localMappedActivityById.flatMapCompletable(
                deleteExistingMappedActivity()
            )
        }
    }

    private fun deleteExistingMappedActivity(): (MappedActivity) -> Completable {
        return completable@{ localMappedActivity ->
            if (localMappedActivity.status == Status.LOCAL_TO_CREATE) {
                return@completable deleteLocalMappedActivity(localMappedActivity)
            }
            return@completable activityRepositoryService.getLocalActivity(localMappedActivity.activityId)
                .flatMapCompletable { localActivity ->
                    deleteRemoteMappedActivity(
                        localActivity.userId,
                        localMappedActivity.id
                    ).andThen(deleteLocalMappedActivity(localMappedActivity))
                        .onErrorResumeNext { t ->
                            if (t is IOException || t is TimeoutException) {
                                return@onErrorResumeNext updateLocalMappedActivity(
                                    localMappedActivity.copy(
                                        status = Status.LOCAL_TO_DELETE
                                    )
                                )
                            }
                            if (t is HttpException && t.code() == 500) {
                                return@onErrorResumeNext deleteLocalMappedActivity(
                                    localMappedActivity
                                )
                            }
                            return@onErrorResumeNext Completable.error(t)
                        }
                }
        }
    }

    private fun deleteRemoteMappedActivity(userId: Int, id: Int): Completable {
        return serverCommunicator.deleteMappedActivity(userId, id)
    }

    private fun deleteLocalMappedActivity(mappedActivity: MappedActivity): Completable {
        return mainDatabase.mappedActivityDao().delete(mappedActivity)
    }

    private fun createLocalMappedActivityByDtoAndGetIt(createDto: MappedActivityCreateDto): Single<MappedActivity> {
        val mappedActivityFromCreateDto = createDto.mappedActivity
        val minId: Int = if (mainDatabase.mappedActivityDao().getMinId() > 0) {
            -mainDatabase.mappedActivityDao().getMinId()
        } else {
            mainDatabase.mappedActivityDao().getMinId() - 1
        }
        return userRepositoryService.getCurrentUser().flatMap { user ->
            scheduleRepositoryService.getScheduleByUserId(user.id!!).flatMap { schedule ->
                val mappedActivity = mappedActivityFromCreateDto.copy(
                    id = minId,
                    scheduleId = schedule.id,
                    createdDate = LocalDateTime.now(),
                    updatedDate = LocalDateTime.now()
                )
                createOrReplaceLocalMappedActivityAndGetIt(mappedActivity)
            }
        }
    }

    private fun updateLocalMappedActivityByDtoAndGetIt(
        id: Int,
        updateDto: MappedActivityUpdateDto
    ): Single<MappedActivity> {
        val (span, activityId) = updateDto
        return getLocalMappedActivity(id).flatMapSingle { mappedActivityToUpdate ->
            val unsavedUpdatedMappedActivity = mappedActivityToUpdate.copy(
                span = span ?: mappedActivityToUpdate.span,
                activityId = activityId ?: mappedActivityToUpdate.activityId,
                updatedDate = LocalDateTime.now(),
                status = if (mappedActivityToUpdate.status == Status.LOCAL_TO_CREATE) Status.LOCAL_TO_CREATE else Status.LOCAL_TO_UPDATE
            )
            updateAndGetLocalMappedActivity(unsavedUpdatedMappedActivity)
        }
    }

    fun generateAndGetMappedActivities(userId: Int): Single<List<MappedActivity>> {
        return serverCommunicator.generateAndGetMappedActivities(userId)
    }
}