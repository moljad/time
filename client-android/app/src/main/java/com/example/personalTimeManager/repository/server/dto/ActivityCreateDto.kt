package com.example.personalTimeManager.repository.server.dto

import androidx.room.Ignore
import com.example.personalTimeManager.R
import com.example.personalTimeManager.repository.CustomColor
import com.example.personalTimeManager.repository.database.Status
import com.example.personalTimeManager.repository.database.entity.Activity
import com.example.personalTimeManager.repository.server.gson.Exclude
import com.google.gson.annotations.SerializedName
import java.time.LocalDateTime

data class ActivityCreateDto(
    @SerializedName("name")
    val name: String?,
    @SerializedName("description")
    val description: String?,
    @SerializedName("priority")
    val priority: Int?,
    @SerializedName("isRest")
    val isRest: Boolean?,
    @SerializedName("link")
    val link: String?,
    @SerializedName("lengthMin")
    val lengthMin: Int?,
    @SerializedName("lengthMax")
    val lengthMax: Int?,
    @SerializedName("lengthRequired")
    val lengthRequired: Int?,
    @SerializedName("color")
    val color: CustomColor?
) {
    @Suppress("unused")
    private constructor() : this(
        "", "", 0, false, "",
        0, 6, 0, CustomColor(R.color.colorAccent)
    )

    @Exclude
    @delegate:Exclude
    @delegate:Ignore
    val activity by lazy {
        Activity(
            -1073741824,
            name ?: "",
            description ?: "",
            priority ?: 0,
            isRest ?: false,
            link ?: "",
            lengthMin ?: 0,
            lengthMax ?: 6,
            lengthRequired ?: 0,
            color ?: CustomColor(R.color.colorAccent),
            -1073741824,
            createdDate = LocalDateTime.now(),
            updatedDate = LocalDateTime.now(),
            status = Status.LOCAL_TO_CREATE
        )
    }
}