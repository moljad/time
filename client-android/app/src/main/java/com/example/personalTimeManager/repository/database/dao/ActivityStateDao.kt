package com.example.personalTimeManager.repository.database.dao

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.example.personalTimeManager.repository.database.dao.base.BaseDao
import com.example.personalTimeManager.repository.database.entity.ActivityState
import com.example.personalTimeManager.repository.database.viewentities.ActivityWithStateIds
import com.example.personalTimeManager.repository.database.viewentities.ActivityWithStates
import com.example.personalTimeManager.repository.database.viewentities.StateWithActivities
import com.example.personalTimeManager.repository.database.viewentities.StateWithActivityIds
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
abstract class ActivityStateDao : BaseDao<ActivityState>() {
    @Query("SELECT * FROM ActivityState WHERE activityId = :activityId AND stateId = :stateId")
    abstract fun getActivityStateByActivityAndStateIds(
        activityId: Int,
        stateId: Int
    ): Maybe<ActivityState>

    @Query("SELECT * FROM ActivityState WHERE activityId = :activityId")
    abstract fun getActivityStatesByActivityId(activityId: Int): Single<List<ActivityState>>

    @Query("SELECT * FROM States WHERE id = :stateId")
    @Transaction
    abstract fun getStateWithActivityIdsByStateId(stateId: Int): Maybe<StateWithActivityIds>

    @Query("SELECT * FROM States WHERE id = :stateId")
    @Transaction
    abstract fun getStateWithActivitiesByStateId(stateId: Int): Maybe<StateWithActivities>

    @Query("SELECT * FROM Activities WHERE id = :activityId")
    @Transaction
    abstract fun getActivityWithStateIdsByActivityId(activityId: Int): Maybe<ActivityWithStateIds>

    @Query("SELECT * FROM Activities WHERE id = :activityId")
    @Transaction
    abstract fun getActivityWithStatesByActivityId(activityId: Int): Maybe<ActivityWithStates>
}