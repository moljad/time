package com.example.personalTimeManager.presentation.activities.groups

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.example.personalTimeManager.presentation.base.BaseViewModel
import com.example.personalTimeManager.repository.AppRepository
import com.example.personalTimeManager.repository.database.entity.Group

class GroupsViewModel(application: Application, private val repository: AppRepository) :
    BaseViewModel(application) {
    val states: MutableLiveData<List<Group>> = MutableLiveData(listOf())
}