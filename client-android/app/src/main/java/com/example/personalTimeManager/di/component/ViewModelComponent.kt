package com.example.personalTimeManager.di.component

import com.example.personalTimeManager.di.module.ViewModelModule
import com.example.personalTimeManager.di.scope.ViewModelScope
import com.example.personalTimeManager.presentation.activities.activities.ActivitiesFragment
import com.example.personalTimeManager.presentation.activities.activityEditor.ActivityEditorFragment
import com.example.personalTimeManager.presentation.activities.groupEditor.GroupEditorFragment
import com.example.personalTimeManager.presentation.activities.groups.GroupsFragment
import com.example.personalTimeManager.presentation.activities.login.LoginFragment
import com.example.personalTimeManager.presentation.activities.login.LoginRegistrationActivity
import com.example.personalTimeManager.presentation.activities.login.RegistrationFragment
import com.example.personalTimeManager.presentation.activities.main.MainActivity
import com.example.personalTimeManager.presentation.activities.mappedActivityEditor.MappedActivityEditorFragment
import com.example.personalTimeManager.presentation.activities.mappedStateEditor.MappedStateEditorFragment
import com.example.personalTimeManager.presentation.activities.activitySchedule.ActivityScheduleFragment
import com.example.personalTimeManager.presentation.activities.scheduleEditor.ScheduleEditorFragment
import com.example.personalTimeManager.presentation.activities.settings.SettingsFragment
import com.example.personalTimeManager.presentation.activities.stateEditor.StateEditorFragment
import com.example.personalTimeManager.presentation.activities.stateSchedule.StateScheduleFragment
import com.example.personalTimeManager.presentation.activities.states.StatesFragment
import dagger.Component

@ViewModelScope
@Component(
    modules = [ViewModelModule::class],
    dependencies = [RepositoryComponent::class, UserComponent::class]
)
interface ViewModelComponent {
    fun inject(activity: MainActivity)
    fun inject(activity: LoginRegistrationActivity)
    fun inject(fragment: LoginFragment)
    fun inject(fragment: RegistrationFragment)
    fun inject(fragment: StatesFragment)
    fun inject(fragment: StateEditorFragment)
    fun inject(fragment: ActivitiesFragment)
    fun inject(fragment: ActivityEditorFragment)
    fun inject(fragment: ActivityScheduleFragment)
    fun inject(fragment: GroupsFragment)
    fun inject(fragment: GroupEditorFragment)
    fun inject(fragment: SettingsFragment)
    fun inject(fragment: MappedStateEditorFragment)
    fun inject(fragment: MappedActivityEditorFragment)
    fun inject(fragment: ScheduleEditorFragment)
    fun inject(fragment: StateScheduleFragment)
}