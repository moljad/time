package com.example.personalTimeManager.presentation.activities.mappedActivityEditor

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.example.personalTimeManager.R
import com.example.personalTimeManager.databinding.FragmentMappedActivityEditorBinding
import com.example.personalTimeManager.di.component.ViewModelComponent
import com.example.personalTimeManager.presentation.base.BaseFragment
import com.example.personalTimeManager.repository.Span
import com.example.personalTimeManager.repository.database.entity.Activity
import com.example.personalTimeManager.repository.server.dto.MappedActivityCreateDto
import com.example.personalTimeManager.repository.server.dto.MappedActivityUpdateDto
import com.google.android.material.floatingactionbutton.FloatingActionButton
import io.reactivex.functions.Consumer
import javax.inject.Inject

class MappedActivityEditorFragment : BaseFragment() {

    lateinit var viewModel: MappedActivityEditorViewModel
        @Inject set

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentMappedActivityEditorBinding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_mapped_activity_editor, container, false
            )
        binding.fragment = this
        binding.lifecycleOwner = this

        val view = binding.root

        val fab: FloatingActionButton = container!!.rootView.findViewById(R.id.fab)
        fab.hide()

        val activitySpinner: Spinner = view.findViewById(R.id.activitySpinner)!!

        activitySpinner.onItemSelectedListener = object : AdapterView.OnItemClickListener,
            AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (viewModel.activities.isNotEmpty()) {
                    viewModel.mappedActivity.value?.activityId = viewModel.activities[position].id
                }
            }

            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

            }
        }

        val updateButtonView: Button = view.findViewById(R.id.update)
        updateButtonView.setOnClickListener(this::onUpdateButtonPressed)

        val deleteButtonView: Button = view.findViewById(R.id.delete)
        deleteButtonView.setOnClickListener(this::onDeleteButtonPressed)

        val createButtonView: Button = view.findViewById(R.id.create)
        createButtonView.setOnClickListener(this::onCreateButtonPressed)

        initializeDayTime(view)

        return view
    }

    fun initializeDayTime(view: View) {
        /*val dayTimeStart:View = view.findViewById(R.id.dayTimeStart)
        dayTimeStart.setOnClickListener{
            throw Exception()
        }*/
    }

    override fun injectDependency(component: ViewModelComponent) {
        component.inject(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val mappedActivityId =
            MappedActivityEditorFragmentArgs.fromBundle(arguments!!).mappedActivityId

        if (mappedActivityId != 0) {
            viewModel.getMappedActivity(mappedActivityId).subscribe {
                viewModel.mappedActivity.postValue(
                    MappedActivityEditorViewModel.MappedActivityModel(
                        mappedActivityId,
                        it.span.begin,
                        it.span.end,
                        it.activityId
                    )
                )
            }
        } else {
            viewModel.mappedActivity.postValue(
                MappedActivityEditorViewModel.MappedActivityModel(
                    mappedActivityId,
                    0,
                    2,
                    -1
                )
            )
        }

        viewModel.getActivities().subscribe({
            viewModel.activities = it

            val selectedActivity =
                viewModel.activities.find { it.id == viewModel.mappedActivity.value?.activityId }
            val activityPos = viewModel.activities.indexOf(selectedActivity)

            val names = viewModel.activities.map(Activity::name)

            val arrayAdapter =
                ArrayAdapter<String>(context!!, android.R.layout.simple_spinner_item, names)
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

            val state: Spinner = view!!.findViewById(R.id.activitySpinner)!!
            state.adapter = arrayAdapter
            state.setSelection(activityPos)
        },
            {
                throw Exception()
            })
    }

    fun onUpdateButtonPressed(v: View) {
        val mappedActivity = viewModel.mappedActivity.value!!

        val mappedActivityUpdateDto = MappedActivityUpdateDto(
            Span(mappedActivity.begin, mappedActivity.end),
            mappedActivity.activityId
        )

        viewModel.updateMappedActivity(mappedActivity.id, mappedActivityUpdateDto)
            .subscribe(Consumer {
                findNavController().popBackStack()
            })
    }

    fun onDeleteButtonPressed(v: View) {
        viewModel.deleteMappedActivity(viewModel.mappedActivity.value?.id!!).subscribe {
            findNavController().popBackStack()
        }
    }

    fun onCreateButtonPressed(v: View) {

        val mappedActivity = viewModel.mappedActivity.value!!

        val mappedActivityCreateDto = MappedActivityCreateDto(
            Span(mappedActivity.begin, mappedActivity.end),
            mappedActivity.activityId
        )

        Log.e("sdfsadfasdfasdfasdf", mappedActivityCreateDto.toString())

        viewModel.createMappedActivity(mappedActivityCreateDto).subscribe({
            findNavController().popBackStack()
        }, {
            throw it
        })
    }

}
