package com.example.personalTimeManager.presentation.activities.states

import android.graphics.Color
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.personalTimeManager.R

class StatesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val textView: TextView = itemView.findViewById(R.id.state_name)
    private val colorView: View = itemView.findViewById(R.id.state_color) as View

    fun setName(string: String) {
        textView.text = string
    }

    fun setColor(color: String) {
        colorView.setBackgroundColor(Color.parseColor(color))
    }
}