package com.example.personalTimeManager.repository.service

import android.content.res.Resources
import android.util.Log
import com.example.personalTimeManager.repository.database.AppDatabase
import com.example.personalTimeManager.repository.database.Status
import com.example.personalTimeManager.repository.database.converter.DateConverters
import com.example.personalTimeManager.repository.database.converter.StatusConverters
import com.example.personalTimeManager.repository.database.entity.MappedState
import com.example.personalTimeManager.repository.server.ServerCommunicator
import com.example.personalTimeManager.repository.server.dto.MappedStateCreateDto
import com.example.personalTimeManager.repository.server.dto.MappedStateUpdateDto
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.MaybeSource
import io.reactivex.Single
import io.reactivex.rxkotlin.zipWith
import retrofit2.HttpException
import java.io.IOException
import java.time.LocalDateTime
import java.util.*
import java.util.concurrent.TimeoutException
import javax.inject.Inject
import kotlin.collections.ArrayList

class MappedStateRepositoryService @Inject constructor(
    private val userRepositoryService: UserRepositoryService,
    private val stateRepositoryService: StateRepositoryService,
    private val scheduleRepositoryService: ScheduleRepositoryService,
    private val serverCommunicator: ServerCommunicator,
    private val mainDatabase: AppDatabase
) {

    /**gets from local mappedState.
     * If is not found, then tries to synchronize it with current user and get it again**/
    fun guessUserIdByMappedStateId(id: Int): Maybe<Int> {
        return getLocalMappedState(id)
            .switchIfEmpty(MaybeSource<MappedState> { maybeObserver ->
                userRepositoryService.guessCurrentUser()
                    .flatMapMaybe { guessedUser ->
                        getMappedStateByUserIdAndId(guessedUser.id!!, id).map {
                            maybeObserver.onSuccess(it)
                        }
                    }
                //maybeSource.onErrorReturn { maybeObserver.onError(it) }
                //maybeSource.isEmpty.filter { it }.doOnComplete {  }
                //maybeSource.doOnComplete {  }
            }).flatMap {
                stateRepositoryService.guessUserIdByStateId(it.stateId)
            }.onErrorResumeNext { _: Throwable -> return@onErrorResumeNext Maybe.empty<Int>() }
    }

    fun getMappedStatesByUserId(userId: Int): Single<List<MappedState>> {
        val refreshedMappedStates = synchronizeMappedStatesAndGetThemByUserId(userId)
        val localMappedStates = getLocalMappedStatesByUserId(userId)

        return refreshedMappedStates.zipWith(localMappedStates)
            .flatMap { (refreshedMappedStates, localMappedStates) ->
                if (!refreshedMappedStates.isPresent) {
                    Log.i("AppRepository", "returning local mappedStates instead")
                    val markedLocalMappedStates =
                        markAsLocalIfNotDeferred(localMappedStates)
                    return@flatMap createOrReplaceLocalMappedStates(markedLocalMappedStates).andThen(
                        Single.just(markedLocalMappedStates)
                    )
                }
                return@flatMap Single.just(refreshedMappedStates.get())
            }
    }

    private fun markAsLocalIfNotDeferred(mappedStates: List<MappedState>): List<MappedState> {
        return mappedStates.map {
            markAsLocalIfNotDeferred(it)
        }
    }

    private fun markAsLocalIfNotDeferred(mappedState: MappedState): MappedState {
        if (mappedState.status != Status.LOCAL_TO_CREATE
            && mappedState.status != Status.LOCAL_TO_UPDATE
            && mappedState.status != Status.LOCAL_TO_DELETE
        ) {
            return mappedState.copy(status = Status.LOCAL)
        }
        return mappedState
    }


    private fun getLocalMappedStatesByUserId(userId: Int): Single<List<MappedState>> {
        return mainDatabase.mappedStateDao().getByUserId(userId)
    }

    private fun synchronizeMappedStatesAndGetThemByUserId(
        userId: Int
    ): Single<Optional<List<MappedState>>> {
        return synchronizeMappedStates(userId).andThen(
            getLocalMappedStatesByUserId(userId)
        ).map { Optional.of(it) }.onErrorResumeNext { t ->
            if (t is IOException || t is TimeoutException) {
                return@onErrorResumeNext Single.just(Optional.empty<List<MappedState>>())
            }
            Single.error<Optional<List<MappedState>>>(t)
        }
    }

    private fun synchronizeMappedStates(userId: Int): Completable {
        return stateRepositoryService.synchronizeStatesAndGetThemByUserId(userId)
            .ignoreElement()
            .andThen(getLocalMappedStatesByUserId(userId).flatMapCompletable { localMappedStates ->
                val synchronizeToCreate =
                    synchronizeDeferredToCreateMappedStates(userId, localMappedStates)
                val synchronizeToUpdate =
                    synchronizeDeferredToUpdateMappedStates(userId, localMappedStates)
                val synchronizeToDelete =
                    synchronizeDeferredToDeleteMappedStates(userId, localMappedStates)

                Completable.mergeArrayDelayError(
                    synchronizeToCreate, synchronizeToUpdate, synchronizeToDelete
                ).andThen(getRemotesAndRefreshMappedStates(userId, localMappedStates))
            })
    }

    private fun synchronizeDeferredToCreateMappedStates(
        userId: Int,
        localMappedStates: List<MappedState>
    ): Completable {
        val taskList = ArrayList<Completable>()
        localMappedStates.forEach { localMappedState ->
            if (localMappedState.status == Status.LOCAL_TO_CREATE) {
                val createRemoteMappedState =
                    synchronizeDeferredToCreateMappedState(userId, localMappedState)
                taskList.add(createRemoteMappedState)
            }
        }
        val params = taskList.toTypedArray()
        return Completable.mergeArrayDelayError(*params)
    }

    private fun synchronizeDeferredToCreateMappedState(
        userId: Int,
        localMappedState: MappedState
    ): Completable {
        return createRemoteMappedState(
            userId,
            localMappedState.mappedStateCreateDto
        ).flatMapCompletable { newMappedState ->
            val (id, span, stateId, scheduleId,
                createdDate, updatedDate, status) = newMappedState
            val stringCreateDate = DateConverters.dateToString(createdDate)
            val stringUpdatedDate = DateConverters.dateToString(updatedDate)
            val stringStatus = StatusConverters.statusToString(status)

            mainDatabase.mappedStateDao().replace(
                localMappedState.id, id, span.begin, span.end, stateId, scheduleId,
                stringCreateDate, stringUpdatedDate, stringStatus
            )
        }.onErrorResumeNext { t ->
            if (t is HttpException && t.code() == 500) {
                return@onErrorResumeNext deleteLocalMappedState(localMappedState)
            }
            if (t is IOException || t is TimeoutException) {
                return@onErrorResumeNext Completable.complete()
            }
            return@onErrorResumeNext Completable.error(t)
        }
    }

    private fun synchronizeDeferredToUpdateMappedStates(
        userId: Int,
        localMappedStates: List<MappedState>
    ): Completable {
        val taskList = ArrayList<Completable>()
        localMappedStates.forEach { localMappedState ->
            if (localMappedState.status == Status.LOCAL_TO_UPDATE) {
                val updateRemoteMappedState =
                    synchronizeDeferredToUpdateMappedState(userId, localMappedState)
                taskList.add(updateRemoteMappedState)
            }
        }
        val params = taskList.toTypedArray()
        return Completable.mergeArrayDelayError(*params)
    }

    private fun synchronizeDeferredToUpdateMappedState(
        userId: Int,
        localMappedState: MappedState
    ): Completable {
        return updateRemoteMappedState(
            userId,
            localMappedState.id,
            localMappedState.mappedStateUpdateDto
        ).flatMapCompletable { Completable.complete() }
            .onErrorResumeNext { t ->
                if (t is HttpException && t.code() == 500) {
                    return@onErrorResumeNext getMappedStateByUserIdAndId(
                        userId,
                        localMappedState.id
                    ).flatMapCompletable { Completable.complete() }
                }
                if (t is IOException || t is TimeoutException) {
                    return@onErrorResumeNext Completable.complete()
                }
                return@onErrorResumeNext Completable.error(t)
            }
    }

    private fun synchronizeDeferredToDeleteMappedStates(
        userId: Int,
        localMappedStates: List<MappedState>
    ): Completable {
        val taskList = ArrayList<Completable>()
        localMappedStates.forEach { localMappedState ->
            if (localMappedState.status == Status.LOCAL_TO_DELETE) {
                val deleteSynchronizationTask =
                    synchronizeDeferredToDeleteMappedState(userId, localMappedState)
                taskList.add(deleteSynchronizationTask)
            }
        }
        val params = taskList.toTypedArray()
        return Completable.mergeArrayDelayError(*params)
    }

    private fun synchronizeDeferredToDeleteMappedState(
        userId: Int,
        localMappedState: MappedState
    ): Completable {
        return deleteRemoteMappedState(userId, localMappedState.id)
            .andThen(deleteLocalMappedState(localMappedState))
            .onErrorResumeNext { t ->
                if (t is HttpException && t.code() == 500) {
                    return@onErrorResumeNext getMappedStateByUserIdAndId(
                        userId,
                        localMappedState.id
                    ).flatMapCompletable { Completable.complete() }
                }
                if (t is IOException || t is TimeoutException) {
                    return@onErrorResumeNext Completable.complete()
                }
                return@onErrorResumeNext Completable.error(t)
            }
    }

    private fun getRemotesAndRefreshMappedStates(
        userId: Int,
        localMappedStates: List<MappedState>
    ): Completable {
        return getRemoteMappedStates(userId).flatMapCompletable { listOptional ->
            if (listOptional.isPresent) {
                val remoteMappedStates = listOptional.get()
                return@flatMapCompletable removeRedundantMappedStates(
                    localMappedStates,
                    remoteMappedStates
                ).andThen(
                    createOrReplaceLocalMappedStates(remoteMappedStates)
                )
            }
            return@flatMapCompletable Completable.error(IOException("Could not receive remote entities"))
        }
    }

    private fun getRemoteMappedStates(userId: Int): Single<Optional<List<MappedState>>> {
        return serverCommunicator.getMappedStates(userId)
            .map { Optional.of(it.map { newMappedState -> newMappedState.copy(status = Status.SYNCHRONIZED) }) }
            .onErrorResumeNext { t ->
                if (t is IOException || t is TimeoutException) {
                    Log.e("AppRepository", t.message!!)
                    return@onErrorResumeNext Single.just(Optional.empty())
                }
                return@onErrorResumeNext Single.just(Optional.empty())
            }
    }

    private fun removeRedundantMappedStates(
        localMappedStates: List<MappedState>,
        actualMappedStates: List<MappedState>
    ): Completable {
        val newIds = actualMappedStates.map { it.id }
        val localIds = localMappedStates.filter {
            it.status != Status.LOCAL_TO_CREATE
                    && it.status != Status.LOCAL_TO_UPDATE
                    && it.status != Status.LOCAL_TO_DELETE
        }.map { it.id }
        val redundantIds = localIds.subtract(newIds)

        val redundant = localMappedStates.filter { it.id in redundantIds }
        if (redundant.isNotEmpty()) {
            return deleteMappedStatesLocally(redundant)
        }
        return Completable.complete()
    }

    private fun deleteMappedStatesLocally(list: List<MappedState>): Completable {
        return mainDatabase.mappedStateDao().deleteList(list)
    }

    private fun createOrReplaceLocalMappedStates(actualMappedStates: List<MappedState>): Completable {
        return Completable.fromCallable {
            mainDatabase.mappedStateDao().upsertList(actualMappedStates)
        }
    }

    fun getMappedStateByUserIdAndId(userId: Int, id: Int): Maybe<MappedState> {
        val communicatorMappedState =
            getRemoteMappedState(userId, id).map { Optional.of(it) }
                .defaultIfEmpty(Optional.empty())
        val localMappedState =
            getLocalMappedState(id).map { Optional.of(it) }.defaultIfEmpty(Optional.empty())

        return communicatorMappedState.zipWith(localMappedState)
            .flatMap { (communicatorMappedState, localMappedState) ->
                if (!communicatorMappedState.isPresent) {    //server doesn't have it, but local does
                    if (localMappedState.isPresent) {  //redundant local data
                        deleteRedundantMappedStateAsync(localMappedState.get())
                    }
                    return@flatMap Maybe.empty<MappedState>()
                }
                if (communicatorMappedState.isPresent && communicatorMappedState.get().isPresent) { //ok. just refresh local data with this new value
                    val actualMappedState = communicatorMappedState.get().get()
                    return@flatMap createOrReplaceLocalMappedStateAndGetIt(actualMappedState).toMaybe()
                }
                if (communicatorMappedState.isPresent && !communicatorMappedState.get().isPresent && localMappedState.isPresent) {
                    Log.i("AppRepository", "returning local mappedState instead")
                    val markedMappedState = markAsLocalIfNotDeferred(localMappedState.get())
                    if (markedMappedState == localMappedState.get()) {
                        return@flatMap Maybe.just(localMappedState.get())
                    }
                    return@flatMap createOrReplaceLocalMappedState(markedMappedState).andThen(
                        Maybe.just(markedMappedState)
                    )
                }
                return@flatMap Maybe.error<MappedState>(Resources.NotFoundException("mappedState was never fetched and connection problem occurred"))
            }
    }

    private fun createOrReplaceLocalMappedStateAndGetIt(mappedState: MappedState): Single<MappedState> {
        return createOrReplaceLocalMappedState(mappedState).andThen(
            getLocalMappedState(
                mappedState.id
            )
        )
            .toSingle()
    }

    private fun createOrReplaceLocalMappedState(actualMappedState: MappedState): Completable {
        return Completable.fromCallable {
            mainDatabase.mappedStateDao().upsert(actualMappedState)
        }
    }

    private fun deleteRedundantMappedStateAsync(
        localMappedStateValue: MappedState
    ) {
        if (localMappedStateValue.status != Status.LOCAL_TO_CREATE
            && localMappedStateValue.status != Status.LOCAL_TO_UPDATE
            && localMappedStateValue.status != Status.LOCAL_TO_DELETE
        ) {
            deleteMappedStateLocally(localMappedStateValue).subscribe()   //would be removed in async way
        }
    }

    private fun deleteMappedStateLocally(mappedState: MappedState): Completable {
        return mainDatabase.mappedStateDao().delete(mappedState)
    }

    /**
     * optional exist over optional empty = connection problem
     * optional exist over optional exist = exists and connection was ok
     * optional empty over optional empty = ??? impossible. Server throws exception on 500 and i dont have a chance to wrap it. There is no Maybe.just(Optional.empty).map(Optional.empty(it))
     * optional empty over optional exist = server doesn't have it and connection was ok
     */
    private fun getRemoteMappedState(userId: Int, id: Int): Maybe<Optional<MappedState>> {
        return serverCommunicator.getMappedState(userId, id)
            .map { it.copy(status = Status.SYNCHRONIZED) }
            .map { Optional.of(it) }.toMaybe()
            .onErrorResumeNext { t: Throwable ->
                if (t is HttpException && t.code() == 500) {
                    return@onErrorResumeNext Maybe.empty<Optional<MappedState>>()
                }
                if (t is IOException || t is TimeoutException) {
                    return@onErrorResumeNext Maybe.just(Optional.empty<MappedState>())
                }
                Maybe.error(t)
            }
    }

    fun createMappedStateAndGetItById(
        userId: Int,
        createDto: MappedStateCreateDto
    ): Single<MappedState> {
        val newCommunicatorMappedState = createRemoteMappedState(userId, createDto)
        return newCommunicatorMappedState.flatMap {
            createOrReplaceLocalMappedStateAndGetIt(it)
        }.onErrorResumeNext { t ->
            if (t is IOException || t is TimeoutException) {
                Log.e("AppRepository", t.message!!)
                return@onErrorResumeNext createLocalMappedStateByDtoAndGetIt(createDto)
            }
            Single.error(t)
        }
    }

    private fun createRemoteMappedState(
        userId: Int,
        createDto: MappedStateCreateDto
    ): Single<MappedState> {
        return serverCommunicator.createMappedState(userId, createDto)
            .map { it.copy(status = Status.SYNCHRONIZED) }
    }

    private fun getLocalMappedState(id: Int): Maybe<MappedState> {
        return mainDatabase.mappedStateDao().getById(id)
    }

    fun updateMappedStateAndGetItById(
        id: Int,
        updateDto: MappedStateUpdateDto
    ): Single<MappedState> {
        val localMappedStateById = getLocalMappedState(id).switchIfEmpty(
            userRepositoryService.guessCurrentUser().flatMapMaybe {
                getMappedStateByUserIdAndId(it.id!!, id)
            }.switchIfEmpty(Single.error<MappedState>(Resources.NotFoundException("can not find mappedState to update")))
        )

        return localMappedStateById.flatMap { localMappedState ->
            stateRepositoryService.getLocalState(localMappedState.stateId)
                .flatMapSingle { localState ->
                    updateRemoteMappedState(
                        localState.userId,
                        localMappedState.id,
                        updateDto
                    )
                }
        }.flatMap {
            updateAndGetLocalMappedState(it)
        }.onErrorResumeNext { t ->
            if (t is IOException || t is TimeoutException) {
                Log.e("AppRepository", t.message!!)
                return@onErrorResumeNext updateLocalMappedStateByDtoAndGetIt(id, updateDto)
            }
            Single.error(t)
        }
    }

    private fun updateRemoteMappedState(
        userId: Int,
        id: Int,
        updateDto: MappedStateUpdateDto
    ): Single<MappedState> {
        return serverCommunicator.updateMappedState(userId, id, updateDto)
            .map { it.copy(status = Status.SYNCHRONIZED) }
    }

    private fun updateAndGetLocalMappedState(mappedState: MappedState): Single<MappedState> {
        return updateLocalMappedState(mappedState).andThen(
            getLocalMappedState(
                mappedState.id
            ).toSingle()
        )
    }

    private fun updateLocalMappedState(mappedState: MappedState): Completable {
        return mainDatabase.mappedStateDao().update(mappedState)
    }

    fun deleteMappedState(id: Int): Completable {
        val localMappedStateById = getLocalMappedState(id).switchIfEmpty(
            userRepositoryService.guessCurrentUser().flatMapMaybe {
                getMappedStateByUserIdAndId(it.id!!, id)
            }
        )
        return localMappedStateById.isEmpty.flatMapCompletable { isLocalMappedStateEmpty ->
            if (isLocalMappedStateEmpty) { //just send a request with current user
                return@flatMapCompletable userRepositoryService.guessCurrentUser()
                    .flatMapCompletable { user ->
                        deleteRemoteMappedState(user.id!!, id)
                    }
            }
            return@flatMapCompletable localMappedStateById.flatMapCompletable(
                deleteExistingMappedState()
            )
        }
    }

    private fun deleteExistingMappedState(): (MappedState) -> Completable {
        return completable@{ localMappedState ->
            if (localMappedState.status == Status.LOCAL_TO_CREATE) {
                return@completable deleteLocalMappedState(localMappedState)
            }
            return@completable stateRepositoryService.getLocalState(localMappedState.stateId)
                .flatMapCompletable { localState ->
                    deleteRemoteMappedState(
                        localState.userId,
                        localMappedState.id
                    ).andThen(deleteLocalMappedState(localMappedState))
                        .onErrorResumeNext { t ->
                            if (t is IOException || t is TimeoutException) {
                                return@onErrorResumeNext updateLocalMappedState(
                                    localMappedState.copy(
                                        status = Status.LOCAL_TO_DELETE
                                    )
                                )
                            }
                            if (t is HttpException && t.code() == 500) {
                                return@onErrorResumeNext deleteLocalMappedState(
                                    localMappedState
                                )
                            }
                            return@onErrorResumeNext Completable.error(t)
                        }
                }
        }
    }

    private fun deleteRemoteMappedState(userId: Int, id: Int): Completable {
        return serverCommunicator.deleteMappedState(userId, id)
    }

    private fun deleteLocalMappedState(mappedState: MappedState): Completable {
        return mainDatabase.mappedStateDao().delete(mappedState)
    }

    private fun createLocalMappedStateByDtoAndGetIt(createDto: MappedStateCreateDto): Single<MappedState> {
        val mappedStateFromCreateDto = createDto.mappedState
        val minId: Int = if (mainDatabase.mappedStateDao().getMinId() > 0) {
            -mainDatabase.mappedStateDao().getMinId()
        } else {
            mainDatabase.mappedStateDao().getMinId() - 1
        }
        return userRepositoryService.getCurrentUser().flatMap { user ->
            scheduleRepositoryService.getScheduleByUserId(user.id!!).flatMap { schedule ->
                val mappedState = mappedStateFromCreateDto.copy(
                    id = minId,
                    scheduleId = schedule.id,
                    createdDate = LocalDateTime.now(),
                    updatedDate = LocalDateTime.now()
                )
                createOrReplaceLocalMappedStateAndGetIt(mappedState)
            }
        }
    }

    private fun updateLocalMappedStateByDtoAndGetIt(
        id: Int,
        updateDto: MappedStateUpdateDto
    ): Single<MappedState> {
        val (span, stateId) = updateDto
        return getLocalMappedState(id).flatMapSingle { mappedStateToUpdate ->
            val unsavedUpdatedMappedState = mappedStateToUpdate.copy(
                span = span ?: mappedStateToUpdate.span,
                stateId = stateId ?: mappedStateToUpdate.stateId,
                updatedDate = LocalDateTime.now(),
                status = if (mappedStateToUpdate.status == Status.LOCAL_TO_CREATE) Status.LOCAL_TO_CREATE else Status.LOCAL_TO_UPDATE
            )
            updateAndGetLocalMappedState(unsavedUpdatedMappedState)
        }
    }
}