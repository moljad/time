package com.example.personalTimeManager.repository.database.entity

import androidx.room.*
import com.example.personalTimeManager.repository.database.Status
import com.example.personalTimeManager.repository.database.converter.DateConverters
import com.example.personalTimeManager.repository.server.gson.Exclude
import com.google.gson.annotations.SerializedName
import java.time.LocalDateTime

@Entity(
    tableName = "Schedules",
    foreignKeys = [ForeignKey(
        entity = UserEntity::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("userId"),
        onUpdate = ForeignKey.CASCADE,
        onDelete = ForeignKey.CASCADE
    )]
)
data class Schedule(
    @PrimaryKey
    @SerializedName("id")
    val id: Int,
    @SerializedName("active")
    val active: Boolean,
    @SerializedName("alternating")
    val alternating: Boolean,
    @SerializedName("restLength")
    val restLength: Int,
    @SerializedName("activityUsualLenght")
    val activityUsualLenght: Int,
    @ColumnInfo(name = "userId", index = true)
    @SerializedName("userId")
    val userId: Int,
    @TypeConverters(DateConverters::class)
    @SerializedName("createdDate")
    val createdDate: LocalDateTime,
    @TypeConverters(DateConverters::class)
    @SerializedName("updatedDate")
    val updatedDate: LocalDateTime,
    @Exclude
    val status: Status = Status.UNKNOWN
) {
    @Suppress("unused")
    private constructor() : this(
        -1073741824, false, false, 0,
        1, -1, LocalDateTime.now(), LocalDateTime.now()
    )
}