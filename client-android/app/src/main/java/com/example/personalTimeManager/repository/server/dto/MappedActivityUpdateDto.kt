package com.example.personalTimeManager.repository.server.dto

import com.example.personalTimeManager.repository.Span
import com.google.gson.annotations.SerializedName

data class MappedActivityUpdateDto(
    @SerializedName("span")
    val span: Span?,
    @SerializedName("activityId")
    val activityId: Int?
)