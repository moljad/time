package com.example.personalTimeManager.repository.server

import com.example.personalTimeManager.repository.database.entity.*
import com.example.personalTimeManager.repository.server.dto.*
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.*

interface ApiService {
    @GET("/users/me")
    fun getCurrentUser(): Single<UserEntity>


    @GET("/users/{id}/schedule")
    fun getSchedule(@Path(value = "id") userId: String): Single<Schedule>

    @PUT("/users/{id}/schedule")
    fun createSchedule(
        @Path(value = "id") userId: String,
        @Body scheduleCreateDto: ScheduleCreateDto
    ): Single<Schedule>

    @PATCH("/users/{id}/schedule")
    fun updateSchedule(
        @Path(value = "id") userId: String,
        @Body scheduleUpdateDto: ScheduleUpdateDto
    ): Single<Schedule>

    @DELETE("/users/{id}/schedule")
    fun deleteSchedule(@Path(value = "id") userId: String): Completable


    @GET("/users/{id}/states")
    fun getStates(@Path(value = "id") userId: String): Single<List<State>>

    @GET("/users/{id}/states/{stateId}")
    fun getState(
        @Path(value = "id") userId: String,
        @Path(value = "stateId") stateId: String
    ): Single<State>

    @POST("/users/{id}/states")
    fun createState(
        @Path(value = "id") userId: String,
        @Body stateCreateDto: StateCreateDto
    ): Single<State>

    @PATCH("/users/{id}/states/{stateId}")
    fun updateState(
        @Path(value = "id") userId: String,
        @Path(value = "stateId") stateId: String,
        @Body stateUpdateDto: StateUpdateDto
    ): Single<State>

    @DELETE("/users/{id}/states/{stateId}")
    fun deleteState(
        @Path(value = "id") userId: String,
        @Path(value = "stateId") stateId: String
    ): Completable


    @GET("/users/{id}/activities")
    fun getActivities(@Path(value = "id") userId: String): Single<List<Activity>>

    @GET("/users/{id}/activities/{activityId}")
    fun getActivity(
        @Path(value = "id") userId: String,
        @Path(value = "activityId") activityId: String
    ): Single<Activity>

    @POST("/users/{id}/activities")
    fun createActivity(
        @Path(value = "id") userId: String,
        @Body activityCreateDto: ActivityCreateDto
    ): Single<Activity>

    @PATCH("/users/{id}/activities/{activityId}")
    fun updateActivity(
        @Path(value = "id") userId: String,
        @Path(value = "activityId") activityId: String,
        @Body activityUpdateDto: ActivityUpdateDto
    ): Single<Activity>

    @DELETE("/users/{id}/activities/{activityId}")
    fun deleteActivity(
        @Path(value = "id") userId: String,
        @Path(value = "activityId") activityId: String
    ): Completable


    @GET("/users/{id}/activities/{activityId}/states")
    fun getActivityState(
        @Path(value = "id") userId: String,
        @Path(value = "activityId") activityId: String
    ): Single<List<ActivityStateDto>>

    @PUT("/users/{id}/activities/{activityId}/states/{stateId}")
    fun createActivityState(
        @Path(value = "id") userId: String,
        @Path(value = "activityId") activityId: String,
        @Path(value = "stateId") stateId: String,
        @Body body: Any = Object()
    ): Completable

    @DELETE("/users/{id}/activities/{activityId}/states/{stateId}")
    fun deleteActivityState(
        @Path(value = "id") userId: String,
        @Path(value = "activityId") activityId: String,
        @Path(value = "stateId") stateId: String
    ): Completable

    @GET("/users/{id}/mappedstates")
    fun getMappedStates(@Path(value = "id") userId: String): Single<List<MappedState>>

    @GET("/users/{id}/mappedstates/{mappedStateId}")
    fun getMappedState(
        @Path(value = "id") userId: String,
        @Path(value = "mappedStateId") mappedStateId: String
    ): Single<MappedState>

    @POST("/users/{id}/mappedstates")
    fun createMappedState(
        @Path(value = "id") userId: String,
        @Body mappedStateCreateDto: MappedStateCreateDto
    ): Single<MappedState>

    @PATCH("/users/{id}/mappedstates/{mappedStateId}")
    fun updateMappedState(
        @Path(value = "id") userId: String,
        @Path(value = "mappedStateId") mappedStateId: String,
        @Body mappedStateUpdateDto: MappedStateUpdateDto
    ): Single<MappedState>

    @DELETE("/users/{id}/mappedstates/{mappedStateId}")
    fun deleteMappedState(
        @Path(value = "id") userId: String,
        @Path(value = "mappedStateId") mappedStateId: String
    ): Completable


    @GET("/users/{id}/mappedactivities")
    fun getMappedActivities(@Path(value = "id") userId: String): Single<List<MappedActivity>>

    @GET("/users/{id}/mappedactivities/{mappedActivityId}")
    fun getMappedActivity(
        @Path(value = "id") userId: String,
        @Path(value = "mappedActivityId") mappedActivityId: String
    ): Single<MappedActivity>

    @GET("/users/{id}/schedule/generatedactivities")
    fun generateAndGetMappedActivities(@Path(value = "id") userId: String): Single<List<MappedActivity>>

    @POST("/users/{id}/mappedactivities")
    fun createMappedActivity(
        @Path(value = "id") userId: String,
        @Body mappedActivityCreateDto: MappedActivityCreateDto
    ): Single<MappedActivity>

    @PATCH("/users/{id}/mappedactivities/{mappedActivityId}")
    fun updateMappedActivity(
        @Path(value = "id") userId: String,
        @Path(value = "mappedActivityId") mappedActivityId: String,
        @Body mappedActivityUpdateDto: MappedActivityUpdateDto
    ): Single<MappedActivity>

    @DELETE("/users/{id}/mappedactivities/{mappedActivityId}")
    fun deleteMappedActivity(
        @Path(value = "id") userId: String,
        @Path(value = "mappedActivityId") mappedActivityId: String
    ): Completable
}