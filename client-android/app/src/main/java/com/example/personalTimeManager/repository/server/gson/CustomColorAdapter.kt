package com.example.personalTimeManager.repository.server.gson

import com.example.personalTimeManager.repository.CustomColor
import com.google.gson.*
import java.lang.reflect.Type

object CustomColorAdapter : JsonDeserializer<CustomColor>, JsonSerializer<CustomColor> {
    override fun deserialize(
        json: JsonElement,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): CustomColor {
        return CustomColor(json.asJsonPrimitive.asString)
    }

    override fun serialize(
        src: CustomColor,
        typeOfSrc: Type?,
        context: JsonSerializationContext?
    ): JsonElement {
        return JsonPrimitive(src.stringColor)
    }
}