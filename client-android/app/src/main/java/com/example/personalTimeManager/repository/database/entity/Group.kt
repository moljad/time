package com.example.personalTimeManager.repository.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.example.personalTimeManager.repository.database.converter.DateConverters
import com.google.gson.annotations.SerializedName
import java.time.LocalDateTime

@Entity(
    tableName = "Groups"
)
data class Group(
    @PrimaryKey
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @TypeConverters(DateConverters::class)
    @SerializedName("createdDate")
    val createdDate: LocalDateTime,
    @TypeConverters(DateConverters::class)
    @SerializedName("updatedDate")
    val updatedDate: LocalDateTime
) {
    @Suppress("unused")
    private constructor() : this(-1073741824, "", LocalDateTime.now(), LocalDateTime.now())
}