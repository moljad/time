package com.example.personalTimeManager.presentation.activities.stateSchedule

import android.app.Application
import com.example.personalTimeManager.presentation.base.BaseViewModel
import com.example.personalTimeManager.repository.AppRepository
import com.example.personalTimeManager.repository.database.entity.MappedState
import com.example.personalTimeManager.repository.database.entity.State
import io.reactivex.Single

class StateScheduleViewModel(application: Application, private val repository: AppRepository) :
    BaseViewModel(application) {
    fun getStates(): Single<List<State>> {
        return repository.getStatesByUserId(null)
    }

    fun getMappedStates(): Single<List<MappedState>> {
        return repository.getMappedStatesByUserId()
    }
}
