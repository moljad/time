package com.example.personalTimeManager.repository.database.entity

import androidx.room.*
import com.example.personalTimeManager.repository.Span
import com.example.personalTimeManager.repository.database.Status
import com.example.personalTimeManager.repository.database.converter.DateConverters
import com.example.personalTimeManager.repository.server.dto.MappedActivityCreateDto
import com.example.personalTimeManager.repository.server.dto.MappedActivityUpdateDto
import com.example.personalTimeManager.repository.server.gson.Exclude
import com.google.gson.annotations.SerializedName
import java.time.LocalDateTime

@Entity(
    tableName = "MappedActivities",
    foreignKeys = [
        ForeignKey(
            entity = Activity::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("activityId"),
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = Schedule::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("scheduleId"),
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        )]
)
data class MappedActivity(
    @PrimaryKey
    @SerializedName("id")
    val id: Int,
    @Embedded
    @SerializedName("span")
    val span: Span,
    @ColumnInfo(name = "activityId", index = true)
    @SerializedName("activityId")
    val activityId: Int,
    @ColumnInfo(name = "scheduleId", index = true)
    @SerializedName("scheduleId")
    val scheduleId: Int,
    @TypeConverters(DateConverters::class)
    @SerializedName("createdDate")
    val createdDate: LocalDateTime,
    @TypeConverters(DateConverters::class)
    @SerializedName("updatedDate")
    val updatedDate: LocalDateTime,
    @Exclude
    val status: Status = Status.UNKNOWN
) {
    @Suppress("unused")
    private constructor() : this(
        -1073741824, Span(0, 1), -1073741824, -1073741824,
        LocalDateTime.now(), LocalDateTime.now()
    )

    @Exclude
    @delegate:Exclude
    @delegate:Ignore
    val mappedActivityCreateDto by lazy {
        MappedActivityCreateDto(
            this.span, this.activityId
        )
    }

    @Exclude
    @delegate:Exclude
    @delegate:Ignore
    val mappedActivityUpdateDto by lazy {
        MappedActivityUpdateDto(
            this.span, this.activityId
        )
    }
}